var canvas2d = document.querySelector("#glCanvas");
var ctx2d = canvas2d.getContext("2d");
var mouseIsDown = false;
var mouseInCanvas = false;
var pi = Math.PI;

var selectedHandle = -1;
var selectedBSPoint = -1;
var prevPos = glMatrix.vec2.create();
var numBone;

var j;

function initTabWeights() {
	if (_modeH == modeHandler.CP) {
		_lLayer[_idActiveLayer].model.unnormalizedWeightsCP = [];
		for (let j=0 ; j<_lLayer[_idActiveLayer].model.cp._cp.length ; j++) {
			_lLayer[_idActiveLayer].model.unnormalizedWeightsCP.push(new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
		}

		_lLayer[_idActiveLayer].model.normalizedWeightsCP = [];
		for (let j=0 ; j<_lLayer[_idActiveLayer].model.cp._cp.length ; j++) {
			_lLayer[_idActiveLayer].model.normalizedWeightsCP.push(new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
		}
	}
	if (_modeH == modeHandler.Skeleton) {
		_lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton = [];
		for (let j=0 ; j<_lLayer[_idActiveLayer].model.skeleton._bones.length ; j++) {
			_lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton.push(new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
		}

		_lLayer[_idActiveLayer].model.normalizedWeightsSkeleton = [];
		for (let j=0 ; j<_lLayer[_idActiveLayer].model.skeleton._bones.length ; j++) {
			_lLayer[_idActiveLayer].model.normalizedWeightsSkeleton.push(new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
		}

	}
	if (_modeH == modeHandler.Cage) {
		_lLayer[_idActiveLayer].model.unnormalizedWeightsCage = [];
		for (let j=0 ; j<_lLayer[_idActiveLayer].model.cage._cage.length ; j++) {
			_lLayer[_idActiveLayer].model.unnormalizedWeightsCage.push(new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
		}

		_lLayer[_idActiveLayer].model.normalizedWeightsCage = [];
		for (let j=0 ; j<_lLayer[_idActiveLayer].model.cage._cage.length ; j++) {
			_lLayer[_idActiveLayer].model.normalizedWeightsCage.push(new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
		}
	}
}


function isHandler(x, y, r) {
	var i = 0;
	if (_modeH == modeHandler.CP || (_modeH == modeHandler.AnimByBSpline && Animation.modeHBSpline == modeHandler.CP)) {
		for (i=0; i<_lLayer[_idActiveLayer].model.cp._cp.length;i++) {
      var dist = Math.sqrt(Math.pow(x - _lLayer[_idActiveLayer].model.cp._cp[i].pos[0], 2) + Math.pow(y - _lLayer[_idActiveLayer].model.cp._cp[i].pos[1], 2));
			if ( dist <= r) {
				selectedHandle = i;
				return;
			}
		}
	}
  if (_modeH == modeHandler.Skeleton || (_modeH == modeHandler.AnimByBSpline && Animation.modeHBSpline == modeHandler.Skeleton)) {
		for (i=0; i<_lLayer[_idActiveLayer].model.skeleton._joints.length; i++) {
      var dist = Math.sqrt(Math.pow(x-_lLayer[_idActiveLayer].model.skeleton._joints[i].pos[0],2) + Math.pow(y-_lLayer[_idActiveLayer].model.skeleton._joints[i].pos[1],2));
			if ( dist <= r) {
				selectedHandle = i;
				for (j = 0; j < _lLayer[_idActiveLayer].model.skeleton._bones.length; j++) {
					if (_lLayer[_idActiveLayer].model.skeleton._bones[j].idChild == i) {
						numBone = j;
					}
				}
				return;
			}
		}
	}
  if (_modeH == modeHandler.Cage || (_modeH == modeHandler.AnimByBSpline && Animation.modeHBSpline == modeHandler.Cage)) {
		for (i=0; i<_lLayer[_idActiveLayer].model.cage._cage.length; i++) {
      var dist = Math.sqrt(Math.pow(x-_lLayer[_idActiveLayer].model.cage._cage[i].pos[0],2) + Math.pow(y-_lLayer[_idActiveLayer].model.cage._cage[i].pos[1],2));
			if ( dist <= r) {
				selectedHandle = i;
				return;
			}
		}
	}
	selectedHandle = -1;
}

function isBSplinePoint(x, y, r) {
  // Check modeHandler
  if (_modeH == modeHandler.AnimByBSpline) {
    // Need to know which mode we are in
    switch(Animation.modeHBSpline) {
    case modeHandler.CP:
      // Check current CP
      var currentCP = _lLayer[_idActiveLayer].model.cp._currentCP;
      if (currentCP < 0 || currentCP >= _lLayer[_idActiveLayer].model.cp._cp.length) {
        return -1;
      }
      // Iterate over current cp bs points
      for (i = 0; i < _lLayer[_idActiveLayer].model.cp._cp[currentCP].BS.points.length; ++i) {
        console.log("Found");
        // BS Points are glMatrix so we use the distance method
        var clicPosition = glMatrix.vec2.fromValues(x, y);
        var dist = glMatrix.vec2.distance(_lLayer[_idActiveLayer].model.cp._cp[currentCP].BS.points[i], clicPosition);
        if ( dist <= r) {
          return i;
        }
      }
      break;
    case modeHandler.Skeleton:
      // Check current CP
      var currentJoint = _lLayer[_idActiveLayer].model.skeleton._currentJoint;
      if (currentJoint < 0 || currentJoint >= _lLayer[_idActiveLayer].model.skeleton._joints.length) {
        return -1;
      }
      // Iterate over current cp bs points
      for (i = 0; i < _lLayer[_idActiveLayer].model.skeleton._joints[currentJoint].BS.points.length; ++i) {
        console.log("Found");
        // BS Points are glMatrix so we use the distance method
        var clicPosition = glMatrix.vec2.fromValues(x, y);
        var dist = glMatrix.vec2.distance(_lLayer[_idActiveLayer].model.skeleton._joints[currentJoint].BS.points[i], clicPosition);
        if ( dist <= r) {
          return i;
        }
      }
      break;
    case modeHandler.Cage:
      // Check current Joint
      var currentVertex = _lLayer[_idActiveLayer].model.cage._currentVertex;
      if (currentVertex < 0 || currentVertex >= _lLayer[_idActiveLayer].model.cage._cage.length) {
        return -1;
      }
      // Iterate over current cp bs points
      for (i = 0; i < _lLayer[_idActiveLayer].model.cage._cage[currentVertex].BS.points.length; ++i) {
        console.log("Found");
        // BS Points are glMatrix so we use the distance method
        var clicPosition = glMatrix.vec2.fromValues(x, y);
        var dist = glMatrix.vec2.distance(_lLayer[_idActiveLayer].model.cage._cage[currentVertex].BS.points[i], clicPosition);
        if ( dist <= r) {
          return i;
        }
      }
      break;
    default:
    }
  }
  // We didn't return any bs point so return invalid index
  return -1;
}


canvas2d.onmousedown = function (event) {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
  var e = event || window.event;
  // trouver la position dans le canvas
  var x = e.offsetX;
  var y = e.offsetY;
  var updateNeeded = false;

  mouseIsDown = true;
  prevPos = glMatrix.vec2.fromValues(x, y);


  //on teste si le clic a ete fait sur une poignee
  // Radius: 2*rayon2+1 = 2*3+1 = 7
  isHandler(x, y, 7);

	if (_modeE == modeEdition.Add) {
		switch(_modeH) {
			case modeHandler.CP:
				if(selectedHandle == -1) {
					addCP(x, y);
					if(_lLayer[_idActiveLayer].model.cp._cp.length == 1) {
						setAstuce("modifierSDC");
					} else if(_lLayer[_idActiveLayer].model.cp._cp.length == 4) {
						setAstuce("validerSDC");
					}
				} else {
					setActiveCP(x, y, selectedHandle);
				}
				updateNeeded = true;
				break;
			case modeHandler.Skeleton:
				if(selectedHandle == -1) {
					addSkeleton(x, y);
					if(_lLayer[_idActiveLayer].model.skeleton._bones.length == 1) {
						setAstuce("modifierSDC");
					} else if(_lLayer[_idActiveLayer].model.skeleton._bones.length == 4) {
						setAstuce("validerSDC");
					}
				} else {
					setActiveSkeleton(x, y, selectedHandle);
				}
				updateNeeded = true;
				break;
			case modeHandler.Cage:
				if(selectedHandle == -1) {
					addCageVertex(x, y);
					_lLayer[_idActiveLayer].model.cage._faces = [];
					if(_lLayer[_idActiveLayer].model.cage._cage.length == 1) {
						setAstuce("modifierSDC");
					} else if(_lLayer[_idActiveLayer].model.cage._cage.length == 4) {
						setAstuce("validerSDC");
					}
				} else {
					setActiveCageVertex(x, y, selectedHandle);
				}
				updateNeeded = true;
				break;
			case modeHandler.AnimByBSpline:
        // Si on a cliqué sur un Handle on change de current CP
        if(selectedHandle != -1) {
          switch (Animation.modeHBSpline) {
          case modeHandler.CP:
            setActiveCP(x, y, selectedHandle);
            break;
          case modeHandler.Skeleton:
            setActiveSkeleton(x, y, selectedHandle);
            break;
          case modeHandler.Cage:
            setActiveCageVertex(x, y, selectedHandle);
            break;
          default:
          }
        } else {
          addBSPoint(x, y);
        }
        updateNeeded = true;
				break;
			default:
				console.log("erreur switch case");
		}
	} else if (_modeE == modeEdition.Delete) {
		switch(_modeH) {
			case modeHandler.CP:
				if(selectedHandle != -1) {
					deleteCP(x, y, selectedHandle);
					updateNeeded = true;
				}
				break;
			case modeHandler.Skeleton:
				if(selectedHandle != -1) {
					if (selectedHandle == 0) {
						deleteRacine(selectedHandle);
					} else if (_lLayer[_idActiveLayer].model.skeleton._joints[selectedHandle].child.length == 0) {
						deleteLeaf(selectedHandle);
					} else if (e.button == 2) {
						deleteNodeRight(selectedHandle);
					} else if (e.button == 0) {
						deleteNodeLeft(selectedHandle);
					}
					updateNeeded = true;
				}
				break;
			case modeHandler.Cage:
				if(selectedHandle != -1) {
					deleteCageVertex(x, y, selectedHandle);
					_lLayer[_idActiveLayer].model.cage._faces = [];
					updateNeeded = true;
				}
				break;
			case modeHandler.AnimByBSpline:
				// Si on a cliqué sur un Handle on change de current CP
				if(selectedHandle != -1) {
					switch (Animation.modeHBSpline) {
					case modeHandler.CP:
						setActiveCP(x, y, selectedHandle);
						break;
					case modeHandler.Skeleton:
						setActiveSkeleton(x, y, selectedHandle);
						break;
					case modeHandler.Cage:
						setActiveCageVertex(x, y, selectedHandle);
						break;
					default:
					}
					updateNeeded = true;
				} else {
					// /!\ Penser à mettre à jour le rayon avec le rayon dans afficherBSplin() de displayCanvas.js
					var foundBSPoint = isBSplinePoint(x, y, 36);
					if(foundBSPoint != -1) {
						console.log("cliqué sur une bspline");
						deleteBSPoint(foundBSPoint);
						updateNeeded = true;
					}
				}
				break;
			default:
				console.log("erreur switch case");
		}
	} else if (_modeE == modeEdition.Move) {
		switch(_modeH) {
			case modeHandler.CP:
				if(selectedHandle != -1) {
					setActiveCP(x, y, selectedHandle);
					updateNeeded = true;
				}
				break;
			case modeHandler.Skeleton:
				if(selectedHandle != -1) {
					setActiveSkeleton(x, y, selectedHandle);
					updateNeeded = true;
				}
				break;
			case modeHandler.Cage:
				if(selectedHandle != -1) {
					setActiveCageVertex(x, y, selectedHandle);
					updateNeeded = true;
				}
				break;
			case modeHandler.AnimByBSpline:
        // Si on a cliqué sur un Handle on change de current CP
        if(selectedHandle != -1) {
          switch (Animation.modeHBSpline) {
          case modeHandler.CP:
              setActiveCP(x, y, selectedHandle);
              break;
          case modeHandler.Skeleton:
              setActiveSkeleton(x, y, selectedHandle);
              break;
		      case modeHandler.Cage:
		          setActiveCageVertex(x, y, selectedHandle);
		          break;
          default:
          }
          updateNeeded = true;
        }
        else {
          // /!\ Penser à mettre à jour le rayon avec le rayon dans afficherBSplin() de displayCanvas.js
          selectedBSPoint = isBSplinePoint(x, y, 10);
        }
				break;
			default:
				console.log("erreur switch case");
		}
	} else if (_modeE == modeEdition.Deform) {
		switch(_modeH) {
			case modeHandler.CP:
				if(selectedHandle != -1) {
					setActiveCP(x, y, selectedHandle);
					updateNeeded = true;
				}
				break;
      case modeHandler.Skeleton:
				if(selectedHandle != -1) {
					setActiveSkeleton(x, y, selectedHandle);
					updateNeeded = true;
				}
				break;
			case modeHandler.Cage:
				if(selectedHandle != -1) {
					setActiveCageVertex(x, y, selectedHandle);
					updateNeeded = true;
				}
				break;
			case modeHandler.AnimByBSpline:
        // Si on a cliqué sur un Handle on change de current CP
        if(selectedHandle != -1) {
          switch (Animation.modeHBSpline) {
          case modeHandler.CP:
              setActiveCP(x, y, selectedHandle);
              break;
          case modeHandler.Skeleton:
              setActiveSkeleton(x, y, selectedHandle);
              break;
		      case modeHandler.Cage:
		          setActiveCageVertex(x, y, selectedHandle);
		          break;
          default:
          }
        }
        updateNeeded = true;
				break;
			default:
				console.log("erreur switch case");
		}
	} else if (_modeE == modeEdition.Paint) {
		  paint(x,y);
		  updateNeeded = true;
	} else if (_modeE == modeEdition.Erase) {
	    erase(x,y);
		  updateNeeded = true;
	}

	//si on a besoin de mettre a jour l'affichage
	if(updateNeeded) {
		switch(_modeH) {
			case modeHandler.CP:
				drawPoint(_lLayer[_idActiveLayer].model);
				break;
			case modeHandler.Skeleton:
				drawSkeleton(_lLayer[_idActiveLayer].model);
				break;
			case modeHandler.Cage:
				if (_lLayer[_idActiveLayer].model.cage._faces.length == 0) {
					drawCage(_lLayer[_idActiveLayer].model);
				}
        else {
        	drawTrianglesCage(_lLayer[_idActiveLayer].model);
        }
				break;
			case modeHandler.AnimByBSpline:
        // For the animation, we need to know which data structure
        switch (Animation.modeHBSpline) {
        case modeHandler.CP:
          drawPoint(_lLayer[_idActiveLayer].model);
          afficherBSpline(_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].BS);
          break;
        case modeHandler.Skeleton:
          drawSkeleton(_lLayer[_idActiveLayer].model);
          afficherBSpline(_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._currentJoint].BS);
          break;
        case modeHandler.Cage:
          drawCage(_lLayer[_idActiveLayer].model);
          afficherBSpline(_lLayer[_idActiveLayer].model.cage._cage[_lLayer[_idActiveLayer].model.cage._currentVertex].BS);
          break;
        default:
        }
				break;
			default:
				console.log("erreur switch case");
		}
		if (_modeE == modeEdition.Paint || _modeE == modeEdition.Erase) {
			drawBrush(x, y);
		}
	}
}

canvas2d.oncontextmenu = function(e) { return false; }

canvas2d.onmousemove = function(e) {
  // trouver la position dans le canvas
  var x = e.offsetX;
  var y = e.offsetY;

  if (_modeE != modeEdition.Paint && _modeE != modeEdition.Erase && !mouseIsDown) return;

  if (_modeH == modeHandler.CP) {
	  if(_modeE == modeEdition.Move && selectedHandle!=-1) {
			moveCP(x,y,selectedHandle);
		}
    //drawPoint(_lLayer[_idActiveLayer].model);
		if(_modeE == modeEdition.Deform && selectedHandle!=-1) {
      moveCP(x, y, selectedHandle);
      updateTransformCP(_lLayer[_idActiveLayer].model, selectedHandle);
      glLoadTransf(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.cp._cp);
      glDrawOne(_lLayer[_idActiveLayer].model);
    }
		drawPoint(_lLayer[_idActiveLayer].model);
  }
  if (_modeH == modeHandler.Skeleton) {
    if(_modeE == modeEdition.Move && selectedHandle!=-1)
      moveSkeleton(x,y,selectedHandle);
      //drawSkeleton(_lLayer[_idActiveLayer].model);
    if(_modeE == modeEdition.Deform && selectedHandle!=-1) {
      moveSkeleton(x, y, selectedHandle);
      updateTransformAngle(x, y, selectedHandle, _lLayer[_idActiveLayer].model, numBone);
      updateTransformSkeleton(_lLayer[_idActiveLayer].model, selectedHandle, numBone);
      glLoadTransf(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.skeleton._bones);
      glDrawOne(_lLayer[_idActiveLayer].model);
    }
    drawSkeleton(_lLayer[_idActiveLayer].model);
  }
  if (_modeH == modeHandler.Cage) {
	  if(_modeE == modeEdition.Move && selectedHandle!=-1) {
			moveCageVertex(x,y,selectedHandle);
		}
    //drawPoint(_lLayer[_idActiveLayer].model);
		if(_modeE == modeEdition.Deform && selectedHandle!=-1) {
			moveCageVertex(x, y, selectedHandle);
			updateTransformCageVertex(_lLayer[_idActiveLayer].model, selectedHandle);
			glLoadTransf(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.cage._cage);
			glDrawOne(_lLayer[_idActiveLayer].model);
		}
		if (_lLayer[_idActiveLayer].model.cage._faces == 0) {
			drawCage(_lLayer[_idActiveLayer].model);
		}
		else {
			drawTrianglesCage(_lLayer[_idActiveLayer].model);
		}
  }

  if (_modeH == modeHandler.AnimByBSpline) {
    if(_modeE == modeEdition.Move && selectedBSPoint!=-1) {
      // Move point
      moveBSPoint(x, y, selectedBSPoint);
      // Update (need to know which data structure to display)
      switch (Animation.modeHBSpline) {
      case modeHandler.CP:
        drawPoint(_lLayer[_idActiveLayer].model);
        afficherBSpline(_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].BS);
        break;
      case modeHandler.Skeleton:
				drawSkeleton(_lLayer[_idActiveLayer].model);
				afficherBSpline(_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._currentJoint].BS);
        break;
      case modeHandler.Cage:
				drawCage(_lLayer[_idActiveLayer].model);
				afficherBSpline(_lLayer[_idActiveLayer].model.cage._cage[_lLayer[_idActiveLayer].model.cage._currentVertex].BS);
        break;
      default:
      }
    }
  }

	if (_modeE == modeEdition.Paint) {
		drawBrush(x, y);
		if (mouseIsDown) {
			paint(x,y);
			prevPos = glMatrix.vec2.fromValues(x, y);
		}
	} else if (_modeE == modeEdition.Erase) {
		drawBrush(x, y);
		if (mouseIsDown) {
			erase(x,y);
			prevPos = glMatrix.vec2.fromValues(x, y);
		}
	}
}

canvas2d.onmouseup = function(e) {
  var x = e.pageX - canvas2d.offsetLeft;
  var y = e.pageY - canvas2d.offsetTop;
  mouseIsDown = false;

  if (_modeE == modeEdition.Move && _modeH == modeHandler.CP && selectedHandle!=-1) {
    moveCP(x,y,selectedHandle);
    drawPoint(_lLayer[_idActiveLayer].model);
  }

  selectedHandle = -1;
  selectedBSPoint = -1;
}

canvas2d.onmouseover = function(e) {
	mouseInCanvas = true;
}

canvas2d.onmouseout = function(e) {
	mouseInCanvas = false;

	if(_modeE == modeEdition.Paint || _modeE == modeEdition.Erase) {
		if (_modeH == modeHandler.CP) {
	        drawPoint(_lLayer[_idActiveLayer].model);
	  }
		else if (_modeH == modeHandler.Skeleton) {
	        drawSkeleton(_lLayer[_idActiveLayer].model);
	  }
	}
}

canvas2d.onmousewheel = canvas2d.onwheel = function(e) {
	if (_modeH == modeHandler.CP && _modeE == modeEdition.Deform) {
		//window.onmousewheel = canvas2d.onmousewheel;
		e.wheelDelta = e.wheelDelta ? e.wheelDelta: (e.deltaY*(-40));
		if (e.wheelDelta > 0) {
			_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].angle += 10;
			updateTransformCP(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.cp._currentCP);
		}
		else if (e.wheelDelta < 0) {
			_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].angle -= 10;
			updateTransformCP(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.cp._currentCP);
		}
		glLoadTransf(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.cp._cp);
		glDrawOne(_lLayer[_idActiveLayer].model);
		e.preventDefault();
	}
}

function euclideanDist (x1,y1,x2,y2) {
  return Math.sqrt(Math.pow(x1-x2,2) + Math.pow(y1-y2,2));
}

function squaredDist (x1,y1,x2,y2) {
  return Math.pow(x1-x2,2) + Math.pow(y1-y2,2);
}

function lerpPaint(t) {
  var a = 1;
  var b = 0;
  return ((a*(1-t)) + (b*t));
}

function paint(x,y) {
  var rayBrush = 0;
  var gradient = 0;
  //pointeur vers les poids
  var weights;
  //on récupère les bonnes valeurs
  switch (_modeH) {
    case modeHandler.CP:
      if (_lLayer[_idActiveLayer].model.cp._currentCP == -1)
        return;

      rayBrush = 0.5 + Number($("#rangeDiamCP").val());
      weights = _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP];
      gradient = $("#rangeDegCP").val();
      break;

    case modeHandler.Skeleton:
      if (_lLayer[_idActiveLayer].model.skeleton._currentBone == -1)
        return;

      rayBrush = 0.5 + Number($("#rangeDiamSquelette").val());
      weights = _lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton[_lLayer[_idActiveLayer].model.skeleton._currentBone];
      gradient = $("#rangeDegSquelette").val();
      break;

    default:
	  console.log("erreur");
      break;
  }

  var gradientWidth;
  var t;
  var dist;

  var minY = Math.min(prevPos[1], y) - rayBrush;
  //plus grande valeur de y avant la sortie anticipée
  var maxY = Math.max(prevPos[1], y) + rayBrush;
  var i;

  var posRep = glMatrix.vec2.create();
  var posClic = glMatrix.vec2.create();

  var tmp1 = glMatrix.vec2.create();
  var tmp2 = glMatrix.vec2.create();
  var tmp3 = glMatrix.vec2.create();

  for (i = 0; i < _lLayer[_idActiveLayer].model.restMesh.length; i+=2) {
    //test pour sortie anticipée
    if ( _lLayer[_idActiveLayer].model.restMesh[i+1] > maxY ) {
      i = _lLayer[_idActiveLayer].model.restMesh.length;
    }

    if( _lLayer[_idActiveLayer].model.restMesh[i+1] > minY ) {
      glMatrix.vec2.set(posRep, _lLayer[_idActiveLayer].model.restMesh[i], _lLayer[_idActiveLayer].model.restMesh[i+1]);
      glMatrix.vec2.set(posClic, x, y);

      //var dist = euclideanDist(x,y,_lLayer[_idActiveLayer].model.restMesh[i],_lLayer[_idActiveLayer].model.restMesh[i+1]);
      dist = Math.sqrt(distPtSeg(posRep, prevPos, posClic, tmp1, tmp2, tmp3));

      // si on est dans le curseur de la brosse
      if (dist <= rayBrush) {
        // si on est entre le centre et le dégradé
        if (dist <= (1-gradient)*rayBrush) {
          weights[i/2] = 1;
        } else {
          // si on est dans le dégradé et toujours à l'intérieur du curseur
          gradientWidth = gradient*rayBrush;
          t = dist - (1-gradient)*rayBrush;
          t = t / gradientWidth;
          weights[i/2] = Math.min(Math.max(lerpPaint(t), weights[i/2]), 1);
        }
      }
    }
  }

  //mise à jour de l'affichage
  switch (_modeH) {
    case modeHandler.CP:
      glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP]);
      break;

    case modeHandler.Skeleton:
      glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton[_lLayer[_idActiveLayer].model.skeleton._currentBone]);
      break;

    default:
      console.log("erreur");
      break;
  }
  glDrawOne(_lLayer[_idActiveLayer].model);
}


//les vecteurs tmp permettent a la fonction de faire ses calculs sans
//devoir creer des nouveaux vecteur, leurs valeurs vont etre modifiees
function distPtSeg(pt, seg1, seg2, tmpVec1, tmpVec2, tmpVec3) {
  // Compute orthogonal band to know whether it is nearer to a point or the segment
  var vec_pt_seg1 = tmpVec1;
  glMatrix.vec2.subtract(vec_pt_seg1, seg1, pt);
  var vec_seg2_pt = tmpVec2;
  glMatrix.vec2.subtract(vec_seg2_pt, pt, seg2);
  var vec_seg1_seg2 = tmpVec3;
  glMatrix.vec2.subtract(vec_seg1_seg2, seg2, seg1);

  if(glMatrix.vec2.dot(vec_seg1_seg2, vec_pt_seg1) >= 0) {
    // Closest point is the parent, hence euclidian distance from point to parent
    boneDistance = glMatrix.vec2.dot(vec_pt_seg1, vec_pt_seg1);

  } else if(glMatrix.vec2.dot(vec_seg1_seg2, vec_seg2_pt) >= 0) {
    // Closest point is the child, hence euclidian distance from point to child
    boneDistance = glMatrix.vec2.dot(vec_seg2_pt, vec_seg2_pt);

  } else {
    // Closest point is point projected onto line segment between parent and child
    var ratio = glMatrix.vec2.dot(vec_seg1_seg2, vec_pt_seg1) / glMatrix.vec2.dot(vec_seg1_seg2, vec_seg1_seg2);

    //on réutilise vec_seg2_pt pour ne pas avoir a recreer un nouveau vecteur
    var right = vec_seg2_pt;
    glMatrix.vec2.scale(right, vec_seg1_seg2, ratio);

    //on réutilise vec_seg1_seg2 pour ne pas avoir a recreer un nouveau vecteur
    var e = vec_seg1_seg2;
    glMatrix.vec2.subtract(e, vec_pt_seg1, right);
    boneDistance = glMatrix.vec2.dot(e, e);
  }
  return boneDistance;
}



function erase(x,y) {
  var rayBrush = 0;
  var weights;
  if (_modeH == modeHandler.CP) {
    rayBrush = 0.5 + Number($("#rangeDiamCP").val());
    weights = _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP];
  }
  else if (_modeH == modeHandler.Skeleton) {
		// Don't erase because there's no current bone
		if (_lLayer[_idActiveLayer].model.skeleton._currentBone == -1) {
			return;
		}

    rayBrush = 0.5 + Number($("#rangeDiamSquelette").val());
    weights = _lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton[_lLayer[_idActiveLayer].model.skeleton._currentBone];
  }
  else if (_modeH == modeHandler.Cage) {
	  console.log("interdit");
  }

  rayBrush = rayBrush*rayBrush;
  var i;
  for (i = 0; i < _lLayer[_idActiveLayer].model.restMesh.length; i+=2) {
    if (_lLayer[_idActiveLayer].model.restMesh[i+1] > _lLayer[_idActiveLayer].model.restMesh[i+1] + rayBrush) {
      return;
    }
    var dist = squaredDist(x,y,_lLayer[_idActiveLayer].model.restMesh[i],_lLayer[_idActiveLayer].model.restMesh[i+1]);
    // si on est dans le curseur de la gomme
    if (dist <= rayBrush) {
        weights[i/2] = 0;
    }
  }
	switch (_modeH) {
		case modeHandler.CP:
			glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP]);
			break;
		case modeHandler.Skeleton:
			console.log("Painting skeleton")
			glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton[_lLayer[_idActiveLayer].model.skeleton._currentBone]);
			break;
		default:
			console.log("erreur");
			break;
	}
  glDrawOne(_lLayer[_idActiveLayer].model);
}
