/**
 * Chef d'oeuvre - Animeme
 * M2 IGAI - 2018/2019 - Université Toulouse 3 Paul Sabatier
 */



/**
 * Animation class to hold animation properties and values and manage handle's bSplines and actually play the animation
 */
class AnimationClass {
  // Class attributes in constructor (yay, javascript)
  constructor(){
    // Default, no proper modeHandler, we update them with the radio buttons
    this.modeHBSpline = modeHandler.CP;

    // Fetch interface default animation values
    // Animation duration
    this.duration = Number($("#animation_Duration").val());
    // Order of the BSpline
    this.order = Number($("#animation_BS_Order").val());

    // Animation properties
    this.isPlaying = false;
    this.time = Number($("#animation_Time").val());

  }

  /**
   * setModeHBSpline
   * @param mode enum ModeHandler to know which data structure to display and use for BSplines
   * Update the modeHandler used by the Animation class
   */
setModeHBSpline(mode) {
	// Update
	this.modeHBSpline = mode;
	// Display the proper handle type
	switch(mode){
	case modeHandler.CP:
		drawPoint(_lLayer[_idActiveLayer].model);
		glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCP);
		break;
	case modeHandler.Skeleton:
		drawSkeleton(_lLayer[_idActiveLayer].model);
		glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsSkeleton);
		break;
	case modeHandler.Cage:
		drawCage(_lLayer[_idActiveLayer].model);
		glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCage);
		break;
	default:
		console.log("No data structure selected for Animation");
	}
}

  /**
   * setValues
   * @param duration duration of the animation set by the input field (check if greater than 1)
   * @param order of the bspline set bu the input field (check if between 2 and 10)
   * Check the parameters and update the instance attributes
   */
  setValues(duration, order) {
    // Test values not to break the class
    if (isNaN(duration) || duration <= 0) {
      // Default duration
      duration = 1;
    }
    if (isNaN(order) || order < 2) {
      // Min order
      order = 2;
    }
    if (order > 10) {
      // Max order
      order = 10;
    }
    // Update values
    this.duration = duration;
    this.order = order;
  }

  setPlayPause() {
		if (this.isPlaying)
			this.isPlaying = false;
		else
			this.isPlaying = true;
  }

  stop() {
  }

	applyAnimation(model, u) {
		if (this.modeHBSpline == modeHandler.CP) {
			for(let i = 0; i < model.cp._cp.length; i++) {
				var pdc = model.cp._cp[i];
				if(pdc.BS.points.length >= pdc.BS.k && pdc.BS.k > 0) {
					glMatrix.vec2.copy(pdc.pos, approximation(pdc.BS, u));
					updateTransformCP(model, i);
				}
			}
			glLoadTransf(model, model.cp._cp);
			//glDrawAll(_lLayer);
			if(!this.isPlaying) {
				drawPoint(model);
			} else {
				ctx2d.clearRect(0,0,800,800);
			}
		} else if (this.modeHBSpline == modeHandler.Skeleton) {
			var numB;
			for(let i = 0; i < model.skeleton._joints.length; i++) {
				var joint = model.skeleton._joints[i];
				if(joint.BS.points.length >= joint.BS.k && joint.BS.k > 0) {
					glMatrix.vec2.copy(joint.pos, approximation(joint.BS, u));
					for (let b  = 0 ; b < model.skeleton._bones.length ; b++) {
						if ( model.skeleton._bones[b].idChild == i) {
							numB = b;
						}
					}
					updateTransformAngle(joint.pos[0], joint.pos[1], i, model, numB);
					updateTransformSkeleton(model, i, numB);
				}
			}
			glLoadTransf(model, model.skeleton._bones);
			//glDraw(model.indTri.length);
			if(!this.isPlaying) {
				drawSkeleton(model);
			} else {
				ctx2d.clearRect(0,0,800,800);
			}
		} else if (this.modeHBSpline == modeHandler.Cage) {
			for(let i = 0; i < model.cage._cage.length; i++) {
				var vCage = model.cage._cage[i];
				if(vCage.BS.points.length >= vCage.BS.k && vCage.BS.k > 0) {
					glMatrix.vec2.copy(vCage.pos, approximation(vCage.BS, u));
					updateTransformCageVertex(model, i);
				}
			}
			glLoadTransf(model, model.cage._cage);
			//glDraw(model.indTri.length);
			if(!this.isPlaying) {
				drawCage(model);
			} else {
				ctx2d.clearRect(0,0,800,800);
			}
		}
	}
}

// Global declaration of the instance of AnimationClass (we need this to have attributes because no static class)
var Animation = new AnimationClass();
