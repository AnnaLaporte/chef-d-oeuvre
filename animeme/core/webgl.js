//------webgl------
//var nbTransfos = 1;
var glDrawWeights = false;
function glSetDrawWeights(valeur) {
	glDrawWeights = (valeur == true);
	glDrawOne(_lLayer[_idActiveLayer].model);
}
// recuperation du canvas
const canvas = document.querySelector('#glCanvasWebGL');
// recuperation du contexte webgl
const gl = canvas.getContext('webgl2');
if (!gl) {
	alert('Unable to initialize WebGL. Your browser or machine may not support it.');
}

gl.clearColor(0.9, 0.9, 0.9, 1.0);
gl.clearDepth(1.0);
//gl.enable(gl.DEPTH_TEST);
//gl.depthFunc(gl.LESS);
gl.enable(gl.BLEND);
gl.blendFunc(gl.ONE, gl.ZERO);
//gl.blendFunc(gl.SRC_COLOR, gl.DST_COLOR);
gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

//-------program------
// Vertex shader program
const vsSource = `
	attribute vec2 aVertexPosition;
	attribute vec2 aTextureCoord;
	attribute float aWeight;
	varying highp vec2 texCoord;
	varying highp float weight;
	void main() {
		gl_Position = vec4(aVertexPosition.x / 400.0 - 1.0, - aVertexPosition.y / 400.0 + 1.0, 0, 1);
		texCoord = aTextureCoord;
		weight = aWeight;
	}
`;
//deformation GPU
const vsSourceGPU = `
	attribute vec2 aVertexPosition;
	attribute vec2 aTextureCoord;
	attribute float aWeight;

	attribute vec4 aTransfWeights;
	attribute vec4 aTransfIDs;

	uniform sampler2D uTransfSampler;
	uniform float uNbTransf;

	varying highp vec2 texCoord;
	varying highp float weight;

	// these offsets assume the texture is 2 pixels across
	#define ROW0_U ((0.5 + 0.0) / 2.)
	#define ROW1_U ((0.5 + 1.0) / 2.)
	mat3 getTransfMatrix(float trID) {
		float v = (trID + 0.5) / uNbTransf;
		highp vec4 l1 = texture2D(uTransfSampler, vec2(v, ROW0_U));
		highp vec4 l2 = texture2D(uTransfSampler, vec2(v, ROW1_U));
		return mat3(l1.x, l2.x, 0, l1.y, l2.y, 0, l1.z, l2.z, 1);
	}

	void main() {
		vec3 origPos = vec3(aVertexPosition, 1);
  		mat3 finalM = getTransfMatrix(aTransfIDs.x) * aTransfWeights.x +
  					  getTransfMatrix(aTransfIDs.y) * aTransfWeights.y +
  					  getTransfMatrix(aTransfIDs.z) * aTransfWeights.z +
  					  getTransfMatrix(aTransfIDs.w) * aTransfWeights.w;
		vec3 defPos = finalM * origPos;
		highp float sum = aTransfWeights.x + aTransfWeights.y +
						  aTransfWeights.z + aTransfWeights.w;
		if(sum < 1.0)
			defPos = defPos + (1.0 - sum) * origPos;

		gl_Position = vec4(defPos.x / 400.0 - 1.0, - defPos.y / 400.0 + 1.0, 0, 1);
		texCoord = aTextureCoord;
		weight = aWeight;
	}
`;
// Fragment shader program
const fsSource = `
	varying highp vec2 texCoord;
	varying highp float weight;
	uniform highp vec4 uColor;
	uniform bool uDrawWeight;
	uniform sampler2D uTexSampler;
	void main() {
		if(uDrawWeight) {
			gl_FragColor = vec4(1.0, 1.0-weight, 1.0-weight, 1.0);
		} else {
			gl_FragColor = vec4(texture2D(uTexSampler, texCoord).rgb, 1);
		}
	}
`;
// Initialize a shader program
const shaderProgram = initShaderProgram(vsSourceGPU, fsSource);
// Collect all the info needed to use the shader program.
// Look up which attribute our shader program is using
// for aVertexPosition and look up uniform locations.
const programInfo = {
	program: shaderProgram,
	attribLocations: {
		vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
		textureCoord: gl.getAttribLocation(shaderProgram, 'aTextureCoord'),
		weight: gl.getAttribLocation(shaderProgram, 'aWeight'),
		transfWeights: gl.getAttribLocation(shaderProgram, 'aTransfWeights'), //anim
		transfIDs: gl.getAttribLocation(shaderProgram, 'aTransfIDs'), //anim
	},
	uniformLocations: {
		color: gl.getUniformLocation(shaderProgram, 'uColor'),
		texSampler: gl.getUniformLocation(shaderProgram, 'uTexSampler'),
		drawWeight: gl.getUniformLocation(shaderProgram, 'uDrawWeight'),
		transfSampler: gl.getUniformLocation(shaderProgram, 'uTransfSampler'), //anim
		nbTransf: gl.getUniformLocation(shaderProgram, 'uNbTransf'), //anim
	},
};
/*
//-------vertices------
//creation du buffer contenant les vertices
const positionBuffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
const positions = [
		100, 700,
		700, 700,
		100, 100,
		100, 100,
		700, 700,
		700, 100,
	];
//envoi des positions à webgl
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

//creation du buffer contenant les indices des triangles
const trianglesBuffer = gl.createBuffer();
gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, trianglesBuffer);
var tri = [
		0,1,2,
		3,4,5
	];
gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint32Array(tri), gl.STATIC_DRAW);

//creation du buffer contenant les coordonnées de texture
const textureCoordBuffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);
const textureCoordinates = [
	0.0,  1.0,
	1.0,  1.0,
	0.0,  0.0,
	0.0,  0.0,
	1.0,  1.0,
	1.0,  0.0,
];
//envoi
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordinates), gl.STATIC_DRAW);

//creation du buffer contenant les poids
const weightsBuffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, weightsBuffer);
const baseWeights = [0, 0, 0, 0, 0, 0];
//envoi
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(baseWeights), gl.STATIC_DRAW);

//creation du buffer contenant les poids des transfo
const transfWeightsBuffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, transfWeightsBuffer);
const basetransfWeights = [0, 0, 0, 0,
						   0, 0, 0, 0,
						   0, 0, 0, 0,
						   0, 0, 0, 0,
						   0, 0, 0, 0,
						   0, 0, 0, 0,];
//envoi
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(basetransfWeights), gl.STATIC_DRAW);

//creation du buffer contenant les identifiants des transfo
const transfIDsBuffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, transfIDsBuffer);
const basetransfIDs = [0, 0, 0, 0,
					   0, 0, 0, 0,
					   0, 0, 0, 0,
					   0, 0, 0, 0,
					   0, 0, 0, 0,
					   0, 0, 0, 0,];
//envoi
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(basetransfIDs), gl.STATIC_DRAW);

//------texture------
const texture = gl.createTexture();
gl.bindTexture(gl.TEXTURE_2D, texture);
//on met une texture temporaire car on ne peut pas avoir une texture vide
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
{
const level = 0;
const internalFormat = gl.RGBA;
const width = 1;
const height = 1;
const border = 0;
const srcFormat = gl.RGBA;
const srcType = gl.UNSIGNED_BYTE;
const pixel = new Uint8Array([7, 70, 148, 255]);
gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
							width, height, border, srcFormat, srcType,
							pixel);
}
//texture contenant les matrices de transformation
const texTransf = gl.createTexture();
gl.bindTexture(gl.TEXTURE_2D, texTransf);
//on met une texture temporaire car on ne peut pas avoir une texture vide
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
{
const level = 0;
const internalFormat = gl.RGB32F;
const width = 1;
const height = 2;
const border = 0;
const srcFormat = gl.RGB;
const srcType = gl.FLOAT;
const initTransf = new Float32Array([1.0, 0.0, 0.0,
									 0.0, 1.0, 0.0,]);
gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
							width, height, border, srcFormat, srcType,
							initTransf);
}

glDraw(6);
*/

function glTimer() {
	var a = performance.now();
	gl.finish();
	var b = performance.now();
	a = b - a;
	//console.log("temps de calcul de la frame : " + a + " ms");
	$("#fpsCounter").text(Math.round(1000 / a) + " fps");
}

//ATTENTION : ne pas utiliser cette fonction hors de webgl.js
function drawModel(model) {

	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, model.triElementBuffID);

	// Tell WebGL how to pull out the positions from the position
	// buffer into the vertexPosition attribute.
	{
		//nombre de valeurs par vertex
		const numComponents = 2;
		const type = gl.FLOAT;
		const normalize = false;
		const stride = 0;
		const offset = 0;
		gl.bindBuffer(gl.ARRAY_BUFFER, model.posBuffID);
		gl.vertexAttribPointer(
				programInfo.attribLocations.vertexPosition,
				numComponents,
				type,
				normalize,
				stride,
				offset);
		gl.enableVertexAttribArray(
				programInfo.attribLocations.vertexPosition);
	}
	// Tell WebGL how to pull out the texture coordinates from
	// the texture coordinate buffer into the textureCoord attribute.
	{
		const numComponents = 2;
		const type = gl.FLOAT;
		const normalize = false;
		const stride = 0;
		const offset = 0;
		gl.bindBuffer(gl.ARRAY_BUFFER, model.texCoordBuffID);
		gl.vertexAttribPointer(
				programInfo.attribLocations.textureCoord,
				numComponents,
				type,
				normalize,
				stride,
				offset);
		gl.enableVertexAttribArray(
				programInfo.attribLocations.textureCoord);
	}
	// Tell WebGL how to pull out the weights to draw from
	// the weights buffer into the aWeight attribute.
	{
		const numComponents = 1;
		const type = gl.FLOAT;
		const normalize = false;
		const stride = 0;
		const offset = 0;
		gl.bindBuffer(gl.ARRAY_BUFFER, model.dispWeightBuffID);
		gl.vertexAttribPointer(
				programInfo.attribLocations.weight,
				numComponents,
				type,
				normalize,
				stride,
				offset);
		gl.enableVertexAttribArray(
				programInfo.attribLocations.weight);
	}
	// Tell WebGL how to pull out the transfWeights from
	// the transfWeights buffer into the aTransfWeights attribute.
	{
		const numComponents = 4;
		const type = gl.FLOAT;
		const normalize = false;
		const stride = 0;
		const offset = 0;
		gl.bindBuffer(gl.ARRAY_BUFFER, model.transfWeightsBuffID);
		gl.vertexAttribPointer(
				programInfo.attribLocations.transfWeights,
				numComponents,
				type,
				normalize,
				stride,
				offset);
		gl.enableVertexAttribArray(
				programInfo.attribLocations.transfWeights);
	}

	// Tell WebGL how to pull out the transfWeights from
	// the transfWeights buffer into the aTransfWeights attribute.
	{
		const numComponents = 4;
		const type = gl.FLOAT;
		const normalize = false;
		const stride = 0;
		const offset = 0;
		gl.bindBuffer(gl.ARRAY_BUFFER, model.transfIDBuffID);
		gl.vertexAttribPointer(
				programInfo.attribLocations.transfIDs,
				numComponents,
				type,
				normalize,
				stride,
				offset);
		gl.enableVertexAttribArray(
				programInfo.attribLocations.transfIDs);
	}

	// Tell WebGL to use our program when drawing
	gl.useProgram(programInfo.program);

	// Tell WebGL we want to affect texture unit 0
	gl.activeTexture(gl.TEXTURE0);
	// Bind the texture to texture unit 0
	gl.bindTexture(gl.TEXTURE_2D, model.texUniformID);
	// Tell the shader we bound the texture to texture unit 0
	gl.uniform1i(programInfo.uniformLocations.texSampler, 0);


	gl.activeTexture(gl.TEXTURE1);
	gl.bindTexture(gl.TEXTURE_2D, model.texTransfUniformID);
	gl.uniform1i(programInfo.uniformLocations.transfSampler, 1);

	// envoi des uniform
	gl.uniform4fv(programInfo.uniformLocations.color, [0.8, 0.0, 0.0, 1.0]);
	gl.uniform1i(programInfo.uniformLocations.drawWeight, glDrawWeights);
	gl.uniform1f(programInfo.uniformLocations.nbTransf, model.nbTransf);

	{
		const primitiveType = gl.TRIANGLES;
		//const primitiveType = gl.LINE_STRIP;
		//taille du tableau triangles
		const count = model.indTri.length;
		const type = gl.UNSIGNED_INT;
		const offset = 0;
		gl.drawElements(primitiveType, count, type, offset);
	}
}

function glDrawOne(model) {
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	drawModel(model);
	glTimer();
}

function glDrawAll(layers) {
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	layers.forEach(function(layer) {
		drawModel(layer.model);
	});
	glTimer();
}

/*
//------fonctions draw------
function glDraw(tailleTabTri) {
	gl.clearColor(0.9, 0.9, 0.9, 1.0);
	gl.clearDepth(1.0);                 // Clear everything
	//gl.enable(gl.DEPTH_TEST);           // Enable depth testing
	//gl.depthFunc(gl.LESS);            // Near things obscure far things
	gl.enable(gl.BLEND);
	gl.blendFunc(gl.ONE, gl.ZERO);
	//gl.blendFunc(gl.SRC_COLOR, gl.DST_COLOR);

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	// Tell WebGL how to pull out the positions from the position
	// buffer into the vertexPosition attribute.
	{
		//nombre de valeurs par vertex
		const numComponents = 2;
		const type = gl.FLOAT;
		const normalize = false;
		const stride = 0;
		const offset = 0;
		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
		gl.vertexAttribPointer(
				programInfo.attribLocations.vertexPosition,
				numComponents,
				type,
				normalize,
				stride,
				offset);
		gl.enableVertexAttribArray(
				programInfo.attribLocations.vertexPosition);
	}
	// Tell WebGL how to pull out the texture coordinates from
	// the texture coordinate buffer into the textureCoord attribute.
	{
		const numComponents = 2;
		const type = gl.FLOAT;
		const normalize = false;
		const stride = 0;
		const offset = 0;
		gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);
		gl.vertexAttribPointer(
				programInfo.attribLocations.textureCoord,
				numComponents,
				type,
				normalize,
				stride,
				offset);
		gl.enableVertexAttribArray(
				programInfo.attribLocations.textureCoord);
	}
	// Tell WebGL how to pull out the weights from
	// the weights buffer into the aWeight attribute.
	{
		const numComponents = 1;
		const type = gl.FLOAT;
		const normalize = false;
		const stride = 0;
		const offset = 0;
		gl.bindBuffer(gl.ARRAY_BUFFER, weightsBuffer);
		gl.vertexAttribPointer(
				programInfo.attribLocations.weight,
				numComponents,
				type,
				normalize,
				stride,
				offset);
		gl.enableVertexAttribArray(
				programInfo.attribLocations.weight);
	}
	// Tell WebGL how to pull out the transfWeights from
	// the transfWeights buffer into the aTransfWeights attribute.
	{
		const numComponents = 4;
		const type = gl.FLOAT;
		const normalize = false;
		const stride = 0;
		const offset = 0;
		gl.bindBuffer(gl.ARRAY_BUFFER, transfWeightsBuffer);
		gl.vertexAttribPointer(
				programInfo.attribLocations.transfWeights,
				numComponents,
				type,
				normalize,
				stride,
				offset);
		gl.enableVertexAttribArray(
				programInfo.attribLocations.transfWeights);
	}

	// Tell WebGL how to pull out the transfWeights from
	// the transfWeights buffer into the aTransfWeights attribute.
	{
		const numComponents = 4;
		const type = gl.FLOAT;
		const normalize = false;
		const stride = 0;
		const offset = 0;
		gl.bindBuffer(gl.ARRAY_BUFFER, transfIDsBuffer);
		gl.vertexAttribPointer(
				programInfo.attribLocations.transfIDs,
				numComponents,
				type,
				normalize,
				stride,
				offset);
		gl.enableVertexAttribArray(
				programInfo.attribLocations.transfIDs);
	}


	// Tell WebGL to use our program when drawing
	gl.useProgram(programInfo.program);

	// Tell WebGL we want to affect texture unit 0
	gl.activeTexture(gl.TEXTURE0);
	// Bind the texture to texture unit 0
	gl.bindTexture(gl.TEXTURE_2D, texture);
	// Tell the shader we bound the texture to texture unit 0
	gl.uniform1i(programInfo.uniformLocations.texSampler, 0);


	gl.activeTexture(gl.TEXTURE1);
	gl.bindTexture(gl.TEXTURE_2D, texTransf);
	gl.uniform1i(programInfo.uniformLocations.transfSampler, 1);

	// envoi des uniform
	gl.uniform4fv(programInfo.uniformLocations.color, [0.8, 0.0, 0.0, 1.0]);
	gl.uniform1i(programInfo.uniformLocations.drawWeight, glDrawWeights);
	gl.uniform1f(programInfo.uniformLocations.nbTransf, nbTransfos);

	{
		const primitiveType = gl.TRIANGLES;
		//const primitiveType = gl.LINE_STRIP;
		//taille du tableau triangles
		const count = tailleTabTri;
		const type = gl.UNSIGNED_INT;
		const offset = 0;
		gl.drawElements(primitiveType, count, type, offset);
	}
	glTimer();
}
*/

//------fonction d'envoi de la texture------
function loadTexture(texture, img) {
	const level = 0;
	const internalFormat = gl.RGBA;
	const srcFormat = gl.RGBA;
	const srcType = gl.UNSIGNED_BYTE;

	//on se bind a la texture
	gl.bindTexture(gl.TEXTURE_2D, texture);
	//on initialise son format
	gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, srcFormat, srcType, img);

	// WebGL1 has different requirements for power of 2 images
	// vs non power of 2 images so check if the image is a
	// power of 2 in both dimensions.
	if (isPowerOf2(img.width) && isPowerOf2(img.height)) {
		// Yes, it's a power of 2. Generate mips.
		gl.generateMipmap(gl.TEXTURE_2D);
	} else {
		// No, it's not a power of 2. Turn of mips and set
		// wrapping to clamp to edge
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	}
}

function isPowerOf2(value) {
	return (value & (value - 1)) == 0;
}

function glSetupModel(model) {
	model.triElementBuffID = gl.createBuffer();
	model.posBuffID = gl.createBuffer();
    model.texCoordBuffID = gl.createBuffer();
    model.dispWeightBuffID = gl.createBuffer();
    model.transfWeightsBuffID = gl.createBuffer();
    model.transfIDBuffID = gl.createBuffer();

    model.texUniformID = gl.createTexture();
	loadTexture(model.texUniformID, model.imgModel);

    model.texTransfUniformID = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, model.texTransfUniformID);
	//on met une texture temporaire car on ne peut pas avoir une texture vide
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	{
	const level = 0;
	const internalFormat = gl.RGB32F;
	const width = 1;
	const height = 2;
	const border = 0;
	const srcFormat = gl.RGB;
	const srcType = gl.FLOAT;
	const initTransf = new Float32Array([1.0, 0.0, 0.0,
										 0.0, 1.0, 0.0,]);
	gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
								width, height, border, srcFormat, srcType,
								initTransf);
	}

	//place l'image dans un canvas invisible pour pouvoir lire ses valeurs
	var canvas = document.createElement("canvas");
	canvas.width = model.imgModel.width;
	canvas.height = model.imgModel.height;
	var ctx = canvas.getContext("2d");
	ctx.drawImage(model.imgModel, 0, 0);

	var canvasWidth = 800;
	var decalageX = Math.round( (canvasWidth - model.imgModel.width) / 2 );
	var decalageY = Math.round( canvasWidth - model.imgModel.height - 10);
	var pixelAlpha; // couleur du pixel actuel
	var points = []; // points du mesh n*2
	var triangles = []; // indices des triangles m*3
	var indTex = []; // indices de texture des points n*2
	var prevLine = []; // indices des points de la ligne precedente
	var currentLine = []; // indices des points de la ligne courante
	var x, y;
	var imgData = ctx.getImageData(0, 0, model.imgModel.width, model.imgModel.height);
	for (y = 0; y < model.imgModel.height; y++) {
		for (x = 0; x < model.imgModel.width; x++) {
			pixelAlpha = imgData.data[(y * model.imgModel.width + x) * 4 + 3];

			//s'il est transparent on met -1 comme indice dans currentLine
			//sinon on le met dans la liste de pts et on retient son indice
			if(pixelAlpha <= 20) {
				currentLine.push(-1);
			} else {
				currentLine.push(points.length / 2);
				points.push(decalageX + x, decalageY + y);
				indTex.push(x/model.imgModel.width, y/model.imgModel.height);
			}

			//si on est pas sur la premiere colonne et ligne on dessine les TRIANGLES
			//pour plus d'info demander a charles de montrer son joli dessin
			if(x!=0 && y!=0) {
				if(currentLine[x] == -1) {
					if(prevLine[x-1] != -1 && prevLine[x] != -1
					&& currentLine[x-1] != -1) {
						//haut gauche
						triangles.push(prevLine[x-1], currentLine[x-1], prevLine[x]);
					}
				} else if(prevLine[x-1] == -1) {
					if(prevLine[x] != -1 && currentLine[x-1] != -1) {
						//bas droite
						triangles.push(prevLine[x], currentLine[x-1], currentLine[x]);
					}
				} else {
					if(prevLine[x] != -1) {
						//haut droite
						triangles.push(prevLine[x-1], currentLine[x], prevLine[x]);
					}
					if(currentLine[x-1] != -1) {
						//bas gauche
						triangles.push(prevLine[x-1], currentLine[x-1], currentLine[x]);
					}
				}
			}
		}
		//passage a la ligne suivante
		prevLine = currentLine.slice();
		currentLine = [];
	}

	model.restMesh = points
	model.indTri = triangles
	model.indTex = indTex

	gl.bindBuffer(gl.ARRAY_BUFFER, model.posBuffID);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(points), gl.STATIC_DRAW);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, model.triElementBuffID);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint32Array(triangles), gl.STATIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, model.texCoordBuffID);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(indTex), gl.STATIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, model.transfWeightsBuffID);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(new Array(points.length * 2).fill(0)), gl.STATIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, model.transfIDBuffID);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(new Array(points.length * 2).fill(0)), gl.STATIC_DRAW);

	glLoadDispWeights(model, new Array(model.restMesh.length/2).fill(0));
}

/*
//crée le mesh du modele a partir de l'image
function createMesh(modele, img) { // DEPRECATED
	//place l'image dans un canvas invisible pour pouvoir lire ses valeurs
	var canvas = document.createElement("canvas");
	canvas.width = img.width;
	canvas.height = img.height;
	var ctx = canvas.getContext("2d");
	ctx.drawImage(img, 0, 0);

	var canvasWidth = 800;
	var decalageX = Math.round( (canvasWidth - img.width) / 2 );
	var decalageY = Math.round( canvasWidth - img.height );
	var pixelAlpha; // couleur du pixel actuel
	var points = []; // points du mesh n*2
	var triangles = []; // indices des triangles m*3
	var indTex = []; // indices de texture des points n*2
	var prevLine = []; // indices des points de la ligne precedente
	var currentLine = []; // indices des points de la ligne courante
	var x, y;
	var imgData = ctx.getImageData(0, 0, img.width, img.height);
	for (y = 0; y < img.height; y++) {
		for (x = 0; x < img.width; x++) {
			pixelAlpha = imgData.data[(y * img.width + x) * 4 + 3];

			//s'il est transparent on met -1 comme indice dans currentLine
			//sinon on le met dans la liste de pts et on retient son indice
			if(pixelAlpha <= 20) {
				currentLine.push(-1);
			} else {
				currentLine.push(points.length / 2);
				points.push(decalageX + x, decalageY + y);
				indTex.push(x/img.width, y/img.height);
			}

			//si on est pas sur la premiere colonne et ligne on dessine les TRIANGLES
			//pour plus d'info demander a charles de montrer son joli dessin
			if(x!=0 && y!=0) {
				if(currentLine[x] == -1) {
					if(prevLine[x-1] != -1 && prevLine[x] != -1
					&& currentLine[x-1] != -1) {
						//haut gauche
						triangles.push(prevLine[x-1], currentLine[x-1], prevLine[x]);
					}
				} else if(prevLine[x-1] == -1) {
					if(prevLine[x] != -1 && currentLine[x-1] != -1) {
						//bas droite
						triangles.push(prevLine[x], currentLine[x-1], currentLine[x]);
					}
				} else {
					if(prevLine[x] != -1) {
						//haut droite
						triangles.push(prevLine[x-1], currentLine[x], prevLine[x]);
					}
					if(currentLine[x-1] != -1) {
						//bas gauche
						triangles.push(prevLine[x-1], currentLine[x-1], currentLine[x]);
					}
				}
			}
		}
		//passage a la ligne suivante
		prevLine = currentLine.slice();
		currentLine = [];
	}

	modele.restMesh = points
	modele.indTri = triangles
	modele.indTex = indTex

	gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(points), gl.STATIC_DRAW);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, trianglesBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint32Array(triangles), gl.STATIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(indTex), gl.STATIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, transfWeightsBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(new Array(points.length * 2).fill(0)), gl.STATIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, transfIDsBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(new Array(points.length * 2).fill(0)), gl.STATIC_DRAW);

	glLoadWeights(new Array(modele.restMesh.length/2).fill(0));
}
*/
/*
function glLoadPoints(points) {
	gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(points), gl.STATIC_DRAW);
}
*/
function glLoadDispWeights(model, weights) {
	gl.bindBuffer(gl.ARRAY_BUFFER, model.dispWeightBuffID);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(weights), gl.STATIC_DRAW);
}

function glLoadTransfWeights(model, weights) {
	if(weights.length == undefined) {
		console.log("glLoadTransfWeights error : tried to load something that's not an array");
		return;
	}
	if(weights.length == 0) {
		console.log("glLoadTransfWeights error : tried to load empty array");
		return;
	}

	var tfw = [];
	var tfID = [];
	var i;
	var j;
	var nbTf;
	for(i = 0; i < weights[0].length; i++) {
		nbTf = 0;
		for(j = 0; j < weights.length; j++) {
			if(weights[j][i] > 0.000001) {
				nbTf++;
				tfw.push(weights[j][i]);
				tfID.push(j);
				if(nbTf == 4)
					break;
			}
		}
		// gérer le cas où on a moins de 4 poids > 0
		for( ; nbTf < 4; nbTf++) {
			tfw.push(0);
			tfID.push(0);
		}
	}

	gl.bindBuffer(gl.ARRAY_BUFFER, model.transfWeightsBuffID);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(tfw), gl.STATIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, model.transfIDBuffID);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(tfID), gl.STATIC_DRAW);
}

function glLoadTransf(model, handles) {
	if(handles.length == undefined) {
		console.log("glLoadTransf error : tried to load something that's not an array");
		return;
	}
	if(handles.length == 0) {
		console.log("glLoadTransf error : tried to load empty array");
		return;
	}

	var l1 = [];
	var l2 = [];
	var nbHandle = 0;
	handles.forEach(function(handle) {
		nbHandle++;
		l1.push(handle.transform[0], handle.transform[3], handle.transform[6]);
		l2.push(handle.transform[1], handle.transform[4], handle.transform[7]);
	});

	model.nbTransf = nbHandle;

	gl.bindTexture(gl.TEXTURE_2D, model.texTransfUniformID);
	const level = 0;
	const internalFormat = gl.RGB32F;
	const width = l1.length / 3;
	const height = 2;
	const border = 0;
	const srcFormat = gl.RGB;
	const srcType = gl.FLOAT;
	const transfs = new Float32Array(l1.concat(l2));
	gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
				  width, height, border, srcFormat, srcType,
				  transfs);
}

//------fonctions d'envoi du programme------
//
// Initialize a shader program, so WebGL knows how to draw our data
//
function initShaderProgram(vsSource, fsSource) {
	const vertexShader = loadShader(gl.VERTEX_SHADER, vsSource);
	const fragmentShader = loadShader(gl.FRAGMENT_SHADER, fsSource);

	// Create the shader program
	const shaderProgram = gl.createProgram();
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);

	// If creating the shader program failed, alert
	if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
		return null;
	}

	return shaderProgram;
}

//
// creates a shader of the given type, uploads the source and
// compiles it.
//
function loadShader(type, source) {
	const shader = gl.createShader(type);

	// Send the source to the shader object
	gl.shaderSource(shader, source);

	// Compile the shader program
	gl.compileShader(shader);

	// See if it compiled successfully
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
		gl.deleteShader(shader);
		return null;
	}

	return shader;
}
