var nextCalqueNumber = 0;
var _idActiveLayer;
var _lLayer = [];
var R;
var alpha;
addLayer();

var astuceMap = new Map();
astuceMap.set("chargerImage", "Commencez par charger un modèle à partir d'une image png avec fond transparent ou un calque avec un fichier .anmm en cliquant sur le bouton ci-dessus.");
astuceMap.set("ajouterSDC", "Cliquez sur Ajouter puis cliquez sur le modèle pour construire votre structure de contrôle. Changez de structure de contrôle en cliquant sur l'onglet correspondant.");
astuceMap.set("modifierSDC", "Vous pouvez déplacer ou supprimer des poignées avec les boutons respectifs.");
astuceMap.set("validerSDC", "Lorsque votre structure de contrôle est terminée, cliquez sur Fin d'édition pour la valider.");
astuceMap.set("poids", "Avant d'animer votre modèle, il faut définir des poids. Vous pouvez les générer automatiquement ou bien les peindre.");
astuceMap.set("poidsAutoCP","Les poids générés automatiquement peuvent présenter des défauts, vous pouvez les rectifier manuellement avec Peindre ou déformez votre modèle à l'aide du clic gauche de la souris et de la molette pour interagir avec les poignées.");
astuceMap.set("poidsAuto","Les poids générés automatiquement peuvent présenter des défauts, vous pouvez les rectifier manuellement avec Peindre ou déformez votre modèle à l'aide du clic gauche de la souris pour interagir avec les poignées.");
astuceMap.set("poidsAutoCage","Il n'est pas possible de rectifier les poids d'une cage avec l'outil de peinture. Déformez votre modèle à l'aide du clic gauche de la souris pour interagir avec les poignées.");
astuceMap.set("peinture", "Choisissez le diamètre et le dégradé de votre pinceau puis peignez les poids de la poignée sélectionnée avec la souris, validez avec Valider poids.");
astuceMap.set("gomme","Choisissez le diamètre de la gomme et gommez les poids de la poignée sélectionnée avec la souris.");
astuceMap.set("deformation","Peignez les poids pour d'autres poignées ou déformez votre modèle à l'aide du clic gauche de la souris sur les poignées.");
astuceMap.set("deformationCP","Peignez les poids pour d'autres poignées ou déformez votre modèle à l'aide du clic gauche de la souris et de la molette pour faire bouger les poignées.");
astuceMap.set("animBspline","Vous pouvez aussi définir des trajectoires pour vos poignées en allant dans le mode Animation Par Bspline en cliquant sur l'onglet.");
astuceMap.set("animSdC","Choisissez la structure de contrôle que vous souhaitez animer par Bspline. Ajoutez ensuite des points de contrôle pour créer une Bspline en sélectionnant une poignée et en cliquant sur Ajouter et dans la fenêtre. Vous pouvez les éditer de la même manière que les poignées..");
astuceMap.set("finEditerBspline","Lorsque votre Bspline est terminée, validez la avec Fin d'édition.");
astuceMap.set("validateBspline","Choisissez la durée et l'ordre de la Bspline (la durée sera la même pour toutes les Bsplines).");
astuceMap.set("optionsBspline","Une fois la Bspline validée, il est possible de la faire boucler sur elle-même et de la positionner sur la poignée sélectionnée. Ensuite, appuyez sur Play / Pause pour lancer l'animation et Stop pour l'arrêter.");
astuceMap.set("More","A présent, vous pouvez par exemple ajouter d'autres calques et animer plusieurs modèles en même temps, de la même manière. N'oubliez pas qu'il est possible d'importer et exporter les calques !");
setAstuce("chargerImage");

function setAstuce(nomAstuce) {
	var texteAstuce = astuceMap.get(nomAstuce);
	if(texteAstuce != undefined) {
		$("#astuceID").text(texteAstuce);
	} else {
		$("#astuceID").text("Erreur : astuce inconnue");
	}
}

function addLayer() {
  var nouvCalque = new Layer();
  nouvCalque.name = "nouveau calque " + (nextCalqueNumber++);
  _lLayer.push(nouvCalque);
  setActiveLayer(_lLayer.length - 1);
  updateCalques();
}

function setActiveLayer(id) {
	_idActiveLayer = id;
	if(_lLayer[_idActiveLayer].model != null) {
		glDrawOne(_lLayer[_idActiveLayer].model);
		if(_modeH == modeHandler.CP) {
			drawPoint(_lLayer[_idActiveLayer].model);
		} else if(_modeH == modeHandler.Skeleton) {
			drawSkeleton(_lLayer[_idActiveLayer].model);
		} else if(_modeH == modeHandler.Cage) {
			if (_lLayer[_idActiveLayer].model.cage._faces.length == 0) {
				drawCage(_lLayer[_idActiveLayer].model);
			} else {
				drawTrianglesCage(_lLayer[_idActiveLayer].model);
			}
		} else if(_modeH == modeHandler.AnimByBSpline) {
			switch (Animation.modeHBSpline) {
			case modeHandler.CP:
				drawPoint(_lLayer[_idActiveLayer].model);
				break;

			case modeHandler.Skeleton:
				drawSkeleton(_lLayer[_idActiveLayer].model);
				break;

			case modeHandler.Cage:
				if (_lLayer[_idActiveLayer].model.cage._faces.length == 0) {
					drawCage(_lLayer[_idActiveLayer].model);
				} else {
					drawTrianglesCage(_lLayer[_idActiveLayer].model);
				}
				break;
			}
		}
	} else {
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		ctx2d.clearRect(0,0,800,800);
		// TODO DESSINER DU BLANC
	}
	updateCalques();
}

function removeLayer(id) {
  if(id >= _lLayer.length || id < 0)
  	return;

  if(_idActiveLayer >= id)
    setActiveLayer(_idActiveLayer - 1);

  _lLayer.splice(id, 1);

  if(_lLayer.length == 0) {
    nextCalqueNumber = 0;
    addLayer();
  }

  updateCalques();
}

function moveLayerUp(id) {
  // Check id
  if(id >= _lLayer.length || id <= 0) {
    return;
  }
  // Permute
  tmpLayer = _lLayer.splice(id, 1);
  _lLayer.splice(id - 1, 0, tmpLayer[0]);
  // Change active layer if it was affected
  if(_idActiveLayer == id) {
    setActiveLayer(id - 1);
  }
  else if(_idActiveLayer == id - 1) {
    setActiveLayer(id);
  }
  else {
    updateCalques();
  }
}

function moveLayerDown(id) {
  // Check id
  if(id >= _lLayer.length-1 || id < 0) {
    return;
  }
  // Permute
  tmpLayer = _lLayer.splice(id, 1);
  _lLayer.splice(id + 1, 0, tmpLayer[0]);
  // Change active layer if it was affected
  if(_idActiveLayer == id) {
    setActiveLayer(id + 1);
  }
  else if(_idActiveLayer == id + 1) {
    setActiveLayer(id);
  }
  else {
    updateCalques();
  }
}


function renameLayer(id, nom) {
  _lLayer[id].name = nom;
  updateCalques();
}

function exportLayer(id) {
	// Model DEEP copy (clone instead of copying references) using jQuery
	var copy = $.extend(true, {}, _lLayer[id].model);

	// Remove CP pos, transform, angle and BS vector
	copy.cp._cp = copy.cp._cp.map(function (sCP) {
		sCP.pos = null;
		sCP.transform = null;
		sCP.angle = 0;
		sCP.BS.nodalVector = null;
		return sCP;
	});

	// Remove Skeleton pos, transform, angle and BS vector
	copy.skeleton._bones = copy.skeleton._bones.map(function (bone) {
		bone.transform = null;
		bone.angle = 0;
		return bone;
	});
	copy.skeleton._joints = copy.skeleton._joints.map(function (joint) {
		joint.pos = null;
		joint.BS.nodalVector = null;
		return joint;
	});
	// Remove Cages pos, transform, angle and BS vector
	copy.cage._cage = copy.cage._cage.map(function (cageVertex) {
		cageVertex.pos = null;
		cageVertex.transform = null;
		cageVertex.BS.nodalVector = null;
		return cageVertex;
	});

	// Remove mesh and indices
  copy.restMesh = [];
  copy.currentMesh = [];
  copy.indTri = [];

	// Need to convert image from ObjectURL blob to base64 first
	var canvas = document.createElement("canvas");
	canvas.width = _lLayer[id].model.imgModel.width;
	canvas.height = _lLayer[id].model.imgModel.height;
	var ctx = canvas.getContext("2d");
	ctx.drawImage(_lLayer[id].model.imgModel, 0, 0);
	var dataURL = canvas.toDataURL("image/png");
  copy.imgModel = dataURL;

	// Buffers reset
  copy.triElementBuffID = -1;
  copy.posBuffID = -1;
  copy.texCoordBuffID = -1;
  copy.dispWeightBuffID = -1;
  copy.transfWeightsBuffID = -1;
  copy.transfIDBuffID = -1;
  copy.texUniformID = -1;
  copy.texTransfUniformID = -1;
  copy.nbTransf = 0;

	// Data
	var json = JSON.stringify(copy);
	var blob = new Blob([json], {type: "application/json"});
	var url  = URL.createObjectURL(blob);
	var fileName = _lLayer[id].name + ".anmm";

	// Download by creating a link tag (<a/>)
	var dl = document.createElement('a');
	dl.download    = fileName;
	dl.href        = url;
	document.body.appendChild(dl);
	dl.click();
	document.body.removeChild(dl);

	console.log("Layer saved to " + fileName)
}

function clearLayers() {
  _lLayer.splice(0, _lLayer.length);
  nextCalqueNumber = 0;
  addLayer();
  updateCalques();
}

function setEditMode(id) {
  $("#pcalque"+id).css('font-size', '0');
  $("#brename"+id).css('display', 'none');
  $("#rename"+id).css('display', 'inline');

  $("#rename"+id).bind("keypress", {}, keypressInBox);

  function keypressInBox(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) { //Enter keycode
      e.preventDefault();
      //console.log("t bo ;)");
      renameLayer(id, $("#rename"+id).val());
    }
  };
}

function updateCalques() {
  $("#idC").empty();
  var i;
  var styleString;
  for(i=0; i < _lLayer.length; i++) {
    if(i == _idActiveLayer) {
			//; text-align:left\"
      styleString = "<li style=\"color:#0404B4; text-align:left\"><p id=\"pcalque" + i + "\">"
      + "<input type=\"radio\" name=\"calques\" id=\"radiocalque" + i + "\" checked=\"checked\" >";
    } else {
      styleString = "<li style=\"text-align:left\"><p id=\"pcalque" + i + "\">"
      + "<input type=\"radio\" name=\"calques\" id=\"radiocalque" + i + "\" onclick=\"setActiveLayer(" + i + ");\">";
    }
    $("#idC").append(styleString
      + "<label for=\"radiocalque" + i + "\">" + _lLayer[i].name + "</label> "
      + "<input type=\"text\" value=\"" + _lLayer[i].name + "\" id=\"rename" + i + "\" style=\"display:none\"> "
      + "<input type=\"button\" value = \"Renommer\" id=\"brename" + i + "\" onclick=\"setEditMode(" + i + ");\" > "
      + "<input type=\"button\" value = \"▲\" onclick=\"moveLayerUp(" + i + ");\" style=\"width:38px\"> "
      + "<input type=\"button\" value = \"▼\" onclick=\"moveLayerDown(" + i + ");\" style=\"width:38px\"> "
      + "<input type=\"button\" value = \"❌\" onclick=\"removeLayer(" + i + ");\" style=\"width:38px\"> "
			+ "<input type=\"button\" value = \"📎\" onclick=\"exportLayer(" + i + ");\" style=\"width:38px\"> "
      + "</p></li>");
  }
}
