const modeHandler = {
  CP: "pointsControle",
  Skeleton: "skeleton",
  Cage: "cage",
  AnimByBSpline: "animByBSpline"
};

const modeEdition = {
  Add: "add",
  Delete: "delete",
  Move: "move",
  Deform: "deform",
  Paint: "paint",
  Erase: "erase"
};

var _modeH = modeHandler.CP;
var _modeE = modeEdition.Add;

//var m = new Model();

function changeStateHandler(id) {
	if(_lLayer[_idActiveLayer].model == null) {
		console.log("t'es nul");
		return;
	}

	if (id == 0) {
		setAstuce("ajouterSDC");
		document.getElementById('pointsControle').style.display = 'block';
		document.getElementById('squelette').style.display = 'none';
		document.getElementById('cage').style.display = 'none';
		document.getElementById('animation').style.display = 'none';
		$("#menuPdC").css("background-color", "#555");
		$("#menuSkeleton").css("background-color", "#333");
		$("#menuCage").css("background-color", "#333");
		$("#menuBSpline").css("background-color", "#333");
		_modeH = modeHandler.CP;
		console.log(_modeH);
		Animation.isPlaying = false;
		if (_lLayer[_idActiveLayer].model.cp._cp.length == 0) {
			changeStateEdition(modeEdition.Add);
			//poids à zero s'il n'y a pas de poignee pour pouvoir dessiner la restpose
			glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
			glLoadTransfWeights(_lLayer[_idActiveLayer].model, [new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0)]);
			//on peut garder les ancienne transfo, de toutes façon les poids sont 0
		} else {
			changeStateEdition(modeEdition.Deform);
			glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP]);
			glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCP);
			glLoadTransf(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.cp._cp);
		}
		glDrawOne(_lLayer[_idActiveLayer].model);
		drawPoint(_lLayer[_idActiveLayer].model);

	} else if (id == 1) {
		setAstuce("ajouterSDC");
		document.getElementById('pointsControle').style.display = 'none';
		document.getElementById('squelette').style.display = 'block';
		document.getElementById('cage').style.display = 'none';
		document.getElementById('animation').style.display = 'none';
		$("#menuPdC").css("background-color", "#333");
		$("#menuSkeleton").css("background-color", "#555");
		$("#menuCage").css("background-color", "#333");
		$("#menuBSpline").css("background-color", "#333");
		_modeH = modeHandler.Skeleton;
		console.log(_modeH);
		Animation.isPlaying = false;
		if (_lLayer[_idActiveLayer].model.skeleton._joints.length == 0) {
			changeStateEdition(modeEdition.Add);
			glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
			glLoadTransfWeights(_lLayer[_idActiveLayer].model, [new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0)]);
		} else {
			changeStateEdition(modeEdition.Deform);
			if (_lLayer[_idActiveLayer].model.skeleton._currentJoint == 0)
				glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
			else
				glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton[_lLayer[_idActiveLayer].model.skeleton._currentBone]);
			glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsSkeleton);
			glLoadTransf(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.skeleton._bones);
		}
		glDrawOne(_lLayer[_idActiveLayer].model);
		drawSkeleton(_lLayer[_idActiveLayer].model);

	} else if (id == 2) {
		setAstuce("ajouterSDC");
		document.getElementById('pointsControle').style.display = 'none';
		document.getElementById('squelette').style.display = 'none';
		document.getElementById('cage').style.display = 'block';
		document.getElementById('animation').style.display = 'none';
		$("#menuPdC").css("background-color", "#333");
		$("#menuSkeleton").css("background-color", "#333");
		$("#menuCage").css("background-color", "#555");
		$("#menuBSpline").css("background-color", "#333");
		_modeH = modeHandler.Cage;
		console.log(_modeH);
		Animation.isPlaying = false;
		if (_lLayer[_idActiveLayer].model.cage._cage.length == 0) {
			changeStateEdition(modeEdition.Add);
			glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
			glLoadTransfWeights(_lLayer[_idActiveLayer].model, [new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0)]);
		} else {
			changeStateEdition(modeEdition.Deform);
			glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCage[_lLayer[_idActiveLayer].model.cage._currentVertex]);
			glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCage);
			glLoadTransf(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.cage._cage);
		}
		glDrawOne(_lLayer[_idActiveLayer].model);
		if (_lLayer[_idActiveLayer].model.cage._faces.length == 0) {
			drawCage(_lLayer[_idActiveLayer].model);
		} else {
			drawTrianglesCage(_lLayer[_idActiveLayer].model);
		}

	} else if (id == 3) {
		setAstuce("animSdC");
		document.getElementById('pointsControle').style.display = 'none';
		document.getElementById('squelette').style.display = 'none';
		document.getElementById('cage').style.display = 'none';
		document.getElementById('animation').style.display = 'block';
		$("#menuPdC").css("background-color", "#333");
		$("#menuSkeleton").css("background-color", "#333");
		$("#menuCage").css("background-color", "#333");
		$("#menuBSpline").css("background-color", "#555");
		if(_modeH != modeHandler.AnimByBSpline)
			Animation.modeHBSpline = _modeH;
		_modeH = modeHandler.AnimByBSpline;
		changeStateEdition(modeEdition.Deform);
		// Update mode handler radio
		$("#animation_Radio_CP").prop("checked", false);
		$("#animation_Radio_Skeleton").prop("checked", false);
		$("#animation_Radio_Cage").prop("checked", false);
		$("#animation_Time").prop("value", 0);
		switch (Animation.modeHBSpline) {
		case modeHandler.CP:
			$("#animation_Radio_CP").prop("checked", true);
			_lLayer.forEach(function(layer) {
				if (layer.model.cp._cp.length != 0)
					resetPoseCP(layer.model);
			});
			break;

		case modeHandler.Skeleton:
			$("#animation_Radio_Skeleton").prop("checked", true);
			_lLayer.forEach(function(layer) {
				if (layer.model.skeleton._joints.length != 0)
					resetPoseSkeleton(layer.model);
			});
			break;

		case modeHandler.Cage:
			$("#animation_Radio_Cage").prop("checked", true);
			_lLayer.forEach(function(layer) {
				if (layer.model.cage._cage.length != 0)
					resetPoseCage(layer.model);
			});
			break;
		}
		ctx2d.clearRect(0,0,800,800);
		glDrawOne(_lLayer[_idActiveLayer].model);
		console.log(_modeH);

	}
}

function changeStateEdition(m) {
	if(_lLayer[_idActiveLayer].model == null) return;
	if (m == modeEdition.Add ) {
		_modeE = modeEdition.Add;
		$("#glCanvas").css("cursor", "auto");

	} else if (m == modeEdition.Move) {
		_modeE = modeEdition.Move;
		$("#glCanvas").css("cursor", "auto");

	} else if (m == modeEdition.Delete) {
		_modeE = modeEdition.Delete;
		$("#glCanvas").css("cursor", "auto");

	} else if (m == modeEdition.Deform) {
		_modeE = modeEdition.Deform;
		$("#glCanvas").css("cursor", "auto");

	} else if (m == modeEdition.Paint) {
		_modeE = modeEdition.Paint;
		$("#glCanvas").css("cursor", "none");

	} else if (m == modeEdition.Erase) {
		_modeE = modeEdition.Erase;
		$("#glCanvas").css("cursor", "none");

	}
  //console.log(_modeE);
}
