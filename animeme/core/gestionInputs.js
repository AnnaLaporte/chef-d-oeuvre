$("#file_input").change(function(e) {
	var URL = window.webkitURL || window.URL;
	// Check if file is an image or an exported layer
	var filename = e.target.files[0].name;
	var extension = filename.split(".").slice(-1)[0];
	if (extension == "anmm") {
		// LOAD A LAYER
		_modeE = modeEdition.Deform;
		var reader = new FileReader();
		// Listener called when the file is successfully loaded
		reader.addEventListener("loadend", function() {
			console.log("Loading layer " + filename)
			// Update current layer
			var importedLayer = JSON.parse(reader.result);
			renameLayer(_idActiveLayer, filename.substring(0, filename.length-5));

			// Re-allocate CP, cast restPos of "Object" type into "Float32Array" with glMatrix
			importedLayer.cp._cp = importedLayer.cp._cp.map(function (sCP) {
				sCP.restPos = glMatrix.vec2.fromValues(sCP.restPos[0], sCP.restPos[1]);
				sCP.pos = glMatrix.vec2.clone(sCP.restPos);
				sCP.transform = glMatrix.mat3.create();
				sCP.BS.points = sCP.BS.points.map(function (bsPoint) {
					return glMatrix.vec2.fromValues(bsPoint[0], bsPoint[1]);
				});
				sCP.BS.nodalVector = [];
		    for(i = 0; i < sCP.BS.points.length + sCP.BS.k; ++i) {
		        sCP.BS.nodalVector.push(i - sCP.BS.k - 1);
		    }
				return sCP;
			});
			// Re-allocate Skeleton
			importedLayer.skeleton._bones = importedLayer.skeleton._bones.map( function(bone) {
				bone.transform = glMatrix.mat3.create();
				return bone;
			});
			importedLayer.skeleton._joints = importedLayer.skeleton._joints.map( function(joint) {
				joint.restPos = glMatrix.vec2.fromValues(joint.restPos[0], joint.restPos[1]);
				joint.pos = glMatrix.vec2.clone(joint.restPos);
				joint.BS.points = joint.BS.points.map(function (bsPoint) {
					return glMatrix.vec2.fromValues(bsPoint[0], bsPoint[1]);
				});
				joint.BS.nodalVector = [];
				for(i = 0; i < joint.BS.points.length + joint.BS.k; ++i) {
		        joint.BS.nodalVector.push(i - joint.BS.k - 1);
		    }
				return joint;
			});
			// Re-allocate Cage
			importedLayer.cage._cage = importedLayer.cage._cage.map( function(cageVertex) {
				cageVertex.restPos = glMatrix.vec2.fromValues(cageVertex.restPos[0], cageVertex.restPos[1]);
				cageVertex.pos = glMatrix.vec2.clone(cageVertex.restPos);
				cageVertex.transform = glMatrix.mat3.create();
				cageVertex.BS.points = cageVertex.BS.points.map(function (bsPoint) {
					return glMatrix.vec2.fromValues(bsPoint[0], bsPoint[1]);
				});
				cageVertex.BS.nodalVector = [];
				for(i = 0; i < cageVertex.BS.points.length + cageVertex.BS.k; ++i) {
		        cageVertex.BS.nodalVector.push(i - cageVertex.BS.k - 1);
		    }
				return cageVertex;
			});

			// Load or recreate the mesh and display, hacky style
			//_lLayer[_idActiveLayer].model = new Model();
			_lLayer[_idActiveLayer].model = importedLayer;

			// Image is stored in b64, convert to blob then get ObjectURL to use for img.src
			fetch(importedLayer.imgModel).then(res => res.blob()).then(function (blob) {
				var url = URL.createObjectURL(blob);
				var img = new Image();
				img.src = url;
				img.onload = function() {
					_lLayer[_idActiveLayer].model.imgModel = img;

					// Recreate mesh, indices, tris, buffers...
					glSetupModel(_lLayer[_idActiveLayer].model);

					// Load according weights and transforms
					switch(_modeH) {
					case modeHandler.CP:
						if (_lLayer[_idActiveLayer].model.cp._cp.length == 0) {
							glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
							glLoadTransfWeights(_lLayer[_idActiveLayer].model, [new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0)]);
						} else {
							glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP]);
							glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCP);
							glLoadTransf(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.cp._cp);
						}
						drawPoint(_lLayer[_idActiveLayer].model);
						break;
					case modeHandler.Skeleton:
						if (_lLayer[_idActiveLayer].model.skeleton._bones.length == 0) {
							glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
							glLoadTransfWeights(_lLayer[_idActiveLayer].model, [new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0)]);
						} else {
							glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton[_lLayer[_idActiveLayer].model.skeleton._currentBone]);
							glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsSkeleton);
							glLoadTransf(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.skeleton._bones);
						}
						drawSkeleton(_lLayer[_idActiveLayer].model);
						break;
					case modeHandler.Cage:
						if (_lLayer[_idActiveLayer].model.cage._cage.length == 0) {
							glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
							glLoadTransfWeights(_lLayer[_idActiveLayer].model, [new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0)]);
						} else {
							glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCage[_lLayer[_idActiveLayer].model.cage._currentVertex]);
							glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCage);
							glLoadTransf(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.cage._cage);
						}
						if (_lLayer[_idActiveLayer].model.cage._faces.length == 0) {
							drawCage(_lLayer[_idActiveLayer].model);
						} else {
							drawTrianglesCage(_lLayer[_idActiveLayer].model);
						}
						break;
					case modeHandler.AnimByBSpline:
						switch(Animation.modeHBSpline) {
						case modeHandler.CP:
							if (_lLayer[_idActiveLayer].model.cp._cp.length == 0) {
								glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
								glLoadTransfWeights(_lLayer[_idActiveLayer].model, [new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0)]);
							} else {
								glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP]);
								glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCP);
								glLoadTransf(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.cp._cp);
							}
							drawPoint(_lLayer[_idActiveLayer].model);
							afficherBSpline(_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].BS);
							break;
						case modeHandler.Skeleton:
							if (_lLayer[_idActiveLayer].model.skeleton._bones.length == 0) {
								glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
								glLoadTransfWeights(_lLayer[_idActiveLayer].model, [new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0)]);
							} else {
								glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton[_lLayer[_idActiveLayer].model.skeleton._currentBone]);
								glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsSkeleton);
								glLoadTransf(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.skeleton._bones);
							}
							drawSkeleton(_lLayer[_idActiveLayer].model);
							afficherBSpline(_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._currentJoint].BS);
							break;
						case modeHandler.Cage:
							if (_lLayer[_idActiveLayer].model.cage._cage.length == 0) {
								glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
								glLoadTransfWeights(_lLayer[_idActiveLayer].model, [new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0)]);
							} else {
								glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCage[_lLayer[_idActiveLayer].model.cage._currentVertex]);
								glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCage);
								glLoadTransf(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.cage._cage);
							}
							if (_lLayer[_idActiveLayer].model.cage._faces.length == 0) {
								drawCage(_lLayer[_idActiveLayer].model);
							} else {
								drawTrianglesCage(_lLayer[_idActiveLayer].model);
								afficherBSpline(_lLayer[_idActiveLayer].model.cage._cage[_lLayer[_idActiveLayer].model.cage._currentVertex].BS);
							}
							break;
						}
						break;
					default:
						break;
					}

					// Drawcall
					glSetDrawWeights($("#checkboxPoids").prop('checked'));
					glDrawOne(_lLayer[_idActiveLayer].model);
				} // Close image callback
			}); // Close fetch callback
		}); // Close reader callback

		// Actual reader function to change according to format of data (text, binary, Array, ObjectUrl)
		reader.readAsText(e.target.files.item(0));
	}
	else {
		// LOAD AN IMAGE
		console.log("Loading image " + filename)
		var url = URL.createObjectURL(e.target.files[0]);

		var img = new Image();
		img.src = url;
		//console.log("chargement de l'image");

		img.onload = function() {
			_lLayer[_idActiveLayer].model = new Model();
			_lLayer[_idActiveLayer].model.imgModel = img;

			initTabWeights();

			glSetupModel(_lLayer[_idActiveLayer].model);
			glDrawOne(_lLayer[_idActiveLayer].model);
			ctx2d.clearRect(0,0,800,800);

			setAstuce("ajouterSDC");
		}
	}
});

$("#demoPic").click(function(e) {
	var img = new Image();
	img.src = "testLink.png";
	//console.log("chargement de l'image");

	img.onload = function() {
		_lLayer[_idActiveLayer].model = new Model();
		_lLayer[_idActiveLayer].model.imgModel = img;

		initTabWeights();

		glSetupModel(_lLayer[_idActiveLayer].model);
		glDrawOne(_lLayer[_idActiveLayer].model);
		ctx2d.clearRect(0,0,800,800);

		setAstuce("ajouterSDC");
	}
});

/* Control Points */
$("#addCP").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (_lLayer[_idActiveLayer].model.cp._cp.length != 0) {
		resetPoseCP(_lLayer[_idActiveLayer].model);
	}
	changeStateEdition(modeEdition.Add);
});

$("#moveCP").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (_lLayer[_idActiveLayer].model.cp._cp.length != 0) {
		resetPoseCP(_lLayer[_idActiveLayer].model);
	}
	changeStateEdition(modeEdition.Move);
});

$("#editedCP").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	changeStateEdition(modeEdition.Deform);
	resetPoseCP(_lLayer[_idActiveLayer].model);
	setAstuce("poids");
});

$("#deleteCP").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (_lLayer[_idActiveLayer].model.cp._cp.length != 0) {
		resetPoseCP(_lLayer[_idActiveLayer].model);
	}
	changeStateEdition(modeEdition.Delete);
});

$("#disInvCP").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	Weight.calculateInverse(_lLayer[_idActiveLayer].model, modeHandler.CP);
	resetPoseCP(_lLayer[_idActiveLayer].model);
	glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP]);
	glSetDrawWeights($("#checkboxPoids").prop('checked'));
	setAstuce("poidsAutoCP");
});

$("#biharmoCP").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	console.log("FONCTION NON SUPPORTEE");
	/*
	resetPoseCP(_lLayer[_idActiveLayer].model);
	Weight.calculateConstrainedWeights(_lLayer[_idActiveLayer].model, modeHandler.CP);
	glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP]);
	glSetDrawWeights($("#checkboxPoids").prop('checked'));
	*/
});

$("#suppCompact").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	Weight.calculateCompact(_lLayer[_idActiveLayer].model, modeHandler.CP, Number($("#R").val()), Number($("#alpha").val())) ;
	resetPoseCP(_lLayer[_idActiveLayer].model);
	glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP]);
	glSetDrawWeights($("#checkboxPoids").prop('checked'));
	setAstuce("poidsAutoCP");
 });

$("#paintCP").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; }
	if (_lLayer[_idActiveLayer].model.cp._cp.length != 0) {
		resetPoseCP(_lLayer[_idActiveLayer].model);
	}
	changeStateEdition(modeEdition.Paint);
	setAstuce("peinture");

	if (_lLayer[_idActiveLayer].model.cp._cp.length == 0) {
		glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
	} else {
		glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP]);
	}
	glSetDrawWeights(true);
	$('#checkboxPoids').prop('checked', true);
});

$("#eraseCP").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; }
	if (_lLayer[_idActiveLayer].model.cp._cp.length != 0) {
		resetPoseCP(_lLayer[_idActiveLayer].model);
	}
	changeStateEdition(modeEdition.Erase);
	setAstuce("gomme");
	if (_lLayer[_idActiveLayer].model.cp._cp.length == 0) {
		glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
	} else {
		glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP]);
	}
	glSetDrawWeights(true);
	$('#checkboxPoids').prop('checked', true);
});

$("#validWeightCP").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	changeStateEdition(modeEdition.Deform);
	Weight.normalizeWeights(_lLayer[_idActiveLayer].model, _modeH);
	setAstuce("deformationCP");
});

$("#refPoseCP").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	resetPoseCP(_lLayer[_idActiveLayer].model);
	setAstuce("animBspline");
});

/* Skeleton */
$("#addSkeleton").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (_lLayer[_idActiveLayer].model.skeleton._joints.length != 0) {
		resetPoseSkeleton(_lLayer[_idActiveLayer].model);
	}
	changeStateEdition(modeEdition.Add);
});

$("#moveSkeleton").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (_lLayer[_idActiveLayer].model.skeleton._joints.length != 0) {
		resetPoseSkeleton(_lLayer[_idActiveLayer].model);
	}
	changeStateEdition(modeEdition.Move);
});

$("#editedSkeleton").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	changeStateEdition(modeEdition.Deform);
	resetPoseSkeleton(_lLayer[_idActiveLayer].model);
	setAstuce("poids");
});

$("#deleteSkeleton").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (_lLayer[_idActiveLayer].model.skeleton._joints.length != 0) {
		resetPoseSkeleton(_lLayer[_idActiveLayer].model);
	}
	changeStateEdition(modeEdition.Delete);
});

$("#disInvSkeleton").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	Weight.calculateInverse(_lLayer[_idActiveLayer].model, modeHandler.Skeleton);
	Weight.normalizeWeights(_lLayer[_idActiveLayer].model, _modeH);
	resetPoseSkeleton(_lLayer[_idActiveLayer].model);
	if (_lLayer[_idActiveLayer].model.skeleton._currentBone != -1) {
		glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton[_lLayer[_idActiveLayer].model.skeleton._currentBone]);
	}
	glSetDrawWeights($("#checkboxPoids").prop('checked'));
	setAstuce("poidsAuto");
});

$("#biharmoSkeleton").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	Weight.calculateBiharmonic(_lLayer[_idActiveLayer].model, modeHandler.Skeleton);
	resetPoseSkeleton(_lLayer[_idActiveLayer].model);
	glSetDrawWeights($("#checkboxPoids").prop('checked'));
});

$("#paintSkeleton").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (_lLayer[_idActiveLayer].model.skeleton._joints.length != 0) {
		resetPoseSkeleton(_lLayer[_idActiveLayer].model);
	}
	changeStateEdition(modeEdition.Paint);
	setAstuce("peinture");
	if (_lLayer[_idActiveLayer].model.skeleton._bones.length == 0) {
		glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
	} else {
		if (_lLayer[_idActiveLayer].model.skeleton._currentJoint == 0)
			glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
		else
			glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton[_lLayer[_idActiveLayer].model.skeleton._currentBone]);
	}
	glSetDrawWeights(true);
	$('#checkboxPoids').prop('checked', true);
});

$("#eraseSkeleton").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (_lLayer[_idActiveLayer].model.skeleton._joints.length != 0) {
		resetPoseSkeleton(_lLayer[_idActiveLayer].model);
	}
	changeStateEdition(modeEdition.Erase);
	setAstuce("gomme");
	if (_lLayer[_idActiveLayer].model.skeleton._bones.length == 0) {
		glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
	} else {
		if (_lLayer[_idActiveLayer].model.skeleton._currentJoint == 0)
			glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
		else
			glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton[_lLayer[_idActiveLayer].model.skeleton._currentBone]);
	}
	glSetDrawWeights(true);
	$('#checkboxPoids').prop('checked', true);
});

$("#validWeightSkeleton").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	changeStateEdition(modeEdition.Deform);
	Weight.normalizeWeights(_lLayer[_idActiveLayer].model, _modeH);
	setAstuce("deformation");
});

$("#refPoseSkeleton").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	resetPoseSkeleton(_lLayer[_idActiveLayer].model);
	setAstuce("animBspline");
});

/* Cage */
$("#addCage").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (_lLayer[_idActiveLayer].model.cage._cage.length != 0) {
		resetPoseCage(_lLayer[_idActiveLayer].model);
	}
	drawCage(_lLayer[_idActiveLayer].model);
	changeStateEdition(modeEdition.Add);
});

$("#moveCage").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (_lLayer[_idActiveLayer].model.cage._cage.length != 0) {
		resetPoseCage(_lLayer[_idActiveLayer].model);
	}
	drawCage(_lLayer[_idActiveLayer].model);
	changeStateEdition(modeEdition.Move);
});

$("#editedCage").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	resetPoseCage(_lLayer[_idActiveLayer].model);
	changeStateEdition(modeEdition.Deform);
	determineWayCage(_lLayer[_idActiveLayer].model);
	drawTrianglesCage(_lLayer[_idActiveLayer].model);
	setAstuce("poids");
});

$("#deleteCage").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (_lLayer[_idActiveLayer].model.cage._cage.length != 0) {
		resetPoseCage(_lLayer[_idActiveLayer].model);
	}
	drawCage(_lLayer[_idActiveLayer].model);
	changeStateEdition(modeEdition.Delete);
});


$("#baryCage").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model

	changeStateEdition(modeEdition.Deform);

	determineWayCage(_lLayer[_idActiveLayer].model);
	drawTrianglesCage(_lLayer[_idActiveLayer].model);

	Weight.calculateBarycentric(_lLayer[_idActiveLayer].model, modeHandler.Cage);
	Weight.normalizeWeights(_lLayer[_idActiveLayer].model, _modeH);

	resetPoseCage(_lLayer[_idActiveLayer].model);
	glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCage[_lLayer[_idActiveLayer].model.cage._currentVertex]);
	glSetDrawWeights($("#checkboxPoids").prop('checked'));
	setAstuce("poidsAutoCage");
});

$("#refPoseCage").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	resetPoseCage(_lLayer[_idActiveLayer].model);
	setAstuce("animBspline");
});


/* BSpline */
// ModeHandler Radio
$("#animation_Radio_CP").change(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	Animation.setModeHBSpline(modeHandler.CP);
	_lLayer.forEach(function(layer) {
		if (layer.model.cp._cp.length == 0) {
		//poids à zero s'il n'y a pas de poignee pour pouvoir dessiner la restpose
		glLoadDispWeights(layer.model, new Array(layer.model.restMesh.length/2).fill(0));
		glLoadTransfWeights(layer.model, [new Array(layer.model.restMesh.length/2).fill(0)]);
		//on peut garder les ancienne transfo, de toutes façon les poids sont 0
	} else {
		glLoadDispWeights(layer.model, layer.model.unnormalizedWeightsCP[layer.model.cp._currentCP]);
		glLoadTransfWeights(layer.model, layer.model.normalizedWeightsCP);
		glLoadTransf(layer.model, layer.model.cp._cp);
		resetPoseCP(layer.model);
	}});
	glDrawOne(_lLayer[_idActiveLayer].model);
	drawPoint(_lLayer[_idActiveLayer].model);
	setAstuce("animSdC");
});

$("#animation_Radio_Skeleton").change(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	Animation.setModeHBSpline(modeHandler.Skeleton);
	_lLayer.forEach(function(layer) {
		if (layer.model.skeleton._joints.length == 0) {
			glLoadDispWeights(layer.model, new Array(layer.model.restMesh.length/2).fill(0));
			glLoadTransfWeights(layer.model, [new Array(layer.model.restMesh.length/2).fill(0)]);
		} else {
			if (layer.model.skeleton._currentJoint == 0)
				glLoadDispWeights(layer.model, new Array(layer.model.restMesh.length/2).fill(0));
			else
				glLoadDispWeights(layer.model, layer.model.unnormalizedWeightsSkeleton[layer.model.skeleton._currentBone]);
			glLoadTransfWeights(layer.model, layer.model.normalizedWeightsSkeleton);
			glLoadTransf(layer.model, layer.model.skeleton._bones);
			resetPoseSkeleton(layer.model);
		}});
	glDrawOne(_lLayer[_idActiveLayer].model);
	drawSkeleton(_lLayer[_idActiveLayer].model);
	setAstuce("animSdC");
});

$("#animation_Radio_Cage").change(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	Animation.setModeHBSpline(modeHandler.Cage);
	_lLayer.forEach(function(layer) {
		if (layer.model.cage._cage.length == 0) {
			glLoadDispWeights(layer.model, new Array(layer.model.restMesh.length/2).fill(0));
			glLoadTransfWeights(layer.model, [new Array(layer.model.restMesh.length/2).fill(0)]);
		} else {
			glLoadDispWeights(layer.model, layer.model.normalizedWeightsCage[layer.model.cage._currentVertex]);
			glLoadTransfWeights(layer.model,layer.model.normalizedWeightsCage);
			glLoadTransf(layer.model, layer.model.cage._cage);
			resetPoseCage(layer.model);
	}});

	glDrawOne(_lLayer[_idActiveLayer].model);
	if (_lLayer[_idActiveLayer].model.cage._faces.length == 0) {
		drawCage(_lLayer[_idActiveLayer].model);
	} else {
		drawTrianglesCage(_lLayer[_idActiveLayer].model);
	}
	setAstuce("animSdC");
});

// Animation values
//$("#animation_Duration")
//$("#animation_BS_Order")
$("#animationValidate").click(function(){
	var duration = Number($("#animation_Duration").val());
	var order = Number($("#animation_BS_Order").val());
	Animation.setValues(duration, order);
	setAstuce("optionsBspline");
});

// BSpline control points edit
$("#animation_BS_Add").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (Animation.modeHBSpline == modeHandler.CP) {
		drawPoint(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].BS);
	}
	if (Animation.modeHBSpline == modeHandler.Skeleton) {
		drawSkeleton(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._currentJoint].BS);
	}
	if (Animation.modeHBSpline == modeHandler.Cage) {
		drawCage(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.cage._cage[_lLayer[_idActiveLayer].model.cage._currentVertex].BS);
	}
	changeStateEdition(modeEdition.Add);
});

$("#animation_BS_Done").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	changeStateEdition(modeEdition.Deform);
	setAstuce("validateBspline");
});

$("#animation_BS_Move").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (Animation.modeHBSpline == modeHandler.CP) {
		drawPoint(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].BS);
	}
	if (Animation.modeHBSpline == modeHandler.Skeleton) {
		drawSkeleton(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._currentJoint].BS);
	}
	if (Animation.modeHBSpline == modeHandler.Cage) {
		drawCage(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.cage._cage[_lLayer[_idActiveLayer].model.cage._currentVertex].BS);
	}
	changeStateEdition(modeEdition.Move);
});

$("#animation_BS_Delete").click(function(){
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (Animation.modeHBSpline == modeHandler.CP) {
		drawPoint(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].BS);
	}
	if (Animation.modeHBSpline == modeHandler.Skeleton) {
		drawSkeleton(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._currentJoint].BS);
	}
	if (Animation.modeHBSpline == modeHandler.Cage) {
		drawCage(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.cage._cage[_lLayer[_idActiveLayer].model.cage._currentVertex].BS);
	}
	changeStateEdition(modeEdition.Delete);
});

// Animation properties
$("#animation_BS_Origin").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	switch (Animation.modeHBSpline) {
	case modeHandler.CP:
		translateBSpline(_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].BS,
						_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].restPos);
		drawPoint(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].BS);
		break;
	case modeHandler.Skeleton:
		translateBSpline(_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._currentJoint].BS,
						_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._currentJoint].restPos);
		drawSkeleton(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._currentJoint].BS);
		break;
	case modeHandler.Cage:
		translateBSpline(_lLayer[_idActiveLayer].model.cage._cage[_lLayer[_idActiveLayer].model.cage._currentVertex].BS,
						_lLayer[_idActiveLayer].model.cage._cage[_lLayer[_idActiveLayer].model.cage._currentVertex].restPos);
		drawCage(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.cage._cage[_lLayer[_idActiveLayer].model.cage._currentVertex].BS);
		break;
	default:
	}
});

$("#animation_BS_Loop").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	switch (Animation.modeHBSpline) {
	case modeHandler.CP:
		loopBSpline(_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].BS);
		drawPoint(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].BS);
		break;
	case modeHandler.Skeleton:
		loopBSpline(_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._currentJoint].BS);
		drawSkeleton(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._currentJoint].BS);
		break;
	case modeHandler.Cage:
		loopBSpline(_lLayer[_idActiveLayer].model.cage._cage[_lLayer[_idActiveLayer].model.cage._currentVertex].BS);
		drawCage(_lLayer[_idActiveLayer].model);
		afficherBSpline(_lLayer[_idActiveLayer].model.cage._cage[_lLayer[_idActiveLayer].model.cage._currentVertex].BS);
		break;
	default:
	}
});

var time;

$("#animation_PlayPause").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (Animation.isPlaying) {
		Animation.isPlaying = false;
	} else {
		Animation.isPlaying = true;
		time = Number($("#animation_Time").val());
		setTimeout(animPlay, 25);
	}
});

function animPlay() {
	if (Animation.isPlaying) {
		_lLayer.forEach(function(layer) {
			Animation.applyAnimation(layer.model, time);
		});
		glDrawAll(_lLayer);
		if((time + 0.025 / Animation.duration) >=  1) {
			time = 0;
			$("#animation_Time").prop('value', time);
		}
		else {
			time = time + 0.025 / Animation.duration;
			$("#animation_Time").prop('value', time);
		}
		setTimeout(animPlay, 25);
	}
}

$("#animation_Stop").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	Animation.isPlaying = false;
	$("#animation_Time").prop('value', 0);
	switch (Animation.modeHBSpline) {
	case modeHandler.CP:
		resetPoseCP(_lLayer[_idActiveLayer].model);
		break;
	case modeHandler.Skeleton:
		resetPoseSkeleton(_lLayer[_idActiveLayer].model);
		break;
	case modeHandler.Cage:
		resetPoseCage(_lLayer[_idActiveLayer].model);
		break;
	}
	setAstuce("More");
});

// Callback on time slider changes
$("#animation_Time").on('input', function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	Animation.isPlaying = false;
	Animation.applyAnimation(_lLayer[_idActiveLayer].model, $("#animation_Time").val());
	glDrawOne(_lLayer[_idActiveLayer].model);
	if($("#animation_Time").val() == 1) {
		$("#animation_Time").prop('value', 0);
	}
});

// Clear BSpline
// /!\ Can delete all BSplines if currentHandle == -1
$("#animation_BS_Clear").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (!Animation.isPlaying) {
		switch (Animation.modeHBSpline) {
		case modeHandler.CP:
			deleteBSpline(_lLayer[_idActiveLayer].model, Animation.modeHBSpline, _lLayer[_idActiveLayer].model.cp._currentCP);
			drawPoint(_lLayer[_idActiveLayer].model);
			afficherBSpline(_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].BS);
			break;
		case modeHandler.Skeleton:
			deleteBSpline(_lLayer[_idActiveLayer].model, Animation.modeHBSpline, _lLayer[_idActiveLayer].model.skeleton._currentJoint);
			drawSkeleton(_lLayer[_idActiveLayer].model);
			afficherBSpline(_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._currentJoint].BS);
			break;
		case modeHandler.Cage:
			deleteBSpline(_lLayer[_idActiveLayer].model, Animation.modeHBSpline, _lLayer[_idActiveLayer].model.cage._currentVertex);
			drawCage(_lLayer[_idActiveLayer].model);
			afficherBSpline(_lLayer[_idActiveLayer].model.cage._cage[_lLayer[_idActiveLayer].model.skeleton._currentVertex].BS);
			break;
		}
	}
});

$("#animation_BS_ClearAll").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	if (!Animation.isPlaying) {
		deleteBSpline(_lLayer[_idActiveLayer].model, Animation.modeHBSpline, -1);
		// Update display
		switch (Animation.modeHBSpline) {
		case modeHandler.CP:
			drawPoint(_lLayer[_idActiveLayer].model);
			afficherBSpline(_lLayer[_idActiveLayer].model.cp._cp[_lLayer[_idActiveLayer].model.cp._currentCP].BS);
			break;
		case modeHandler.Skeleton:
			drawSkeleton(_lLayer[_idActiveLayer].model);
			afficherBSpline(_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._currentJoint].BS);
			break;
		case modeHandler.Cage:
			drawCage(_lLayer[_idActiveLayer].model);
			afficherBSpline(_lLayer[_idActiveLayer].model.cage._cage[_lLayer[_idActiveLayer].model.skeleton._currentVertex].BS);
			break;
		}
	}
});

/* Layers */
$("#addCalque").click(function() {
		addLayer();
});

$("#clearCalques").click(function() {
		clearLayers();
});

$("#checkboxPoids").click(function() {
	if (_lLayer[_idActiveLayer].model === null) { return; } // Check if there is a model
	switch(_modeH) {
	case modeHandler.CP:
		if (_lLayer[_idActiveLayer].model.cp._cp.length == 0) {
			glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
		} else {
			glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP]);
		}
		break;
	case modeHandler.Skeleton:
		if (_lLayer[_idActiveLayer].model.skeleton._bones.length == 0) {
			glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
		} else {
			if (_lLayer[_idActiveLayer].model.skeleton._currentJoint == 0)
				glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
			else
				glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton[_lLayer[_idActiveLayer].model.skeleton._currentBone]);
		}
		break;
	case modeHandler.Cage:
		if (_lLayer[_idActiveLayer].model.cage._cage.length == 0) {
			glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
		} else {
			glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCage[_lLayer[_idActiveLayer].model.cage._currentVertex]);
		}
		break;
	case modeHandler.AnimByBSpline:
		switch(Animation.modeHBSpline) {
		case modeHandler.CP:
			if (_lLayer[_idActiveLayer].model.cp._cp.length == 0) {
				glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
			} else {
				glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP]);
			}
			break;
		case modeHandler.Skeleton:
			if (_lLayer[_idActiveLayer].model.skeleton._bones.length == 0) {
				glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
			} else {
				if (_lLayer[_idActiveLayer].model.skeleton._currentJoint == 0)
					glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
				else
					glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton[_lLayer[_idActiveLayer].model.skeleton._currentBone]);
			}
			break;
		case modeHandler.Cage:
			if (_lLayer[_idActiveLayer].model.cage._cage.length == 0) {
				glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
			} else {
				glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCage[_lLayer[_idActiveLayer].model.cage._currentVertex]);
			}
			break;
		}
		break;
	default:
		break;
	}
	glSetDrawWeights($("#checkboxPoids").prop('checked'));
});
