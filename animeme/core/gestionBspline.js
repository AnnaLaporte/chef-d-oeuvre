function addBSPoint(x, y) {
  // Check mode in which we are
  switch(Animation.modeHBSpline) {
  case modeHandler.CP:
    // Check selected CP Range:
    var currentCP = _lLayer[_idActiveLayer].model.cp._currentCP;
    if (currentCP >= 0 && currentCP < _lLayer[_idActiveLayer].model.cp._cp.length) {
      // Push a bs point for this given CP
      _lLayer[_idActiveLayer].model.cp._cp[currentCP].BS.points.push(glMatrix.vec2.fromValues(x, y));

      // Update BSpline order and nodalVector
      buildNodalVectorCP(currentCP);
      if (_lLayer[_idActiveLayer].model.cp._cp[currentCP].BS.points.length > 2)
        setAstuce("finEditerBspline");
    }
    break;
  case modeHandler.Skeleton:
    // Check selected bone Range:
    var currentJoint = _lLayer[_idActiveLayer].model.skeleton._currentJoint;
    if (currentJoint >= 0 && currentJoint < _lLayer[_idActiveLayer].model.skeleton._joints.length) {
      // Push a bs point for this given CP
      _lLayer[_idActiveLayer].model.skeleton._joints[currentJoint].BS.points.push(glMatrix.vec2.fromValues(x, y));

      // Update BSpline order and nodalVector
      buildNodalVectorSkeleton(currentJoint);
      if (_lLayer[_idActiveLayer].model.skeleton._joints[currentJoint].BS.points.length > 2)
        setAstuce("finEditerBspline");
    }
    break;
  case modeHandler.Cage:
    // Check selected vertex Range:
    var currentVertex = _lLayer[_idActiveLayer].model.cage._currentVertex;
    if (currentVertex >= 0 && currentVertex < _lLayer[_idActiveLayer].model.cage._cage.length) {
      // Push a bs point for this given CP
      _lLayer[_idActiveLayer].model.cage._cage[currentVertex].BS.points.push(glMatrix.vec2.fromValues(x, y));

      // Update BSpline order and nodalVector
      buildNodalVectorCage(currentVertex);
      if (_lLayer[_idActiveLayer].model.cage._cage[currentVertex].BS.points.length > 2)
        setAstuce("finEditerBspline");
    }
    break;
  default:
    console.log("addBSPoint: no known modeHandler selected");
  }
}

function deleteBSPoint(id) {
  // We need to know in which data structure to remove bs point from
  switch(Animation.modeHBSpline) {
  case modeHandler.CP:
    // Check currentCP value
    var currentCP = _lLayer[_idActiveLayer].model.cp._currentCP;
    if (currentCP >= 0 && currentCP < _lLayer[_idActiveLayer].model.cp._cp.length) {
      _lLayer[_idActiveLayer].model.cp._cp[currentCP].BS.points.splice(i,1);

      // Update BSpline order and nodalVector
      buildNodalVectorCP(currentCP);
    }
    break;
  case modeHandler.Skeleton:
    // Check currentBone value
    var currentJoint = _lLayer[_idActiveLayer].model.skeleton._currentJoint;
    if (currentJoint >= 0 && currentJoint < _lLayer[_idActiveLayer].model.skeleton._joints.length) {
      _lLayer[_idActiveLayer].model.skeleton._joints[currentJoint].BS.points.splice(i,1);

      // Update BSpline order and nodalVector
      buildNodalVectorSkeleton(currentJoint);
    }
    break;
  case modeHandler.Cage:
    // Check currentVertex value
    var currentVertex = _lLayer[_idActiveLayer].model.cage._currentVertex;
    if (currentVertex >= 0 && currentVertex < _lLayer[_idActiveLayer].model.cage._cage.length) {
      _lLayer[_idActiveLayer].model.cage._cage[currentVertex].BS.points.splice(i,1);

      // Update BSpline order and nodalVector
      buildNodalVectorCage(currentVertex);
    }
    break;
  default:
    console.log("deleteBSPoint: no known modeHandler selected");
  }
}


function moveBSPoint(x, y, id) {
  // We need to know in which data structure to remove bs point from
  switch(Animation.modeHBSpline) {
  case modeHandler.CP:
    // Check currentCP value
    var currentCP = _lLayer[_idActiveLayer].model.cp._currentCP;
    if (currentCP >= 0 && currentCP < _lLayer[_idActiveLayer].model.cp._cp.length) {
      glMatrix.vec2.set(_lLayer[_idActiveLayer].model.cp._cp[currentCP].BS.points[id], x, y);
      // No need to update BSpline order or nodalVector
    }
    break;
  case modeHandler.Skeleton:
    // Check currentCP value
    var currentJoint = _lLayer[_idActiveLayer].model.skeleton._currentJoint;
    if (currentJoint >= 0 && currentJoint < _lLayer[_idActiveLayer].model.skeleton._joints.length) {
      glMatrix.vec2.set(_lLayer[_idActiveLayer].model.skeleton._joints[currentJoint].BS.points[id], x, y);
      // No need to update BSpline order or nodalVector
    }
    break;
  case modeHandler.Cage:
    // Check currentCP value
    var currentVertex = _lLayer[_idActiveLayer].model.cage._currentVertex;
    if (currentVertex >= 0 && currentVertex < _lLayer[_idActiveLayer].model.cage._cage.length) {
      glMatrix.vec2.set(_lLayer[_idActiveLayer].model.cage._cage[currentVertex].BS.points[id], x, y);
      // No need to update BSpline order or nodalVector
    }
    break;
  default:
    console.log("moveBSPoint: no known modeHandler selected");
  }
}

function buildNodalVectorCP(currentCP) {
  _lLayer[_idActiveLayer].model.cp._cp[currentCP].BS.k = Animation.order;
  _lLayer[_idActiveLayer].model.cp._cp[currentCP].BS.nodalVector = [...Array(_lLayer[_idActiveLayer].model.cp._cp[currentCP].BS.points.length + Animation.order)].map((_, i) => i - (Animation.order - 1));
}

function buildNodalVectorSkeleton(currentBJoint) {
  _lLayer[_idActiveLayer].model.skeleton._joints[currentBJoint].BS.k = Animation.order;
  _lLayer[_idActiveLayer].model.skeleton._joints[currentBJoint].BS.nodalVector = [...Array(_lLayer[_idActiveLayer].model.skeleton._joints[currentBJoint].BS.points.length + Animation.order)].map((_, i) => i - (Animation.order - 1));
}

function buildNodalVectorCage(currentVertex) {
  _lLayer[_idActiveLayer].model.cage._cage[currentVertex].BS.k = Animation.order;
  _lLayer[_idActiveLayer].model.cage._cage[currentVertex].BS.nodalVector = [...Array(_lLayer[_idActiveLayer].model.cage._cage[currentVertex].BS.points.length + Animation.order)].map((_, i) => i - (Animation.order - 1));
}
