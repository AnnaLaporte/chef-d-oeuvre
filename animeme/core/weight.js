/**
 * Chef d'oeuvre - Animeme
 * M2 IGAI - 2018/2019 - Université Toulouse 3 Paul Sabatier
 */

// TEST variables
// var modelTest = new Model();
// modelTest.restMesh = [
//     0,   0,
//   800,   0,
//     0, 800,
//   800, 800,
//   400, 400,
// ];
//
// modelTest.cp._cp[0] = new SingleCP();
// modelTest.cp._cp[0].restPos = glMatrix.vec2.fromValues(400, 400);
// modelTest.cp._cp[1] = new SingleCP();
// modelTest.cp._cp[1].restPos = glMatrix.vec2.fromValues(200, 200);
//
// modelTest.skeleton._joints[0] = new Joint();
// modelTest.skeleton._joints[0].restPos = glMatrix.vec2.fromValues(400, 400);
// modelTest.skeleton._joints[1] = new Joint();
// modelTest.skeleton._joints[1].restPos = glMatrix.vec2.fromValues(100, 500);
// modelTest.skeleton._joints[2] = new Joint();
// modelTest.skeleton._joints[2].restPos = glMatrix.vec2.fromValues(700, 100);
//
// modelTest.skeleton._bones.push(new Bone);
// modelTest.skeleton._bones[0].idFather = 0;
// modelTest.skeleton._bones[0].idChild = 1;
//
// modelTest.skeleton._bones.push(new Bone);
// modelTest.skeleton._bones[1].idFather = 0;
// modelTest.skeleton._bones[1].idChild = 2;


/**
 * Weigth static class to compute weight for the model according the mode
 */

class Weight {
  /*
  constructor(){
  }
  */

  /**
   * normalizeWeights
   * @param model instance of Model class which contains the current model to normalize weights on
   * @param mode enum ModeH to know in which data structure of the model to normalize weights on
   * Normalize weights if the sum of all data structure handles weights for a given vertex is greather than 1
   */
   static normalizeWeights(model, mode){
     switch(mode) {
       case modeHandler.CP:
         // New weight matrix trick (creates a matrix of cp.length sub arrays of length cp and value 0));

         // Iterating over mesh vertices
         for (var vertexId = 0; vertexId < model.restMesh.length/2; ++vertexId) {
           // First we build a sum of weights from all control points for the given vertex
           var sum = 0;
           for (var cpId = 0; cpId < model.cp._cp.length; ++cpId){
             //model.normalizedWeightsCP[cpId][vertexId] = model.unnormalizedWeightsCP[cpId][vertexId];
             sum += model.unnormalizedWeightsCP[cpId][vertexId];
           }

           // Sum is nul (no weight for this vertex, it won't move so it stays at zero and matrix already set to zero)
           // Sum is less than 1, no need to normalize (already set the weight in previous loop)
           // Else normalize anyway
           if (sum > 1) {
             for (var cpId = 0; cpId < model.cp._cp.length; ++cpId){
               model.normalizedWeightsCP[cpId][vertexId] = model.unnormalizedWeightsCP[cpId][vertexId] / sum;
             }
           }
         }
		 	 	 glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCP);
         break;

       case modeHandler.Skeleton:
         // New weight matrix trick (creates a matrix of cp.length sub arrays of length cp and value 0));
         var normalizedWeightsCP = [...Array(model.skeleton._bones.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));

         // Iterating over mesh vertices
         for (var vertexId = 0; vertexId < model.restMesh.length/2; ++vertexId) {
           // First we build a sum of weights from all control points for the given vertex
           var sum = 0;
           for (var boneId = 0; boneId < model.skeleton._bones.length; ++boneId){
             model.normalizedWeightsSkeleton[boneId][vertexId] = model.unnormalizedWeightsSkeleton[boneId][vertexId];
             sum += model.unnormalizedWeightsSkeleton[boneId][vertexId];
           }

           // Sum is nul (no weight for this vertex, it won't move so it stays at zero and matrix already set to zero)
           // Sum is less than 1, no need to normalize (already set the weight in previous loop)
           // Else normalize anyway
           if (sum > 1) {
             for (var boneId = 0; boneId < model.skeleton._bones.length; ++boneId){
               model.normalizedWeightsSkeleton[boneId][vertexId] = model.unnormalizedWeightsSkeleton[boneId][vertexId] / sum;
             }
           }
         }
		 	 	 glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsSkeleton);
         break;
       case modeHandler.Cage:
         // New weight matrix trick (creates a matrix of cp.length sub arrays of length cp and value 0));
         var normalizedWeightsCage = [...Array(model.cage._cage.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));

         // Iterating over mesh vertices
         for (var vertexId = 0; vertexId < model.restMesh.length/2; ++vertexId) {
           // First we build a sum of weights from all control points for the given vertex
           var sum = 0;
           for (var cvId = 0; cvId < model.cage._cage.length; ++cvId){
             model.normalizedWeightsCage[cvId][vertexId] = model.unnormalizedWeightsCage[cvId][vertexId];
             sum += model.unnormalizedWeightsCage[cvId][vertexId]
           }

           // Sum is nul (no weight for this vertex, it won't move so it stays at zero and matrix already set to zero)
           // Sum is less than 1, no need to normalize (already set the weight in previous loop)
           // Else normalize anyway
           if (sum > 1) {
             for (var cvId = 0; cvId < model.cage._cage.length; ++cvId){
               model.normalizedWeightsCage[cvId][vertexId] = model.unnormalizedWeightsCage[cvId][vertexId] / sum;
             }
           }
         }
		 	 	 glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCage);
         break;
       default:
         console.log("normalizeWeights: Unknown modeHandler value")
     }
   }

  /**
   * calculateInverse
   * @param model instance of Model class which contains the current model weights
   * @param mode enum ModeH to know in which data structure of the model to calculate weights if applicable
   * @param p smoothing term used in the equation of control points
   * Compute the euclidian inverse distance weights of handles
   * For the control points the distance is capped at 'minDistance' but not 'maxDistance'
   */
  static calculateInverse(model, mode, p = 2){
    // Canvas size
    var width  = $("#glCanvas").width();
    var height = $("#glCanvas").width();
    var weightNumber = 0;
		var maxs = [];


    switch(mode) {
      case modeHandler.CP:
        // Parameters
        var minDistance  =   1;
        var maxDistance  = 20000;

        // Vars created (and allocated for vectors) only once
        var weightNumber =   0;
        var sum = 0;
        var distance = 0;
        var weight = 0;
				var vertex = glMatrix.vec2.create();
        var controlPoint = glMatrix.vec2.create();

        // New weight matrix trick (creates a matrix of cp.length sub arrays of length cp and value 0)
        var unnormalizedWeightsCP = [...Array(model.cp._cp.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));
        var normalizedWeightsCP = [...Array(model.cp._cp.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));

        // Iterating over mesh vertices
        for (var vertexId = 0; vertexId < model.restMesh.length/2; ++vertexId) {
          sum = 0;
          maxs = [];

          // Iterating over control points
          for (var cpId = 0; cpId < model.cp._cp.length; ++cpId){
            // Get points
            vertex = glMatrix.vec2.fromValues(model.restMesh[2*vertexId], model.restMesh[2*vertexId+1]);
            controlPoint = model.cp._cp[cpId].restPos;
            // Compute the distance
            distance = glMatrix.vec2.distance(vertex, controlPoint);
            // Capping the distance
            distance = (distance < minDistance) ? minDistance : distance;
            distance = (distance > maxDistance) ? maxDistance : distance;
            // Compute the weight
            weight = 1 / Math.pow(distance, p);
						++weightNumber;
            if (maxs.length <= 3) {
            	maxs.push([weight,cpId]);
            }
            else {
            	maxs.push([weight,cpId]);
            	maxs.sort((a, b) => b[0] - a[0]);
            	maxs.pop();
            }
            //unnormalizedWeightsCP[cpId][vertexId] = 0;
          }

        	sum = 0;
        	for (var i = 0; i < maxs.length; i++) {
        		unnormalizedWeightsCP[maxs[i][1]][vertexId] = maxs[i][0];
        		sum += maxs[i][0];
	       	}

          // Normalize now for optimization (saving one loop run)
          for (var cpId = 0; cpId < model.cp._cp.length; ++cpId){
            normalizedWeightsCP[cpId][vertexId] = unnormalizedWeightsCP[cpId][vertexId] / sum;
          }
        }

        // Replace matrices
				model.normalizedWeightsCP = [...Array(model.cp._cp.length)].map((_, i) => normalizedWeightsCP[i].slice());
				model.unnormalizedWeightsCP = [...Array(model.cp._cp.length)].map((_, i) => normalizedWeightsCP[i].slice());

				glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCP);

        console.log("Control Points: calculated " + weightNumber + " inverse weights");
        break;

        case modeHandler.Skeleton:
          // New weight matrix trick (creates a matrix of cp.length sub arrays of length skeleton.length and value 0)
          var unnormalizedWeightsSkeleton = [...Array(model.skeleton._bones.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));
          var normalizedWeightsSkeleton = [...Array(model.skeleton._bones.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));

          // Iterating over mesh vertices
          for (var vertexId = 0; vertexId < model.restMesh.length/2; ++vertexId) {
            // Keep the nearestBone and its distance to the vertex ()
            var nearestBone = null;
            // Iterate over bones
            for (var boneId = 0; boneId < model.skeleton._bones.length; ++boneId) {
              var boneDistance = 0;
              // Get points
              var vertex = glMatrix.vec2.fromValues(model.restMesh[2*vertexId], model.restMesh[2*vertexId+1]);
              // To use if JOINTS ARE in same coordinates as vertices
              var childJoint = model.skeleton._joints[model.skeleton._bones[boneId].idChild].restPos;
              var parentJoint = model.skeleton._joints[model.skeleton._bones[boneId].idFather].restPos;

              // Compute orthogonal band to know whether it is nearer to a point or the segment
              var vector_point_parent = glMatrix.vec2.create();
              glMatrix.vec2.subtract(vector_point_parent, parentJoint, vertex);
              var vector_child_point = glMatrix.vec2.create();
              glMatrix.vec2.subtract(vector_child_point, vertex, childJoint);
              var segment_parent_child = glMatrix.vec2.create();
              glMatrix.vec2.subtract(segment_parent_child, childJoint, parentJoint);

              // Closest point is the parent, hence euclidian distance from point to parent
              if(glMatrix.vec2.dot(segment_parent_child, vector_point_parent) >= 0) {
                boneDistance = glMatrix.vec2.dot(vector_point_parent, vector_point_parent);
              }
              // Closest point is the child, hence euclidian distance from point to child
              else if(glMatrix.vec2.dot(segment_parent_child, vector_child_point) >= 0) {
                boneDistance = glMatrix.vec2.dot(vector_child_point, vector_child_point);
              }
              // Closest point is point projected onto line segment between parent and child
              else {
                var right = glMatrix.vec2.create();
                var ratio = glMatrix.vec2.dot(segment_parent_child, vector_point_parent) / glMatrix.vec2.dot(segment_parent_child, segment_parent_child)
                glMatrix.vec2.scale(right, segment_parent_child, ratio);
                var e = glMatrix.vec2.create();
                glMatrix.vec2.subtract(e, vector_point_parent, right);
                boneDistance = glMatrix.vec2.dot(e, e);
              }

              // nearestBone doesn't exist yet so we put a reference
              if (nearestBone == null) {
                nearestBone = Object;
                nearestBone.boneId = boneId;
                nearestBone.distance = boneDistance;
              }
              // Look for the minimum distance to a bone
              else if (boneDistance < nearestBone.distance) {
                nearestBone.boneId = boneId;
                nearestBone.distance = boneDistance;
              }
            }

            // For now act like rigid distance: set 0
            unnormalizedWeightsSkeleton[nearestBone.boneId][vertexId] =
            normalizedWeightsSkeleton[nearestBone.boneId][vertexId] = 1;
            ++weightNumber;
          }

          // Joint weight fadeoff
          var ratio = 0.25;
          var n = 3;

          // Iterate over bones to find bone pairs
          for (var boneId = 0; boneId < model.skeleton._bones.length; ++boneId) {
            // 1. Parent is root, no pairs
            if (model.skeleton._joints[model.skeleton._bones[boneId].idFather].idFather == -1) {
              continue;
            }

            // 2. Else we have a 2 bones (a pair) and 3 joints such as j1<--b1-->j2<--b2-->j3
            // j3: child joint of bone 2
            var idJoint3 = model.skeleton._bones[boneId].idChild;
            // b2: bone 2, the child bone
            var idBone2 = boneId;
            // j2: child joint of bone 1 and parent joint of bone 2
            var idJoint2 = model.skeleton._bones[boneId].idFather;
            // b1: bone 1, the parent bone, found with a loop
            var idBone1 = 0;
            for (var i = 0; i < model.skeleton._bones.length; ++i) {
              if (model.skeleton._bones[i].idChild == idJoint2) {
                idBone1 = i;
                break;
              }
            }
            // j1: parent joint of bone 1
            var idJoint1 = model.skeleton._bones[idBone1].idFather;

            // 3. Find the separation line /!\ there 2 cases: bones are colinear or they aren't
            var bone1 = glMatrix.vec2.create();
            glMatrix.vec2.subtract(bone1, model.skeleton._joints[idJoint1].restPos, model.skeleton._joints[idJoint2].restPos);
            var bone2 = glMatrix.vec2.create();
            glMatrix.vec2.subtract(bone2, model.skeleton._joints[idJoint3].restPos, model.skeleton._joints[idJoint2].restPos);

            var separationLine = glMatrix.vec2.create();
            // If bones are colinear, we find the normal of first bone
            if (Weight.collinear(bone1, bone2)) {
              separationLine = glMatrix.vec2.fromValues(-bone1[1], bone1[0]);
            }
            // If they are not, just add the 2 normalized bone vectors)
            else {
              var tempNormalized = glMatrix.vec2.create();
              glMatrix.vec2.normalize(tempNormalized, bone1);
              glMatrix.vec2.add(separationLine, separationLine, tempNormalized);
              glMatrix.vec2.normalize(tempNormalized, bone2);
              glMatrix.vec2.add(separationLine, separationLine, tempNormalized);
            }
            glMatrix.vec2.normalize(separationLine, separationLine);

            // 4. /!\ Sign of separation line distance
            // TODO: Le produit scalaire ne renseigne pas sur le sens de la bissectrice

            // 5. Find the lowest offset of bones
            var R = ratio * Math.min(glMatrix.vec2.length(bone1), glMatrix.vec2.length(bone2)) * 2;

            // 6. Prepare the point-line distance from the vertex to the separation line
            var point1 = model.skeleton._joints[idJoint2].restPos;
            var point2 = glMatrix.vec2.create();
            glMatrix.vec2.add(point2, point1, separationLine);
            var lineEquation = Weight.calculateLine(point1, point2);

            // Try with shifting the separation line
            var separationLineNormal = glMatrix.vec2.fromValues(-separationLine[1], separationLine[0]);
            var point1Prime = glMatrix.vec2.create();
            var point2Prime = glMatrix.vec2.create();
            glMatrix.vec2.scale(point1Prime, separationLineNormal, R)
            glMatrix.vec2.add(point1Prime, point1, point1Prime);
            glMatrix.vec2.add(point2Prime, point1Prime, separationLine);

            var lineEquationShifted = Weight.calculateLine(point1Prime, point2Prime);

            // 7. Iterate over vertices to compute the weight fadeoff
            for (var vertexId = 0; vertexId < model.restMesh.length/2; ++vertexId) {
              // I. Check if weights are relevant to this vertex
              if (normalizedWeightsSkeleton[idBone1][vertexId] == 0 && normalizedWeightsSkeleton[idBone2][vertexId] == 0) {
                continue;
              }

              // II. If weights are relevant, get the vertex position
              var vertex = glMatrix.vec2.fromValues(model.restMesh[2*vertexId], model.restMesh[2*vertexId+1]);

              // III. Compute the distance
              var d = Weight.distance2DPointToLine(vertex, lineEquation);
              var dShifted = Weight.distance2DPointToLine(vertex, lineEquationShifted) / 2;

              // Irrelevant if distance is greater than R
              if (Math.abs(d) >= R) {
                continue;
              }

              // IV. Compact support gaussian equation
              var weight = (1 / Math.pow(R, 2*n)) * Math.pow( Math.pow(R, 2) - Math.pow(dShifted, 2), n);

              // Finding the direction of the angle
              var direction = Math.atan2(separationLine[1], separationLine[0]) - Math.atan2(bone1[1], bone1[0]);
              if (direction > Math.PI)
                direction -= Math.PI * 2;
              if (direction < -Math.PI)
                direction += Math.PI * 2;

              // Given the direction, fadeoff in one direction or its opposite
              if(direction > 0) {
              	weight = 1 - weight;
              }

              if (unnormalizedWeightsSkeleton[idBone1][vertexId] > 0) {
	              unnormalizedWeightsSkeleton[idBone1][vertexId] = Math.min(unnormalizedWeightsSkeleton[idBone1][vertexId], weight);
	            } else {
	            	unnormalizedWeightsSkeleton[idBone1][vertexId] = weight;
	            }
              if (unnormalizedWeightsSkeleton[idBone2][vertexId] > 0) {
	              unnormalizedWeightsSkeleton[idBone2][vertexId] = Math.min(unnormalizedWeightsSkeleton[idBone2][vertexId], 1 - weight);
	            } else {
	            	unnormalizedWeightsSkeleton[idBone2][vertexId] = 1 - weight;
	            }
            }
          }


          // lissage entre les paires de frères
          for (var boneId = 0; boneId < model.skeleton._bones.length; ++boneId) {
            var siblingNumber = model.skeleton._joints[model.skeleton._bones[boneId].idFather].child.length;
            // 1. Bone has no brother
            if ( siblingNumber <= 1 ) {
              continue;
            }
            //récupérer les frères (pas moi)
            for (var filsId = 0; filsId < siblingNumber; filsId++) {
              // 2. Skip itself (same child joint)
            	if (model.skeleton._bones[boneId].idChild == model.skeleton._joints[model.skeleton._bones[boneId].idFather].child[filsId]) {
            		continue;
              }
              // 2. Else we have a 2 bones (a pair) and 3 joints such as j1 <--b1 (brother)--> j2 <--b2 (self)--> j3
	            // j3: child joint of self
	            var idJoint3 = model.skeleton._bones[boneId].idChild;
	            // j2: parent joint of self AND brother
	            var idJoint2 = model.skeleton._bones[boneId].idFather;
              // j1: child joint of brother
              var idJoint1 = model.skeleton._joints[model.skeleton._bones[boneId].idFather].child[filsId];
              // b2: bone 2, self
              var idBone2 = boneId;
	            // b1: bone 1, brother (it has same j2 as parent and j1 as child)
              var idBone1 = 0;
              for (var i = 0; i < model.skeleton._bones.length; ++i) {
                if (model.skeleton._bones[i].idFather == idJoint2 && model.skeleton._bones[i].idChild == idJoint1) {
                  idBone1 = i;
                  break;
                }
              }

	            // 3. Find the separation line /!\ there 2 cases: bones are colinear or they aren't
	            var bone1 = glMatrix.vec2.create();
	            glMatrix.vec2.subtract(bone1, model.skeleton._joints[idJoint1].restPos, model.skeleton._joints[idJoint2].restPos);
	            var bone2 = glMatrix.vec2.create();
	            glMatrix.vec2.subtract(bone2, model.skeleton._joints[idJoint3].restPos, model.skeleton._joints[idJoint2].restPos);

	            var separationLine = glMatrix.vec2.create();
	            // If bones are colinear, we find the normal of first bone
	            if (Weight.collinear(bone1, bone2)) {
	              separationLine = glMatrix.vec2.fromValues(-bone1[1], bone1[0]);
	            }
	            // If they are not, just add the 2 normalized bone vectors)
	            else {
	              var tempNormalized = glMatrix.vec2.create();
	              glMatrix.vec2.normalize(tempNormalized, bone1);
	              glMatrix.vec2.add(separationLine, separationLine, tempNormalized);
	              glMatrix.vec2.normalize(tempNormalized, bone2);
	              glMatrix.vec2.add(separationLine, separationLine, tempNormalized);
	            }
							glMatrix.vec2.normalize(separationLine, separationLine);

	            // 4. /!\ Sign of separation line distance
	            // TODO: Le produit scalaire ne renseigne pas sur le sens de la bissectrice

	            // 5. Find the lowest offset of bones
	            var R = ratio * Math.min(glMatrix.vec2.length(bone1), glMatrix.vec2.length(bone2)) * 2;

	            // 6. Prepare the point-line distance from the vertex to the separation line
	            var point1 = model.skeleton._joints[idJoint2].restPos;
	            var point2 = glMatrix.vec2.create();
	            glMatrix.vec2.add(point2, point1, separationLine);
	            var lineEquation = Weight.calculateLine(point1, point2);

	            // Try with shifting the separation line
	            var separationLineNormal = glMatrix.vec2.fromValues(-separationLine[1], separationLine[0]);
	            var point1Prime = glMatrix.vec2.create();
	            var point2Prime = glMatrix.vec2.create();
	            glMatrix.vec2.scale(point1Prime, separationLineNormal, R)
	            glMatrix.vec2.add(point1Prime, point1, point1Prime);
	            glMatrix.vec2.add(point2Prime, point1Prime, separationLine);

	            var lineEquationShifted = Weight.calculateLine(point1Prime, point2Prime);

	            // 7. Iterate over vertices to compute the weight fadeoff
	            for (var vertexId = 0; vertexId < model.restMesh.length/2; ++vertexId) {
	              // I. Check if weights are relevant to this vertex
	              if (normalizedWeightsSkeleton[idBone1][vertexId] == 0 && normalizedWeightsSkeleton[idBone2][vertexId] == 0) {
	                continue;
	              }

	              // II. If weights are relevant, get the vertex position
	              var vertex = glMatrix.vec2.fromValues(model.restMesh[2*vertexId], model.restMesh[2*vertexId+1]);

	              // III. Compute the distance
	              var d = Weight.distance2DPointToLine(vertex, lineEquation);
	              var dShifted = Weight.distance2DPointToLine(vertex, lineEquationShifted) / 2;

	              // Irrelevant if distance is greater than R
	              if (Math.abs(d) >= R) {
	                continue;
	              }

	              // IV. Compact support gaussian equation
	              var weight = (1 / Math.pow(R, 2*n)) * Math.pow( Math.pow(R, 2) - Math.pow(dShifted, 2), n);

	              // Finding the direction of the angle
	              var direction = Math.atan2(separationLine[1], separationLine[0]) - Math.atan2(bone1[1], bone1[0]);
	              if (direction > Math.PI)
	                direction -= Math.PI * 2;
	              if (direction < -Math.PI)
	                direction += Math.PI * 2;

	              // Given the direction, fadeoff in one direction or its opposite
	              if(direction > 0) {
	              	weight = 1 - weight;
	              }

	              if (unnormalizedWeightsSkeleton[idBone1][vertexId] > 0.0001) {
		              unnormalizedWeightsSkeleton[idBone1][vertexId] = Math.min(unnormalizedWeightsSkeleton[idBone1][vertexId],weight);
		            } else {
		            	unnormalizedWeightsSkeleton[idBone1][vertexId] = weight;
		            }
	              if (unnormalizedWeightsSkeleton[idBone2][vertexId] > 0.0001) {
		              unnormalizedWeightsSkeleton[idBone2][vertexId] = Math.min(unnormalizedWeightsSkeleton[idBone2][vertexId],1-weight);
		            } else {
		            	unnormalizedWeightsSkeleton[idBone2][vertexId] = 1-weight;
		            }
	            }
	          }
        	}
	        for (var vertexId = 0; vertexId < model.restMesh.length/2; ++vertexId) {
	        	var maxs = [];
	        	for (var boneId = 0; boneId < model.skeleton._bones; boneId++) {
							if (maxs.length <= 3) {
            		maxs.push([unnormalizedWeightsSkeleton[boneId][vertexId],boneId]);
            	}
            	else {
            		maxs.push([unnormalizedWeightsSkeleton[boneId][vertexId],boneId]);
            		maxs.sort((a, b) => b[0] - a[0]);
            		maxs.pop();
            	}
            	unnormalizedWeightsSkeleton[boneId][vertexId] = 0;
          	}
						sum = 0;
        		for (var i = 0; i < maxs.length; i++) {
        			unnormalizedWeightsSkeleton[maxs[i][1]][vertexId] = maxs[i][0];
        			sum += maxs[i][0];
	       		}

          // Normalize now for optimization (saving one loop run)
          	for (var boneId = 0; boneId < model.skeleton._bones.length; ++boneId){
          	  normalizedWeightsSkeleton[boneId][vertexId] = unnormalizedWeightsSkeleton[boneId][vertexId] / sum;
          	}
          // Replace matrices
						/*model.unnormalizedWeightsSkeleton = [...Array(model.skeleton._bones.length)].map((_, i) => unnormalizedWeightsSkeleton[i].slice());
						model.normalizedWeightsSkeleton = [...Array(model.skeleton._bones.length)].map((_, i) => normalizedWeightsSkeleton[i].slice());*/
          	model.unnormalizedWeightsSkeleton = unnormalizedWeightsSkeleton.slice();
          	model.normalizedWeightsSkeleton = normalizedWeightsSkeleton.slice();

					}
		  glLoadTransfWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsSkeleton);
          console.log("Skeleton : calculated " + weightNumber + " inverse weights");
          break;

      case modeHandler.Cage:
        console.log("UNIMPLEMENTED: class Weight.calculateBiharmonic() for CAGES");
        return "UNIMPLEMENTED";
        break;

      default:
        console.log("calculateInverse: Unknown modeHandler value")
    }
  }


  /**
   * collinear
   * @param v1 glMatrix.vec2 which represents the first vector
   * @param v2 glMatrix.vec2 which represents the second vector
   * Tells if 2 vectors are collinear
   */
  static collinear(v1, v2) {
    return (v1[0] * v2[1] == v1[1] * v2[0])
  }

  /**
   * calculateLine
   * @param p1 glMatrix.vec2 which represents the first point on the line
   * @param p2 glMatrix.vec2 which represents the second point on the line
   * Compute the line coefficients a, b and c and return them in an array such as ax + by + c = 0
   */
  static calculateLine(p1, p2) {
    var a = p1[1] - p2[1];
    var b = p2[0] - p1[0];
    var c = p1[0] * p2[1] - p2[0] * p1[1];
    return [a, b, c]
  }

  /**
   * distance2DPointToLine
   * @param point glMatrix.vec2 which represents the point to compute the distance from
   * @param line array of line equation coefficients a, b and c such as (ax+by+c) to compute the distance to
   */
  static distance2DPointToLine(point, line)
  {
    return (line[0] * point[0] + line[1] * point[1] + line[2]) / Math.sqrt(line[0] * line[0] + line[1] * line[1]);
  }

  /**
   * calculateCompact
   * @param model instance of Model class which contains the current model weights
   * @param mode enum ModeH to know in which data structure of the model to calculate weights if applicable
   * @param R the distance to which the fadeoff should take place
   * @param n the power to which the compact support gaussian is raised
   * Compute the compact support truncated gaussian weights of cp only
   */
  static calculateCompact(model, mode, R = 100, n = 3){
    // Canvas size
    var width  = $("#glCanvas").width();
    var height = $("#glCanvas").width();
    var weightNumber = 0;

    switch(mode) {
      case modeHandler.CP:
        // Vars created (and allocated for vectors) only once
        var sum = 0;
        var vertex = glMatrix.vec2.create();
        var controlPoint = glMatrix.vec2.create();
        var d = 0;
        var weight = 0;
        var maxs = [];

        // New weight matrix trick (creates a matrix of cp.length sub arrays of length cp and value 0)
        var unnormalizedWeightsCP = [...Array(model.cp._cp.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));
        var normalizedWeightsCP = [...Array(model.cp._cp.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));
        // Iterating over mesh vertices
        for (var vertexId = 0; vertexId < model.restMesh.length/2; ++vertexId) {
          // Clearing sum and array for this vertex
          sum = 0;
          maxs = [];

          // Iterating over control points
          for (var cpId = 0; cpId < model.cp._cp.length; ++cpId){
            // Get points
            vertex = glMatrix.vec2.fromValues(model.restMesh[2*vertexId], model.restMesh[2*vertexId+1]);

            // To use if CP ARE in same coordinates as vertices
            controlPoint = model.cp._cp[cpId].restPos;

            // Compute the distance
            d = glMatrix.vec2.distance(vertex, controlPoint);
            // No need to cap d because compact support gaussian = 1 when d = 0

            // Compute the weight with compact support "truncated" gaussian
            weight = (1 / Math.pow(R, 2*n)) * Math.pow( Math.pow(R, 2) - Math.pow(d, 2), n);
            weight = (d >= R) ? 0 : weight;

            // Keeping only the 4 highest weights
          	maxs.push([weight,cpId]);
            if (maxs.length > 4) {
            	maxs.sort((a, b) => b[0] - a[0]);
            	maxs.pop();
            }
	        }

          // Iteration over max weights to get the sum, assigning the 4 highest weights
          for (var i = 0; i < maxs.length; ++i) {
            normalizedWeightsCP[maxs[i][1]][vertexId] = unnormalizedWeightsCP[maxs[i][1]][vertexId] = maxs[i][0];
            sum += maxs[i][0];
            ++weightNumber;
          }

          // Last pass, assigning normalized weights
          for (var i = 0; i < maxs.length; ++i) {
            // Sum is less than one, already set the weight
            // Sum is over 1, normalize now for optimization (saving one loop run)
            if (sum > 1) {
              normalizedWeightsCP[maxs[i][1]][vertexId] = unnormalizedWeightsCP[maxs[i][1]][vertexId] / sum;
            }
          }
				}
        // Replace matrices
     		model.unnormalizedWeightsCP = unnormalizedWeightsCP;
      	model.normalizedWeightsCP = normalizedWeightsCP;

				glLoadTransfWeights(model, model.normalizedWeightsCP);

        console.log("Control Points: calculated " + weightNumber + " compact support gaussian weights");
        break;

      case modeHandler.Skeleton:
        console.log("UNIMPLEMENTED: class Weight.calculateBiharmonic() for SKELETON");
        return "UNIMPLEMENTED";
        break;

      case modeHandler.Cage:
        console.log("UNIMPLEMENTED: class Weight.calculateBiharmonic() for CAGES");
        return "UNIMPLEMENTED";
        break;

      default:
        console.log("calculateInverse: Unknown modeHandler value")
    }
  }


  /**
   * calculateBiharmonic
   * @param model instance of Model class which contains the current model weights
   * @param mode enum ModeH to know in which data structure of the model to calculate weights if applicable
   * Compute the biharmonic weights of handles of cp and skeleton
   */
  static calculateBiharmonic(model, mode){
    console.log("UNIMPLEMENTED: class Weight.calculateBiharmonic()")
    return "UNIMPLEMENTED"
  }


	static det(a,b) {
		return (a[0] * b[1] - a[1] * b[0]);
	}

	static sign(p1, p2, p3) {
		return ((p1[0] - p3[0])*(p2[1]-p3[1]) - (p2[0] - p3[0])*(p1[1]-p3[1]));
	}
  /**
   * calculateBarycentric
   * @param model instance of Model class which contains the current model weights
   * @param mode enum ModeH to know in which data structure of the model to calculate weights if applicable
   * Compute the barycentric weights of cage vertices
   */
  static calculateBarycentric(model,mode){
    if (mode == modeHandler.Cage) {
   		var unnormalizedWeightsCage = [...Array(model.cage._cage.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));
    	var normalizedWeightsCage = [...Array(model.cage._cage.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));
    	for (var vertexId = 0; vertexId < model.restMesh.length/2; ++vertexId) {
    		var P = glMatrix.vec2.fromValues(model.restMesh[2*vertexId], model.restMesh[2*vertexId+1]);

	    	for (let faces=0 ; faces<model.cage._faces.length ; faces++) {

					var A = glMatrix.vec2.fromValues(model.cage._cage[model.cage._faces[faces][0]].pos[0], model.cage._cage[model.cage._faces[faces][0]].pos[1]);
					var B = glMatrix.vec2.fromValues(model.cage._cage[model.cage._faces[faces][1]].pos[0], model.cage._cage[model.cage._faces[faces][1]].pos[1]);
					var C = glMatrix.vec2.fromValues(model.cage._cage[model.cage._faces[faces][2]].pos[0], model.cage._cage[model.cage._faces[faces][2]].pos[1]);

					// to check if the point is inside the triangle.
					var d1 = Weight.sign(P, A, B);
					var d2 = Weight.sign(P, B, C);
					var d3 = Weight.sign(P, C, A);
					var has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
					var has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

					if (!(has_neg && has_pos)) {
						var PA = glMatrix.vec2.fromValues(A[0] - P[0], A[1] - P[1]);
						var PB = glMatrix.vec2.fromValues(B[0] - P[0], B[1] - P[1]);
						var PC = glMatrix.vec2.fromValues(C[0] - P[0], C[1] - P[1]);

						var alpha = Math.abs(Weight.det(PB,PC));
						var beta = Math.abs(Weight.det(PC,PA));
						var gamma = Math.abs(Weight.det(PA,PB));

						unnormalizedWeightsCage[model.cage._faces[faces][0]][vertexId] = alpha;
						unnormalizedWeightsCage[model.cage._faces[faces][1]][vertexId] = beta;
						unnormalizedWeightsCage[model.cage._faces[faces][2]][vertexId] = gamma;
					}
				}
    	}
  	}
		model.unnormalizedWeightsCage = unnormalizedWeightsCage;
	}

	static calculateConstrainedWeights(model, mode) {
		var tabVoisins = [...Array(model.restMesh.length/2)].map((_, i) => Array());
		var handleGoals = new Map();
		var handlesList;


		switch (mode) {
			case modeHandler.CP:
				handlesList = model.cp._cp;
				break;
			case modeHandler.Skeleton:
				handlesList = model.skeleton._bones;
				break;
			default:
				console.log("erreur");
				return;
		}

		//calcule la distance entre deux vertex
		function distPts(model, idPt1, idPt2) {
			return Math.sqrt(
					  Math.pow(model.restMesh[idPt1 * 2] - model.restMesh[idPt2 * 2], 2)
					+ Math.pow(model.restMesh[idPt1 * 2 + 1] - model.restMesh[idPt2 * 2 + 1], 2) );
		}

		//teste si une liste de voisins contient un voisin d'un certain id
		function containsVois(listVois, idVois) {
			for(var i = 0; i < listVois.length; i++ ) {
				if (listVois[i].id == idVois)
					return true;
			}
			return false;
		}

		var pt1;
		var pt2;
		var pt3;
		//remplissage de tabVoisins
		for (var i = 0; i < model.indTri.length; i += 3) {
			pt1 = model.indTri[i];
			pt2 = model.indTri[i+1];
			pt3 = model.indTri[i+2];
			if (!containsVois(tabVoisins[pt1], pt2))
				tabVoisins[pt1].push({id: pt2, dist: distPts(model, pt1, pt2) });
			if (!containsVois(tabVoisins[pt1], pt3))
				tabVoisins[pt1].push({id: pt3, dist: distPts(model, pt1, pt3) });
			if (!containsVois(tabVoisins[pt2], pt1))
				tabVoisins[pt2].push({id: pt1, dist: distPts(model, pt2, pt1) });
			if (!containsVois(tabVoisins[pt2], pt3))
				tabVoisins[pt2].push({id: pt3, dist: distPts(model, pt2, pt3) });
			if (!containsVois(tabVoisins[pt3], pt2))
				tabVoisins[pt3].push({id: pt1, dist: distPts(model, pt3, pt1) });
			if (!containsVois(tabVoisins[pt3], pt2))
				tabVoisins[pt3].push({id: pt2, dist: distPts(model, pt3, pt2) });
		}

		//calcule la distance entre un point et une poignée de poits de controle
		function distPtHandlePdC(model, pt, handlesList, handle) {
			return Math.sqrt(
					  Math.pow(model.restMesh[pt] - handlesList[handle].restPos[0], 2)
					+ Math.pow(model.restMesh[pt + 1] - handlesList[handle].restPos[1], 2) );
		}
		//calcule la distance entre un point et un os
		function distPtHandleSquelette(ptx, pty, seg1, seg2) {
			var pt = glMatrix.vec2.fromValues(ptx, pty);
			var vec_pt_seg1 = glMatrix.vec2.create();
			var vec_seg2_pt = glMatrix.vec2.create();
			var vec_seg1_seg2 = glMatrix.vec2.create();

			// Compute orthogonal band to know whether it is nearer to a point or the segment
			glMatrix.vec2.subtract(vec_pt_seg1, seg1, pt);
			glMatrix.vec2.subtract(vec_seg2_pt, pt, seg2);
			glMatrix.vec2.subtract(vec_seg1_seg2, seg2, seg1);

			if(glMatrix.vec2.dot(vec_seg1_seg2, vec_pt_seg1) >= 0) {
				// Closest point is the parent, hence euclidian distance from point to parent
				boneDistance = glMatrix.vec2.dot(vec_pt_seg1, vec_pt_seg1);

			} else if(glMatrix.vec2.dot(vec_seg1_seg2, vec_seg2_pt) >= 0) {
				// Closest point is the child, hence euclidian distance from point to child
				boneDistance = glMatrix.vec2.dot(vec_seg2_pt, vec_seg2_pt);

			} else {
				// Closest point is point projected onto line segment between parent and child
				var ratio = glMatrix.vec2.dot(vec_seg1_seg2, vec_pt_seg1) / glMatrix.vec2.dot(vec_seg1_seg2, vec_seg1_seg2);

				//on réutilise vec_seg2_pt pour ne pas avoir a recreer un nouveau vecteur
				var right = vec_seg2_pt;
				glMatrix.vec2.scale(right, vec_seg1_seg2, ratio);

				//on réutilise vec_seg1_seg2 pour ne pas avoir a recreer un nouveau vecteur
				var e = vec_seg1_seg2;
				glMatrix.vec2.subtract(e, vec_pt_seg1, right);
				boneDistance = glMatrix.vec2.dot(e, e);
			}

			return Math.sqrt(boneDistance);
		}

		//remplissage de handleGoals
		for (var i = 0; i < handlesList.length; i++) {
			for (var j = 0; j < model.restMesh.length; j += 2) {
				switch (mode) {
					case modeHandler.CP:
						if(distPtHandlePdC(model, j, handlesList, i) < 1) {
							//handleGoals[i].push(j);
							handleGoals.set(j/2, i);
						}
						break;
					case modeHandler.Skeleton:
						if(distPtHandleSquelette(model.restMesh[j],
								model.restMesh[j+1],
								model.skeleton._joints[handlesList.idFather].restPos,
								model.skeleton._joints[handlesList.idChild].restPos) < 1) {
							//handleGoals[i].push(j);
							handleGoals.set(j/2, i);
						}
						break;
					default:
				}
			}
		}

		//tabVoisins et handleGoals sont remplis

		//faire le remplissage des poids ici
		var normWeights;
		var unnormWeights;

		var weightsUpdated;
		var weights;
		var oldWeights;
		var closestHandle;
		var tmpArrayPointer;
		var tmp = [];

		switch (mode) {
			case modeHandler.CP:
				normWeights = model.normalizedWeightsCP;
				unnormWeights = model.unnormalizedWeightsCP;
				normWeights = [...Array(model.cp._cp.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));
				unnormWeights = [...Array(model.cp._cp.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));
				break;
			case modeHandler.Skeleton:
				normWeights = model.normalizedWeightsSkeleton;
				unnormWeights = model.unnormalizedWeightsSkeleton;
				normWeights = [...Array(model.skeleton._bones.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));
				unnormWeights = [...Array(model.skeleton._bones.length)].map((_, i) => Array(model.restMesh.length/2).fill(0));
				break;
			default:
		}

		console.log(handleGoals);
		console.log(tabVoisins);
		//return;

		weights = new Array(model.restMesh.length/2).fill(-1);
		for (var i = 0; i < model.restMesh.length/2; i++) {
			//console.log(i);
			//si on est un goal
			if(handleGoals.get(i) != undefined) {
				closestHandle = handleGoals.get(i);
				normWeights[closestHandle][i] = 1;
				unnormWeights[closestHandle][i] = 1;
				continue;
			}
			oldWeights = new Array(model.restMesh.length/2).fill(-1);
			oldWeights[i] = 0;
			var COMPTEURDEBUG = 0;
			do {
				COMPTEURDEBUG++;
				weightsUpdated = false;
				for (var j = 0; j < model.restMesh.length/2; j++) {
					//console.log(weights);//TODO
					if (oldWeights[j] > -0.5) {
						weights[j] = oldWeights[j];
						continue;
					}
					tmp = [];
					tabVoisins[j].forEach(function(voisin) {
						if(oldWeights[voisin.id] > -0.5) {
							//tmp.push(oldWeights[voisin.id] + voisin.dist);
							tmp.push(1);
						}
					});
					if (tmp.length != 0) {
						weightsUpdated = true;
						weights[j] = 1;
						if (handleGoals.get(j) != undefined) {
							closestHandle = handleGoals.get(j);
							//console.log("ici : " + closestHandle);
							weightsUpdated = false;
							break;
						}
					} else {
						weights[j] = oldWeights[j];
					}
				}
				tmpArrayPointer = weights;
				weights = oldWeights;
				oldWeights = tmpArrayPointer;
			} while(weightsUpdated);
			//console.log(closestHandle);
			//console.log(COMPTEURDEBUG);
			if(normWeights[closestHandle] == undefined)
				console.log("undefined : " + closestHandle);
			normWeights[closestHandle][i] = 1;
			unnormWeights[closestHandle][i] = 1;
		}

		model.normalizedWeightsCP = normWeights;
		model.unnormalizedWeightsCP = unnormWeights;
	}
}
