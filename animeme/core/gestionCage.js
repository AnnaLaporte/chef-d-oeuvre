function addCageVertex(x, y) {
	var cv = new CageVertex();
	cv.restPos = glMatrix.vec2.fromValues(x, y);
	cv.pos = glMatrix.vec2.fromValues(x, y);

	if (_lLayer[_idActiveLayer].model.cage._currentVertex == _lLayer[_idActiveLayer].model.cage._cage.length - 1) {
		_lLayer[_idActiveLayer].model.cage._cage.push(cv);
		_lLayer[_idActiveLayer].model.cage._currentVertex++;
	}
	else {
		_lLayer[_idActiveLayer].model.cage._cage.splice(_lLayer[_idActiveLayer].model.cage._currentVertex+1, 0, cv);
		_lLayer[_idActiveLayer].model.cage._currentVertex ++;
	}
	initTabWeights();
}

function setActiveCageVertex(x,y,i) {
  _lLayer[_idActiveLayer].model.cage._currentVertex = i;
	if(glDrawWeights) {
  		glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.normalizedWeightsCage[_lLayer[_idActiveLayer].model.cage._currentVertex]);
	    glDrawOne(_lLayer[_idActiveLayer].model);
	}
}

function deleteCageVertex(x,y,i) {
  _lLayer[_idActiveLayer].model.cage._cage.splice(i,1);
  if (_lLayer[_idActiveLayer].model.cage._currentVertex >= i && _lLayer[_idActiveLayer].model.cage._currentVertex>0)
    _lLayer[_idActiveLayer].model.cage._currentVertex = _lLayer[_idActiveLayer].model.cage._currentVertex - 1;
	initTabWeights();
}

function moveCageVertex(x,y,i) {
  _lLayer[_idActiveLayer].model.cage._cage[i].pos[0] = x;
  _lLayer[_idActiveLayer].model.cage._cage[i].pos[1] = y;
	if (_modeE == modeEdition.Move) {
		_lLayer[_idActiveLayer].model.cage._cage[i].restPos[0] = x;
	  _lLayer[_idActiveLayer].model.cage._cage[i].restPos[1] = y;
	}
}

function updateTransformCageVertex(model, i) {
	var Mtinv = glMatrix.mat3.create();
	var transfo = glMatrix.mat3.create();
	var Mt = glMatrix.mat3.create();

	//rotation
	//glMatrix.mat3.rotate(transfo, transfo, model.cage._cage[i].angle * (pi/180));
	//translation
  transfo[6] = model.cage._cage[i].pos[0] - model.cage._cage[i].restPos[0];
  transfo[7] = model.cage._cage[i].pos[1] - model.cage._cage[i].restPos[1];

	Mtinv[6] = - model.cage._cage[i].restPos[0];
	Mtinv[7] = - model.cage._cage[i].restPos[1];

	Mt[6] = model.cage._cage[i].restPos[0];
	Mt[7] = model.cage._cage[i].restPos[1];

	glMatrix.mat3.multiply(transfo, transfo, Mtinv);
	glMatrix.mat3.multiply(transfo, Mt, transfo);

	//ça ne fait pas la copie mais la ref
	model.cage._cage[i].transform = transfo;
}

function resetPoseCage(m) {
	for (var i = 0; i < m.cage._cage.length; i++) {
		 m.cage._cage[i].pos[0] = m.cage._cage[i].restPos[0];
		 m.cage._cage[i].pos[1] = m.cage._cage[i].restPos[1];
	}

	for (let j=0 ; j<m.cage._cage.length ; j++) {
		m.cage._cage[j].transform = glMatrix.mat3.create();
	}

	m.currentMesh = m.restMesh.slice();
	glLoadTransf(m, m.cage._cage);
	glDrawOne(m);
	if (m.cage._faces.length == 0) {
		drawCage(m);
	}
  else {
    drawTrianglesCage(m);
  }
}


function determineWayCage(m) {
	var sizeCage = m.cage._cage.length;

	m.cage._mesh = [];
	m.cage._faces = [];

	var sum = 0;
	var sensTrigo = true;

	for (let i=0 ; i < sizeCage ; i++) {
		var p1 = m.cage._cage[i];
		var p2 = m.cage._cage[(i + 1)%sizeCage];
		var p3 = m.cage._cage[(i + 2)%sizeCage];

		var v1 = glMatrix.vec2.fromValues(p1.restPos[0] - p2.restPos[0], p1.restPos[1] - p2.restPos[1]);
		var v2 = glMatrix.vec2.fromValues(p3.restPos[0] - p2.restPos[0], p3.restPos[1] - p2.restPos[1]);

		var abDot = glMatrix.vec2.dot(v1, v2);
		var abCross = glMatrix.vec3.create();
		glMatrix.vec2.cross(abCross, v1, v2);

		var abTan = abCross[2]/abDot;
		var angle = 0;

		if (abDot < 0) {
			angle = pi + Math.atan(abTan);
		} else {
			angle = Math.atan(abTan);
		}
		angle = angle*(180/pi);

		if (angle<0)
			angle = angle + 360;

		sum = sum + angle;
	}
	if (sum < ((m.cage._cage.length - 2)*180)-0.05 || sum > ((m.cage._cage.length - 2)*180)+0.05) {
		sensTrigo = false;
	}

	var tmp = _lLayer[_idActiveLayer].model.cage._cage.slice();
	triangulation(m, tmp, sensTrigo);
}

function sign(p1, p2, p3) {
    return ((p1[0] - p3[0])*(p2[1]-p3[1]) - (p2[0] - p3[0])*(p1[1]-p3[1]));
}

function isInsideTriangle(P,A,B,C) {
    var d1 = sign(P, A, B);
    var d2 = sign(P, B, C);
    var d3 = sign(P, C, A);
    var has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
    var has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);
    return (!(has_neg && has_pos));
}

function triangulation(m, c, sens) {

	var idMin;
	var angleMin = 360;
	var id1, id2, id3;
	if (c.length == 3) {
		id1 = findVertex(c[0].restPos[0], c[0].restPos[1],m);
		id2 = findVertex(c[1].restPos[0], c[1].restPos[1],m);
		id3 = findVertex(c[2].restPos[0], c[2].restPos[1],m);
		m.cage._faces.push(glMatrix.vec3.fromValues(id1, id2, id3));

		return;
	}

	for (let i=0 ; i < c.length ; i++) {
		var p1 = c[i];
		var p2 = c[(i + 1)%c.length];
		var p3 = c[(i + 2)%c.length];

		if (sens) {
			var v1 = glMatrix.vec2.fromValues(p1.restPos[0] - p2.restPos[0], p1.restPos[1] - p2.restPos[1]);
			var v2 = glMatrix.vec2.fromValues(p3.restPos[0] - p2.restPos[0], p3.restPos[1] - p2.restPos[1]);
		}
		else {
			var v1 = glMatrix.vec2.fromValues(p3.restPos[0] - p2.restPos[0], p3.restPos[1] - p2.restPos[1]);
			var v2 = glMatrix.vec2.fromValues(p1.restPos[0] - p2.restPos[0], p1.restPos[1] - p2.restPos[1]);
		}

		var abDot = glMatrix.vec2.dot(v1, v2);
		var abCross = glMatrix.vec3.create();
		glMatrix.vec2.cross(abCross, v1, v2);

		var abTan = abCross[2]/abDot;
		var angle = 0;

		if (abDot < 0) {
			angle = pi + Math.atan(abTan);
		} else {
			angle = Math.atan(abTan);
		}
		angle = angle*(180/pi);

		if (angle<0)
			angle = angle + 360;

		id1 = findVertex(p1.restPos[0], p1.restPos[1],m);
		id2 = findVertex(p2.restPos[0], p2.restPos[1],m);
		id3 = findVertex(p3.restPos[0], p3.restPos[1],m);

		if (angle < angleMin) {
			var isInside = false;
			for (let tri = 0; tri < m.cage._cage.length; tri ++) {
			 	if (tri != id1 && tri != id2 && tri != id3) {
			   	if (isInsideTriangle(m.cage._cage[tri].restPos,m.cage._cage[id1].restPos,m.cage._cage[id2].restPos,m.cage._cage[id3].restPos)) {
			     	isInside = true;
			    }
			  }
			}
			if (!isInside) {
				angleMin = angle;
			  idMin = (i + 1)%c.length;
			}
		}
	}

	id1 = findVertex(c[(idMin-1+c.length)%c.length].restPos[0], c[(idMin-1+c.length)%c.length].restPos[1],m);
	id2 = findVertex(c[idMin].restPos[0], c[idMin].restPos[1],m);
	id3 = findVertex(c[(idMin+1)%c.length].restPos[0], c[(idMin+1)%c.length].restPos[1],m);
	m.cage._faces.push(glMatrix.vec3.fromValues(id1, id2, id3));

	c.splice(idMin, 1);
	triangulation(m, c, sens);
}


function findVertex(x,y,model) {
	for (let cageId = 0; cageId < model.cage._cage.length; ++cageId) {
		if (x == model.cage._cage[cageId].restPos[0] && y == model.cage._cage[cageId].restPos[1])
			return cageId;
	}
	return -1;
}
