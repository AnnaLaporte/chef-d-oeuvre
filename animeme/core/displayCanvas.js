function testBSpline() {

  var bs = BSpline();
  bs.k = 3;
  bs.points.push(glMatrix.vec2.fromValues(0,0));
  bs.points.push(glMatrix.vec2.fromValues(100,300));
  bs.points.push(glMatrix.vec2.fromValues(400,200));
  bs.points.push(glMatrix.vec2.fromValues(500,500));
  n = bs.points.length - 1;

  return bs;
}

function afficherBSpline(splin) {
  var rayon = 8;
  var i;

	ctx2d.setLineDash([]);
  for (i=0 ; i < splin.points.length ; i++) {
  	if(i != 0) {
			ctx2d.strokeStyle = "#FF0000";
	    ctx2d.lineWidth = 2;
  	  ctx2d.beginPath();
  		ctx2d.moveTo(splin.points[i-1][0], splin.points[i-1][1]);
  		ctx2d.lineTo(splin.points[i][0], splin.points[i][1]);
  		ctx2d.stroke();
	}
	if(i == splin.points.length-1)
		ctx2d.strokeStyle = "#5FAA00";
	else
		ctx2d.strokeStyle = "#7FFF00";

	ctx2d.lineWidth = 3;
    ctx2d.beginPath();
    ctx2d.arc(splin.points[i][0], splin.points[i][1], rayon, 0, 2 * Math.PI);
    ctx2d.stroke();
  }

  // Not enough points to draw line, return
  if (splin.k - 1 >= splin.points.length) {
    return;
  }

  var u;
  var res;
  ctx2d.beginPath();
  for (u=0 ; u<=1 ; u = u+( 1 / ((splin.points.length * 3)) ) ) {
    res = approximation(splin, u);
    rayon = 3;
    ctx2d.strokeStyle = "#00FFFF";
    //ctx2d.beginPath();
    //ctx2d.arc(res[0] - rayon, res[1] - rayon, 2*rayon, 0, 2 * Math.PI);
    ctx2d.lineTo(res[0], res[1]);
    //console.log(res[0] + ", " + res[1]);
  }
  res = approximation(splin, 1);
  rayon = 3;
  ctx2d.strokeStyle = "#00FFFF";
  //ctx2d.beginPath();
  //ctx2d.arc(res[0] - rayon, res[1] - rayon, 2*rayon, 0, 2 * Math.PI);
  ctx2d.lineTo(res[0], res[1]);
  ctx2d.stroke();
}

function drawPoint(m) {
  ctx2d.clearRect(0,0,800,800);
  ctx2d.setLineDash([]);
  var i;
  var rayon = 2;
  var rayon2 = 3;
  for (i=0 ; i<m.cp._cp.length ; i++){
    ctx2d.beginPath();
    ctx2d.lineWidth = 3;
    if (i == m.cp._currentCP) {
      ctx2d.strokeStyle = "#6495ED";
    }
    else {
      ctx2d.strokeStyle = "#DC143C";
    }
    // console.log(m.cp._cp[i].pos[0] +" " + m.cp._cp[i].pos[1]);
    ctx2d.arc(m.cp._cp[i].pos[0], m.cp._cp[i].pos[1], 2*rayon + 1, 0, 2 * Math.PI);
    ctx2d.stroke();

    ctx2d.beginPath();
    if (i == m.cp._currentCP) {
      ctx2d.strokeStyle = "pink";
    }
    else {
      ctx2d.strokeStyle = "black";
    }
    // console.log(m.cp._cp[i].pos[0] +" " + m.cp._cp[i].pos[1]);
    ctx2d.arc(m.cp._cp[i].pos[0], m.cp._cp[i].pos[1], 2*rayon2 + 1, 0, 2 * Math.PI);
    ctx2d.stroke();
  }
}

//unfinished
function drawCage(model) {
  ctx2d.clearRect(0,0,800,800);
	var rayon = 2;
	ctx2d.setLineDash([]);
	for (let i=0 ; i<model.cage._cage.length ; i++) {
	  ctx2d.beginPath();
	  ctx2d.lineWidth = 3;
	  if (i == model.cage._currentVertex) {
	    ctx2d.strokeStyle = "#6495ED";
	  }
	  else {
	    ctx2d.strokeStyle = "#DC143C";
	  }
	  // console.log(m.cp._cp[i].pos[0] +" " + m.cp._cp[i].pos[1]);
	  ctx2d.arc(model.cage._cage[i].pos[0], model.cage._cage[i].pos[1], 2*rayon + 1, 0, 2 * Math.PI);
	  ctx2d.stroke();

		if (i>0) {
			ctx2d.strokeStyle = "#DC143C";
			ctx2d.beginPath();

	    ctx2d.moveTo(model.cage._cage[i-1].pos[0], model.cage._cage[i-1].pos[1]);
	    ctx2d.lineTo(model.cage._cage[i].pos[0], model.cage._cage[i].pos[1]);

	    ctx2d.stroke();
		}
	}
	if(model.cage._cage.length != 0) {
		ctx2d.strokeStyle = "#DC143C";
		if (_modeE == modeEdition.Add || _modeE == modeEdition.Move || _modeE == modeEdition.Delete)
			ctx2d.setLineDash([5, 15]);
		else {
			ctx2d.setLineDash([]);
		}
		ctx2d.beginPath();

		ctx2d.moveTo(model.cage._cage[model.cage._cage.length-1].pos[0], model.cage._cage[model.cage._cage.length-1].pos[1]);
		ctx2d.lineTo(model.cage._cage[0].pos[0], model.cage._cage[0].pos[1]);

		ctx2d.stroke();
	}
}


function drawBrush(x, y) {
	ctx2d.setLineDash([]);
  var rayBrush = 0;
  if (_modeH == modeHandler.CP)
    rayBrush = Number($("#rangeDiamCP").val());
  else if (_modeH == modeHandler.Skeleton)
    rayBrush = Number($("#rangeDiamSquelette").val());
  else if (_modeH == modeHandler.Cage)
    rayBrush = Number($("#rangeDiamCage").val());

  ctx2d.beginPath();
    ctx2d.strokeStyle = "#000000";
    ctx2d.arc(x, y, rayBrush+0.5, 0, 2 * Math.PI);
    ctx2d.stroke();

}

function drawArrow(toParent, toChild) {
  var arrowLength = 15;
	ctx2d.setLineDash([]);

  var arrowAngle = Math.atan2(toChild[1] - toParent[1], toChild[0] - toParent[0]);
  ctx2d.lineTo(
    toChild[0] - arrowLength * Math.cos(arrowAngle - Math.PI/6),
    toChild[1] - arrowLength * Math.sin(arrowAngle - Math.PI/6)
  );
  ctx2d.lineTo(toChild[0], toChild[1]);
  ctx2d.lineTo(
    toChild[0] - arrowLength * Math.cos(arrowAngle + Math.PI/6),
    toChild[1] - arrowLength * Math.sin(arrowAngle + Math.PI/6)
  );
  ctx2d.lineTo(toChild[0], toChild[1]);
}

function drawSkeleton(m) {
  ctx2d.clearRect(0,0,800,800);
  ctx2d.setLineDash([]);
	ctx2d.lineWidth = 3;
  var i;
  var rayon = 2;
  var lineWidth = 2;

  for (i=0 ; i<m.skeleton._joints.length ; i++){
    ctx2d.beginPath();
    if (i == m.skeleton._currentJoint) {
      ctx2d.strokeStyle = "#6495ED";
    }
    // Root bone
    else if (i == 0){
      ctx2d.strokeStyle = "#000000"
    }
    else {
      ctx2d.strokeStyle = "#DC143C";
    }
    if ( i>=1 ){
      // Coordinates
      var toParentX = m.skeleton._joints[m.skeleton._joints[i].idFather].pos[0];
      var toParentY = m.skeleton._joints[m.skeleton._joints[i].idFather].pos[1];
      var toChildX = m.skeleton._joints[i].pos[0];
      var toChildY = m.skeleton._joints[i].pos[1];
      // Parent - Child
      ctx2d.moveTo(toParentX, toParentY);
      ctx2d.lineTo(toChildX, toChildY);

      ctx2d.stroke();
      // Arrow
      //drawArrow([toParentX, toParentY], [toChildX, toChildY]);
    }
      ctx2d.beginPath();
    ctx2d.arc(m.skeleton._joints[i].pos[0], m.skeleton._joints[i].pos[1], 2*rayon + 1, 0, 2 * Math.PI);
    ctx2d.stroke();
  }
}

function drawTrianglesCage(m) {
  ctx2d.clearRect(0,0,800,800);
  ctx2d.strokeStyle = "#DC143C";
  ctx2d.setLineDash([5, 15]);
	var rayon = 2;
	for (let j=0 ; j<m.cage._faces.length ; j++) {
		//console.log(m.cage._mesh[m.cage._faces[j][0]][0] + ", " + m.cage._mesh[m.cage._faces[j][0]][1]);
		var p1 = glMatrix.vec2.fromValues(m.cage._cage[m.cage._faces[j][0]].pos[0], m.cage._cage[m.cage._faces[j][0]].pos[1]);
		var p2 = glMatrix.vec2.fromValues(m.cage._cage[m.cage._faces[j][1]].pos[0], m.cage._cage[m.cage._faces[j][1]].pos[1]);
		var p3 = glMatrix.vec2.fromValues(m.cage._cage[m.cage._faces[j][2]].pos[0], m.cage._cage[m.cage._faces[j][2]].pos[1]);
		ctx2d.beginPath();

		ctx2d.moveTo(p1[0], p1[1]);
		ctx2d.lineTo(p2[0], p2[1]);
		ctx2d.lineTo(p3[0], p3[1]);

		ctx2d.closePath();
		ctx2d.stroke();
	}
	for (let i=0 ; i<m.cage._cage.length ; i++) {
		ctx2d.setLineDash([]);
	  ctx2d.beginPath();
	  ctx2d.lineWidth = 3;
	  // console.log(m.cp._cp[i].pos[0] +" " + m.cp._cp[i].pos[1]);
    if (i == m.cage._currentVertex) {
      ctx2d.strokeStyle = "#6495ED";
    }
    else {
      ctx2d.strokeStyle = "#DC143C";
    }
	  ctx2d.arc(m.cage._cage[i].pos[0], m.cage._cage[i].pos[1], 2*rayon + 1, 0, 2 * Math.PI);
	  ctx2d.stroke();
	}
}
