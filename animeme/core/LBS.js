function applyLBS(model) {
	var i;
	var j;
	var posH;
	var newPos = glMatrix.vec3.create();
	var tmpos = glMatrix.vec3.create();
	var lengthbytwo = model.restMesh.length / 2;
	var sumWeight;

	if (_modeH == modeHandler.CP) {
		for (i = 0 ; i < lengthbytwo ; i++) {
			glMatrix.vec3.zero(newPos);
			posH = glMatrix.vec3.fromValues(model.restMesh[2*i], model.restMesh[2*i+1], 1);
			sumWeight = 0;
			for (j = 0 ; j < model.cp._cp.length ; j++) {
				if (model.normalizedWeightsCP[j][i] > 0.00001) {
					sumWeight += model.normalizedWeightsCP[j][i];
					glMatrix.vec3.zero(tmpos);
					glMatrix.vec3.transformMat3(tmpos, posH, model.cp._cp[j].transform);
					glMatrix.vec3.scaleAndAdd(newPos, newPos, tmpos, model.normalizedWeightsCP[j][i]);
				}
			}
			if (sumWeight < 1) {
				glMatrix.vec3.scaleAndAdd(newPos, newPos, posH, (1 - sumWeight));
			}
			model.currentMesh[2*i] = newPos[0];
			model.currentMesh[2*i+1] = newPos[1];
		}
	}

	else if (_modeH == modeHandler.Skeleton) {
	    for (i = 0 ; i < lengthbytwo ; i++) {
	        glMatrix.vec3.zero(newPos);
	        posH = glMatrix.vec3.fromValues(model.restMesh[2*i], model.restMesh[2*i+1], 1);
	        sumWeight = 0;
	        for (j = 0 ; j < model.skeleton._bones.length ; j++) {
	            if (model.normalizedWeightsSkeleton[j][i] > 0.00001) {
	                sumWeight += model.normalizedWeightsSkeleton[j][i];
	                glMatrix.vec3.zero(tmpos);
	                glMatrix.vec3.transformMat3(tmpos, posH, model.skeleton._bones[j].transform);
	                glMatrix.vec3.scaleAndAdd(newPos, newPos, tmpos, model.normalizedWeightsSkeleton[j][i]);
	            }
	        }
	        //newPos = newPos + restPos * (1 - {somme des poids});
	        if (sumWeight < 1) {
	            glMatrix.vec3.scaleAndAdd(newPos, newPos, posH, (1 - sumWeight));
	        }
	        model.currentMesh[2*i] = newPos[0];
	        model.currentMesh[2*i+1] = newPos[1];
	    }
	}
	else if (_modeH == modeHandler.Cage) {
		for (i = 0 ; i < lengthbytwo ; i++) {
			glMatrix.vec3.zero(newPos);
			posH = glMatrix.vec3.fromValues(model.restMesh[2*i], model.restMesh[2*i+1], 1);
			sumWeight = 0;
			for (j = 0 ; j < model.cage._cage.length ; j++) {
				if (model.normalizedWeightsCage[j][i] > 0.00001) {
					sumWeight += model.normalizedWeightsCage[j][i];
					glMatrix.vec3.zero(tmpos);
					glMatrix.vec3.transformMat3(tmpos, posH, model.cage._cage[j].transform);
					glMatrix.vec3.scaleAndAdd(newPos, newPos, tmpos, model.normalizedWeightsCage[j][i]);
				}
			}
			if (sumWeight < 1) {
				glMatrix.vec3.scaleAndAdd(newPos, newPos, posH, (1 - sumWeight));
			}
			model.currentMesh[2*i] = newPos[0];
			model.currentMesh[2*i+1] = newPos[1];
		}
	}

	else if (_modeH == modeHandler.AnimByBSpline) {
		if(Animation.modeHBSpline == modeHandler.CP) {
			for (i = 0 ; i < lengthbytwo ; i++) {
				glMatrix.vec3.zero(newPos);
				posH = glMatrix.vec3.fromValues(model.restMesh[2*i], model.restMesh[2*i+1], 1);
				sumWeight = 0;
				for (j = 0 ; j < model.cp._cp.length ; j++) {
					if (model.normalizedWeightsCP[j][i] > 0.00001) {
						sumWeight += model.normalizedWeightsCP[j][i];
						glMatrix.vec3.zero(tmpos);
						glMatrix.vec3.transformMat3(tmpos, posH, model.cp._cp[j].transform);
						glMatrix.vec3.scaleAndAdd(newPos, newPos, tmpos, model.normalizedWeightsCP[j][i]);
					}
				}
				if (sumWeight < 1) {
					glMatrix.vec3.scaleAndAdd(newPos, newPos, posH, (1 - sumWeight));
				}
				model.currentMesh[2*i] = newPos[0];
				model.currentMesh[2*i+1] = newPos[1];
			}
		}
	}
}
