function addSkeleton(x, y)
{
  // création des Bone associé
	var joint = new Joint();
	joint.restPos = glMatrix.vec2.fromValues(x,y);
	joint.pos = glMatrix.vec2.fromValues(x,y);
	if (_lLayer[_idActiveLayer].model.skeleton._currentJoint != -1)
	joint.idFather = _lLayer[_idActiveLayer].model.skeleton._currentJoint;
	joint.id = _lLayer[_idActiveLayer].model.skeleton._joints.length;


	if (selectedHandle == -1 )
	{
		_lLayer[_idActiveLayer].model.skeleton._joints.push(joint);

		// ajout du fils au père
		if (joint.idFather != -1) {
			var b = new Bone();
			b.idChild = _lLayer[_idActiveLayer].model.skeleton._joints.length - 1;
			b.transform = glMatrix.mat3.create();
			b.idFather = joint.idFather;
			_lLayer[_idActiveLayer].model.skeleton._bones.push(b);

			_lLayer[_idActiveLayer].model.skeleton._joints[joint.idFather].child.push(joint.id);

			// Create empty skeleton weight matrices
			_lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton = [...Array(_lLayer[_idActiveLayer].model.skeleton._bones.length)].map((_, i) => Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
			_lLayer[_idActiveLayer].model.normalizedWeightsSkeleton = [...Array(_lLayer[_idActiveLayer].model.skeleton._bones.length)].map((_, i) => Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
		}

		_lLayer[_idActiveLayer].model.skeleton._currentJoint = _lLayer[_idActiveLayer].model.skeleton._joints.length-1;
		_lLayer[_idActiveLayer].model.skeleton._currentBone = _lLayer[_idActiveLayer].model.skeleton._bones.length-1;
	}
	else {
	_lLayer[_idActiveLayer].model.skeleton._currentJoint = selectedHandle;
	}

	initTabWeights();
	selectedHandle = -1;
	}

function setActiveSkeleton(x, y, i)
{
	// CURRENT JOINT
	_lLayer[_idActiveLayer].model.skeleton._currentJoint = i;

	// CURRENT BONE
	// No bone if the root is selected
	if (i != 0) {
		// Looking for the bone for which the _currentJoint is its child
		for (var idBone = 0; idBone < _lLayer[_idActiveLayer].model.skeleton._bones.length; ++idBone) {
			if (_lLayer[_idActiveLayer].model.skeleton._bones[idBone].idChild == i)
				break;
		}
		_lLayer[_idActiveLayer].model.skeleton._currentBone = idBone;
	}
	else
		_lLayer[_idActiveLayer].model.skeleton._currentBone = -1;

	// DRAW SKELETON
	if(glDrawWeights && i !=0) {
		glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsSkeleton[idBone]);
	} else if (glDrawWeights) {
		glLoadDispWeights(_lLayer[_idActiveLayer].model, new Array(_lLayer[_idActiveLayer].model.restMesh.length/2).fill(0));
	}
	glDrawOne(_lLayer[_idActiveLayer].model);
}

function moveSkeleton(x,y,i)
{
  _lLayer[_idActiveLayer].model.skeleton._joints[i].pos[0] = x;
  _lLayer[_idActiveLayer].model.skeleton._joints[i].pos[1] = y;
	if (_modeE == modeEdition.Move) {
		_lLayer[_idActiveLayer].model.skeleton._joints[i].restPos[0] = x;
	  _lLayer[_idActiveLayer].model.skeleton._joints[i].restPos[1] = y;
	}
	//console.log(_lLayer[_idActiveLayer].model.skeleton._joints[i]);
}

function deleteRacine(i) {
	_lLayer[_idActiveLayer].model.skeleton._joints = [];
	_lLayer[_idActiveLayer].model.skeleton._bones = [];
	_lLayer[_idActiveLayer].model.skeleton._currentJoint = -1;
	initTabWeights();
}

function deleteLeaf(i) {
	if (_lLayer[_idActiveLayer].model.skeleton._joints[i].child.length != 0) return;

	// on supprime dans _bones
	for (let j=0 ; j<_lLayer[_idActiveLayer].model.skeleton._bones.length ; j++) {
		if (_lLayer[_idActiveLayer].model.skeleton._bones[j].idChild == i) {
			_lLayer[_idActiveLayer].model.skeleton._bones.splice(j,1);
		}
	}

	for (let j=0 ; j<_lLayer[_idActiveLayer].model.skeleton._bones.length ; j++) {
		if (_lLayer[_idActiveLayer].model.skeleton._bones[j].idChild > i) {
			_lLayer[_idActiveLayer].model.skeleton._bones[j].idChild--;
		}
		if (_lLayer[_idActiveLayer].model.skeleton._bones[j].idFather > i) {
			_lLayer[_idActiveLayer].model.skeleton._bones[j].idFather--;
		}
	}

	// on supprime l'élément dans la liste des enfants de son père
	for (j=0 ; j<_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._joints[i].idFather].child.length ; j++) {
		if (i == _lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._joints[i].idFather].child[j])
			_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._joints[i].idFather].child.splice(j,1);
	}

	// on met à jour tous les indices
			//}
			//console.log("lbs");
	for (j=0 ; j<_lLayer[_idActiveLayer].model.skeleton._joints.length ; j++) {
		if (_lLayer[_idActiveLayer].model.skeleton._joints[j].id > i)
			_lLayer[_idActiveLayer].model.skeleton._joints[j].id--;
		if (_lLayer[_idActiveLayer].model.skeleton._joints[j].idFather > i)
			_lLayer[_idActiveLayer].model.skeleton._joints[j].idFather --;
		for (c = 0; c < _lLayer[_idActiveLayer].model.skeleton._joints[j].child.length ; c++) {
			if (_lLayer[_idActiveLayer].model.skeleton._joints[j].child[c] > i)
				_lLayer[_idActiveLayer].model.skeleton._joints[j].child[c]--;
		}
	}

	// supprime l'élément dans la liste des bone
	_lLayer[_idActiveLayer].model.skeleton._joints.splice(i,1);

	// on met à jour le currentJoint
	if (_lLayer[_idActiveLayer].model.skeleton._currentJoint >= i && _lLayer[_idActiveLayer].model)
		_lLayer[_idActiveLayer].model.skeleton._currentJoint = _lLayer[_idActiveLayer].model.skeleton._currentJoint - 1;

	initTabWeights();
}

function deleteNodeRight(i) {
	// raccroche fils au père

		// on supprime dans _bones
		for (let j=0 ; j<_lLayer[_idActiveLayer].model.skeleton._bones.length ; j++) {
			if (_lLayer[_idActiveLayer].model.skeleton._bones[j].idChild == i) {
				var fatherChange = _lLayer[_idActiveLayer].model.skeleton._bones[j].idFather;
				_lLayer[_idActiveLayer].model.skeleton._bones.splice(j,1);
			}
		}

		for (let j=0 ; j<_lLayer[_idActiveLayer].model.skeleton._bones.length ; j++) {
			if (_lLayer[_idActiveLayer].model.skeleton._bones[j].idChild > i) {
				_lLayer[_idActiveLayer].model.skeleton._bones[j].idChild--;
			}
			if (_lLayer[_idActiveLayer].model.skeleton._bones[j].idFather > i) {
				_lLayer[_idActiveLayer].model.skeleton._bones[j].idFather--;
			}
		}

		// on supprime dans _bones
		for (let j=0 ; j<_lLayer[_idActiveLayer].model.skeleton._bones.length ; j++) {
			if (_lLayer[_idActiveLayer].model.skeleton._bones[j].idFather == i) {
				_lLayer[_idActiveLayer].model.skeleton._bones[j].idFather = fatherChange;
			}
		}

	// on supprime l'élément dans la liste des enfants de son père
	for (j=0 ; j<_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._joints[i].idFather].child.length ; j++) {
		if (i == _lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._joints[i].idFather].child[j])
			_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._joints[i].idFather].child.splice(j,1);
	}

	// on met à jour tous les indices
	for (j=0 ; j<_lLayer[_idActiveLayer].model.skeleton._joints.length ; j++) {
		if (_lLayer[_idActiveLayer].model.skeleton._joints[j].id > i)
			_lLayer[_idActiveLayer].model.skeleton._joints[j].id--;
	}

	// push les fils dans la liste child du père et on mets à jour les idFather des fils
	for (j=0 ; j<_lLayer[_idActiveLayer].model.skeleton._joints[i].child.length ; j++) {
		_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._joints[i].idFather].child.push(_lLayer[_idActiveLayer].model.skeleton._joints[i].child[j]);
		_lLayer[_idActiveLayer].model.skeleton._joints[_lLayer[_idActiveLayer].model.skeleton._joints[i].child[j]].idFather = _lLayer[_idActiveLayer].model.skeleton._joints[i].idFather;

	}
	// supprime l'élément dans la liste des bone
	_lLayer[_idActiveLayer].model.skeleton._joints.splice(i,1);

	// on met à jour les indices idFather et child de toute la liste
	for (j=0 ; j<_lLayer[_idActiveLayer].model.skeleton._joints.length ; j++) {
		if (_lLayer[_idActiveLayer].model.skeleton._joints[j].idFather > i)
			_lLayer[_idActiveLayer].model.skeleton._joints[j].idFather --;
		for (c = 0; c < _lLayer[_idActiveLayer].model.skeleton._joints[j].child.length ; c++) {
			if (_lLayer[_idActiveLayer].model.skeleton._joints[j].child[c] > i)
				_lLayer[_idActiveLayer].model.skeleton._joints[j].child[c]--;
		}
	}

	// on met à jour le currentJoint
	if (_lLayer[_idActiveLayer].model.skeleton._currentJoint >= i && _lLayer[_idActiveLayer].model.skeleton._currentJoint>=0)
		_lLayer[_idActiveLayer].model.skeleton._currentJoint = _lLayer[_idActiveLayer].model.skeleton._currentJoint - 1;

	initTabWeights();
}

function deleteNodeLeft(i) {
	if (_lLayer[_idActiveLayer].model.skeleton._joints[i].child.length == 0) {
		deleteLeaf(i);
	} else {
		while(_lLayer[_idActiveLayer].model.skeleton._joints[i].child.length > 0) {
			deleteNodeLeft(_lLayer[_idActiveLayer].model.skeleton._joints[i].child[_lLayer[_idActiveLayer].model.skeleton._joints[i].child.length-1]);
		}
		deleteLeaf(i);
	}
}

function updateTransformAngle(x, y, i, model, numBone) {
	if (i != 0) {
		var pos = glMatrix.vec3.fromValues(model.skeleton._joints[i].restPos[0], model.skeleton._joints[i].restPos[1], 1);
		var numFather;
		var transfo =  glMatrix.mat3.create();
		if (model.skeleton._joints[i].idFather == 0) {
			transfo = model.skeleton._rootTransform;
		} else {
			for (let tmp = 0; tmp < model.skeleton._bones.length; tmp++) {
				if (model.skeleton._bones[tmp].idChild == model.skeleton._joints[i].idFather) {
					numFather = tmp;
				}
			}
			transfo = model.skeleton._bones[numFather].transform;
		}
		glMatrix.vec3.transformMat3(pos, pos, transfo);

		var a = glMatrix.vec2.fromValues(pos[0]-model.skeleton._joints[model.skeleton._joints[i].idFather].pos[0], pos[1]-model.skeleton._joints[model.skeleton._joints[i].idFather].pos[1]);
		var b = glMatrix.vec2.fromValues( x-model.skeleton._joints[model.skeleton._joints[i].idFather].pos[0], y-model.skeleton._joints[model.skeleton._joints[i].idFather].pos[1]);
		if(a[0] == 0 && a[1] == 0 || b[0] == 0 && b[1] == 0) return;
		var abDot = glMatrix.vec2.dot(a, b);
		var abCross = glMatrix.vec3.create();
		abCross = glMatrix.vec2.cross(abCross, a, b);

		var abTan = abCross[2]/abDot;
		var angle;

		if (abDot < 0) {
			angle = pi + Math.atan(abTan);
		} else {
			angle = Math.atan(abTan);
		}
		model.skeleton._bones[numBone].angle = angle;
	}
}

function updateTransformSkeleton(model, i, numBone) {

	var Mtinv = glMatrix.mat3.create();
	 var transfo = glMatrix.mat3.create();
	 var Mt = glMatrix.mat3.create();

	 //translation
	 if(i == 0) {
		 transfo[6] = model.skeleton._joints[0].pos[0] - model.skeleton._joints[0].restPos[0];
		 transfo[7] = model.skeleton._joints[0].pos[1] - model.skeleton._joints[0].restPos[1];
	 }

	 if(i != 0) {

		 //rotation
		 glMatrix.mat3.rotate(transfo, transfo, model.skeleton._bones[numBone].angle);
		 //console.log(numBone);
		 Mtinv[6] = - model.skeleton._joints[model.skeleton._joints[i].idFather].restPos[0];
		 Mtinv[7] = - model.skeleton._joints[model.skeleton._joints[i].idFather].restPos[1];

		 Mt[6] = model.skeleton._joints[model.skeleton._joints[i].idFather].restPos[0];
		 Mt[7] = model.skeleton._joints[model.skeleton._joints[i].idFather].restPos[1];

		 glMatrix.mat3.multiply(transfo, transfo, Mtinv);
		glMatrix.mat3.multiply(transfo, Mt, transfo);
	 } else {
			 Mtinv[6] = - model.skeleton._joints[i].restPos[0];
			 Mtinv[7] = - model.skeleton._joints[i].restPos[1];

			 Mt[6] = model.skeleton._joints[i].restPos[0];
			 Mt[7] = model.skeleton._joints[i].restPos[1];

			glMatrix.mat3.multiply(transfo, transfo, Mtinv);
			glMatrix.mat3.multiply(transfo, Mt, transfo);

			model.skeleton._rootTransform = transfo;
	 }

	 //ça ne fait pas la copie mais la ref
	 if(model.skeleton._joints[i].idFather > 0) {
		 	var numFatherBone;
			for (var n = 0; n < model.skeleton._bones.length; n++) {
 			 if (model.skeleton._bones[n].idChild == model.skeleton._bones[numBone].idFather)
 			 {
 				 numFatherBone = n;
 			 }
 		 }
		 glMatrix.mat3.multiply(transfo, model.skeleton._bones[numFatherBone].transform, transfo);
	 }
	 else if (model.skeleton._joints[i].idFather == 0) {
		 glMatrix.mat3.multiply(transfo, model.skeleton._rootTransform, transfo);
	 }
	 if (i == 0)
	  model.skeleton._rootTransform = transfo;
	else
	  model.skeleton._bones[numBone].transform = transfo;

		var posChild = glMatrix.vec3.fromValues(model.skeleton._joints[i].restPos[0], model.skeleton._joints[i].restPos[1], 1);
		glMatrix.vec3.transformMat3(posChild, posChild, transfo);

		model.skeleton._joints[i].pos[0] = posChild[0];
		model.skeleton._joints[i].pos[1] = posChild[1];

	 for (var m = 0; m < model.skeleton._joints[i].child.length; m++ ) {
		 var numChildBone;
		 for (var n = 0; n < model.skeleton._bones.length; n++) {
			 if (model.skeleton._bones[n].idChild == model.skeleton._joints[i].child[m])
			 {
				 numChildBone = n;
			 }

		 }
		 updateTransformSkeleton(model, model.skeleton._joints[i].child[m], numChildBone);
	 }
}

function resetPoseSkeleton(m) {
	for (var i = 0; i < m.skeleton._joints.length; i++) {
		m.skeleton._joints[i].pos[0] = m.skeleton._joints[i].restPos[0];
		m.skeleton._joints[i].pos[1] = m.skeleton._joints[i].restPos[1];
	}

	m.skeleton._rootTransform = glMatrix.mat3.create();
	for (let j=0 ; j<m.skeleton._bones.length ; ++j) {
		m.skeleton._bones[j].transform = glMatrix.mat3.create();
		m.skeleton._bones[j].angle = 0;
	}



	m.currentMesh = m.restMesh.slice();
	glLoadTransf(m, m.skeleton._bones);
	glDrawOne(m);
	drawSkeleton(m);
}
