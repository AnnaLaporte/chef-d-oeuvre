function addCP(x, y)
{
  // création des singleCP associés witch
  var cp = new SingleCP();
  cp.restPos = glMatrix.vec2.fromValues(x,y);
  cp.pos = glMatrix.vec2.fromValues(x,y);

  _lLayer[_idActiveLayer].model.cp._cp.push(cp);
  _lLayer[_idActiveLayer].model.cp._currentCP = _lLayer[_idActiveLayer].model.cp._cp.length-1;

	initTabWeights();

}

function deleteCP(x,y,i) {
  _lLayer[_idActiveLayer].model.cp._cp.splice(i,1);
  if (_lLayer[_idActiveLayer].model.cp._currentCP >= i && _lLayer[_idActiveLayer].model.cp._currentCP>0)
    _lLayer[_idActiveLayer].model.cp._currentCP = _lLayer[_idActiveLayer].model.cp._currentCP - 1;
	initTabWeights();
}

function setActiveCP(x,y,i) {
  _lLayer[_idActiveLayer].model.cp._currentCP = i;
	if(glDrawWeights) {
  		glLoadDispWeights(_lLayer[_idActiveLayer].model, _lLayer[_idActiveLayer].model.unnormalizedWeightsCP[_lLayer[_idActiveLayer].model.cp._currentCP]);
	    glDrawOne(_lLayer[_idActiveLayer].model);
	}
}

function moveCP(x,y,i) {
  _lLayer[_idActiveLayer].model.cp._cp[i].pos[0] = x;
  _lLayer[_idActiveLayer].model.cp._cp[i].pos[1] = y;
	if (_modeE == modeEdition.Move) {
		_lLayer[_idActiveLayer].model.cp._cp[i].restPos[0] = x;
	  _lLayer[_idActiveLayer].model.cp._cp[i].restPos[1] = y;
	}
}

function updateTransformCP(model,i) {
	var Mtinv = glMatrix.mat3.create();
	var transfo = glMatrix.mat3.create();
	var Mt = glMatrix.mat3.create();

	//rotation
	glMatrix.mat3.rotate(transfo, transfo, model.cp._cp[i].angle * (pi/180));
	//translation
  transfo[6] = model.cp._cp[i].pos[0] - model.cp._cp[i].restPos[0];
  transfo[7] = model.cp._cp[i].pos[1] - model.cp._cp[i].restPos[1];

	Mtinv[6] = - model.cp._cp[i].restPos[0];
	Mtinv[7] = - model.cp._cp[i].restPos[1];

	Mt[6] = model.cp._cp[i].restPos[0];
	Mt[7] = model.cp._cp[i].restPos[1];

	glMatrix.mat3.multiply(transfo, transfo, Mtinv);
	glMatrix.mat3.multiply(transfo, Mt, transfo);

	//ça ne fait pas la copie mais la ref
	model.cp._cp[i].transform = transfo;
}

function resetPoseCP(m) {
	for (var i = 0; i < m.cp._cp.length; i++) {
		 m.cp._cp[i].pos[0] = m.cp._cp[i].restPos[0];
		 m.cp._cp[i].pos[1] = m.cp._cp[i].restPos[1];
	}

	for (let j=0 ; j<m.cp._cp.length ; j++) {
		m.cp._cp[j].transform = glMatrix.mat3.create();
		m.cp._cp[j].angle = 0;
	}

	m.currentMesh = m.restMesh.slice();
	glLoadTransf(m, m.cp._cp);
	glDrawOne(m);
	drawPoint(m);
}
