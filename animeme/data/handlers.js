function SingleCP() {
  var sCP = {};
  // sCP.id = 0;
  sCP.restPos = glMatrix.vec2.create();
  sCP.pos = glMatrix.vec2.create();
  sCP.transform = glMatrix.mat3.create();
  sCP.angle = 0;
  sCP.BS = new BSpline();
  return sCP;
}

function CP() {
  var cp = {};
  cp._cp = [];
  cp._currentCP = 0;
  return cp;
}

function Bone() {
	var b = {};
	b.idChild = -1;
	b.idFather = -1;
	b.transform = glMatrix.mat3.create();
  b.angle = 0;
	return b;
}

function Joint() {
  var j = {};
  j.restPos = glMatrix.vec2.create();
  j.pos = glMatrix.vec2.create();
	j.child = [];
  j.id = 0;
  j.idFather = -1;
  j.BS = new BSpline();
  return j;
}

function Skeleton() {
  var s = {};
  s._joints = [];
	s._bones = [];
  s._currentJoint = -1;
	s._currentBone = -1;
	s._rootTransform = glMatrix.mat3.create();
  return s;
}

function CageVertex() {
  var vertex = {};
  vertex.restPos = glMatrix.vec2.create();
  vertex.pos = glMatrix.vec2.create();
  vertex.transform = glMatrix.mat3.create();
  vertex.BS = new BSpline();
  // vertex.id = 0;
  return vertex;
}

function Cage() {
  var c = {};
  c._cage = [];
  c._currentVertex = -1;
	c._faces = [];
  return c;
}
