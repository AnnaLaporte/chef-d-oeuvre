function Model() {
  var mo = {};

  mo.normalizedWeightsCP = [];
  mo.unnormalizedWeightsCP = [];
  mo.normalizedWeightsSkeleton = [];
  mo.unnormalizedWeightsSkeleton = [];
  mo.normalizedWeightsCage = [];
  mo.unnormalizedWeightsCage = [];

  mo.cp = new CP();
  mo.skeleton = new Skeleton();
  mo.cage = new Cage();

  mo.restMesh = [];
  mo.currentMesh = [];
  mo.indTri = [];
  mo.imgModel = null;

  mo.triElementBuffID = -1;
  mo.posBuffID = -1;
  mo.texCoordBuffID = -1;
  mo.dispWeightBuffID = -1;
  mo.transfWeightsBuffID = -1;
  mo.transfIDBuffID = -1;

  mo.texUniformID = -1;
  mo.texTransfUniformID = -1;

  mo.nbTransf = 0;

  return mo;
}
