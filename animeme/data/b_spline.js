function BSpline() {
    var bs = {};
    bs.points = [];
    bs.nodalVector = [];
    bs.k = 0;
    return bs;
}

// splin Bspline a utiliser pour approximer
// u valeur de u a approximer entre 0 et 1
function approximation(splin, u) {
    if(splin.k < 2)
        return glMatrix.vec2.fromValues(0,0);

    var i;
    var m = splin.k - 1;

    var vecNod = [];
    for(i = 0; i < splin.points.length + splin.k; i++) {
        vecNod.push(i - m);
    }

    // on normalise par rapport au vecteur nodal
    u = u * vecNod[splin.points.length];

    var dec = 0;
    for(i = splin.k; u > vecNod[i]; i++) {
        dec++;
    }

    var pCal = [];
    for(i = 0; i < splin.k; i++) {
        pCal.push(glMatrix.vec2.clone(splin.points[dec + i]));
    }

    //console.log(pCal);
    //console.log(u);
    //console.log(vecNod);

    return floraison(pCal, vecNod, u, splin.k, dec);
}

// ATTENTION :
// ne pas appeller floraison ailleurs que dans approximation
// pCal points influents sur p(u)
// v vecteur nodal
// u le u a approximer
// k l'ordre
// dec le decalage dans le vecteur nodal
function floraison(pCal, v, u, k, dec) {
    var min;
    var max;
    var j;
    var tmp1 = glMatrix.vec2.fromValues(0,0);
    var tmp2 = glMatrix.vec2.fromValues(0,0);
    for( j = 0; j < k - 1; j++ ) {
        max = v[dec + k + j];
        min = v[dec + 1 + j];
		glMatrix.vec2.set(tmp1, 0, 0);
		glMatrix.vec2.set(tmp2, 0, 0);
        glMatrix.vec2.scale(tmp1, pCal[j], (max - u) / (max - min));
        glMatrix.vec2.scale(tmp2, pCal[j+1], (u - min) / (max - min));
        glMatrix.vec2.add(pCal[j], tmp1, tmp2);
    }

    // k ne devrait jamais être < 2
    if(k == 2) {
        return pCal[0];
    } else {
        return floraison(pCal, v, u, k-1, dec+1);
    }

}

function loopBSpline(splin) {
	if(splin.k > splin.points.length)
		return;
	var tmp1 = approximation(splin, 0);
	var tmp2 = approximation(splin, 1);
	if(glMatrix.vec2.distance(tmp1, tmp2) < 100) {
		//user already tried to loop it
		console.log("fine tuning loop");
		var i;
		for(i = 0; i < splin.k - 1; i++) {
			splin.points[splin.points.length - splin.k + 1 + i] = glMatrix.vec2.clone(splin.points[i]);
		}
	} else {
		//user wants us to loop it for him
		console.log("creating loop");
		var i;
		for(i = 0; i < splin.k - 1; i++) {
			splin.points.push(glMatrix.vec2.clone(splin.points[i]));
		}
	}
}

function translateBSpline(splin, pt) {
	if(splin.k > splin.points.length)
		return;
	var transl = approximation(splin, 0);
	glMatrix.vec2.subtract(transl, pt, transl);
	splin.points.forEach(function(point) {
		glMatrix.vec2.add(point, point, transl);
		if(point[0] < 0) {
			point[0] = 0;
		} else if(point[0] > 800) {
			point[0] = 800;
		}
		if(point[1] < 0) {
			point[1] = 0;
		} else if(point[1] > 800) {
			point[1] = 800;
		}
	});
}

// Deletes the BSpline for the given handleId for modeH data structure in model
// If handleId == -1, deletes all BSpline for that data structure
function deleteBSpline(model, modeH, handleId) {
  switch (modeH) {
  // CP
  case modeHandler.CP:
    switch (handleId) {
    case -1:
      for ( var id = 0; id < model.cp._cp.length; ++id) {
        model.cp._cp[id].BS = new BSpline();
      }
      break;
    default:
      model.cp._cp[handleId].BS = new BSpline();
      break;
    }
    break;

  // Skeleton
  case modeHandler.Skeleton:
    switch (handleId) {
    case -1:
      for ( var id = 0; id < model.skeleton._joints.length; ++id) {
        model.skeleton._joints[id].BS = new BSpline();
      }
      break;
    default:
      model.skeleton._joints[handleId].BS = new BSpline();
      break;
    }
    break;

  // Cage
  case modeHandler.Cage:
    switch (handleId) {
    case -1:
      for ( var id = 0; id < model.cage._cage.length; ++id) {
        model.cage._cage[id].BS = new BSpline();
      }
      break;
    default:
      model.cage._cage[handleId].BS = new BSpline();
      break;
    }
    break;
  }
}
