# Animeme

`Animeme` est un projet réalisé à l'occasion du "Chef d'Œuvre" du Master 2 IGAI (*Informatique Graphique et Analyse d'Image*) de l'Université Toulouse III - Paul Sabatier.

Ce projet vise à implémenter l'algorithme et les structures de données du _Linear Blend Skinning_ largement utilisé dans l'animation 3D mais dans notre cas pour les images en 2D. Il est à noter que l'animation 2D traditionnelle se repose le plus souvent sur le dessin successif et éventuellement l'interpolation de différentes images et non de l'animation à partir de la pose de repos. Certains problèmes peuvent alors émerger, il faut interpoler l'image pour éviter des "trous" dans l'image, nous nous reposons sur la rasterisation de WebGL pour pour pallier à ce problème et être performant. Un autre problème pouvant apparaître en 2D est le 2-fighting (clignotement de triangles qui se superposent) or nous utilisons une résolution au pixel sur notre maillage pour garder une qualité d'image optimale et résoudre ce problème.

L'application utilise massivement le langage Javascript et repose sur la technologie WebGL 2.0. Elle est construire autour d'une interface en HTML5 `interface.html` qui charge les fichiers Javascript nécessaires.

Vous êtes libre de:
* charger une image (`.png` avec transparence) ou fichier de calque `.anmm` (format JSON),
* créer des points de contrôle, un squelette et une cage pour un modèle donnée,
* peindre manuellement les poids ou les générer automatiquement avec soit la méthode de distance inverse, support compact ou encore barycentrique,
* **déformer votre modèle**,
* automatiser vos animations avec des B-Splines.

Les auteurs sont:
* Anna Laporte
* Yi Nie
* Suzanne Sorli
* Charles Beaudonnet
* Pierre Cholet

![Salut Link!](files/hi4.gif)

## Structure du Projet

* `animeme/`: contient les fichiers source
  * `core/`: contient les classes et le code fonctionnel
  * `data/`: contient les template des objets utilisés (struct)
* `files/`: contient les rapports et supports de présentation orales
* `lib`/: bibliothèques tierces utilisées (décrites plus bas)
* `test/`: code de test
* `interface.html`: point d'entrée d'Animeme
* `site.html`: page web hebergée sur le site du Master IGAI

## Bibliothèques utilisées

Nous avons utilisé 2 bibliothèques qui sont:
* [jQuery](https://jquery.com/) v3.3.1 (MIT)
* [glMatrix](https://glmatrix.net/) v3.0 (MIT)
